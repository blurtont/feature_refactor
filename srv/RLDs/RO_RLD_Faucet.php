<?php
   include_once "RO_Device.php";

   class RO_RLD_Faucet extends RO_Device
   {
      const name_table = "RLD_Faucets";
      
      public $type_faucet;
      public $use_per_day_minutes;
      public $use_per_day_minutes_reduce_to;

      public $action_free_reduce_time;
      public $action_upgrade_low_flow;
      public $action_upgrade_standard;
      public $action_free_worst_time;

      static function create_default($id_room)
      {
         return RO_RLD_Faucet::load(Helper_RTLDs::create_device_and_actions($id_room, "RLD_Faucets", 16));
      }

      /* save an existing device in the DB */
      static function update($device)
      {   
         RO_Action::update_actions($device->ro_actions);
                               
         $mysqli = connecti();

         $query = sprintf("UPDATE RLD_Faucets SET
                     id_room_RLD_Faucets='%d',
                     id_residence_RLD_Faucets='%d',
                     is_info_entered='%d',
                     type_faucet='%d',
                     use_per_day_minutes='%d',
                     use_per_day_minutes_reduce_to='%d'
                     WHERE id_device='%d'",
                     mysql_real_escape_string($device->id_room),
                     mysql_real_escape_string($device->id_residence),
                     mysql_real_escape_string($device->is_info_entered),
                     mysql_real_escape_string($device->type_faucet),
                     mysql_real_escape_string($device->use_per_day_minutes),
                     mysql_real_escape_string($device->use_per_day_minutes_reduce_to),
                     mysql_real_escape_string($device->id_device));      
         if (!$mysqli->query($query))
         {
            error_log("ERROR: failed to update faucet ".$mysqli->error);
         }
      }
                                        
      static function load_all_in_room($id_room)
      {
         $mysqli = connecti();
         $new_faucets = array();
         $query = sprintf("SELECT * FROM RLD_Faucets WHERE id_room_RLD_Faucets='%d'",
                     mysql_real_escape_string($id_room));
         if (!($result = $mysqli->query($query)))
         {
            error_log("Failed to select faucetss. ".$mysqli->error);
            return null;
         }
         while($a_row = $result->fetch_assoc())
         {
            array_push($new_faucets, RO_RLD_Faucet::copy_to_device($a_row));
         }
         return $new_faucets;
      }

      static function load($id_device)
      {
         $mysqli = connecti();
         $query = sprintf("SELECT * FROM RLD_Faucets WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if(!($result = $mysqli->query($query))) 
         {
            error_log("ERROR: Failed to select faucets with id_device ".$id_device." .".$mysqli->error);
            return null;
         }
         if ($a_row = $result->fetch_assoc())
         {
            return RO_RLD_Faucet::copy_to_device($a_row);
         }
         else
         {
            error_log("There is no faucet ".$id_device);
            return null;
         }
      }

      static function copy_to_device($a_row)
      {
         $new_faucet = new RO_RLD_Faucet();

         $new_faucet->id_device = (int) $a_row['id_device'];
         $new_faucet->id_room = (int) $a_row['id_room_RLD_Faucets'];
         $new_faucet->id_residence = (int) $a_row['id_residence_RLD_Faucets'];
         $new_faucet->is_info_entered = (int) $a_row['is_info_entered'];
         
         $new_faucet->type_faucet = (int) $a_row['type_faucet'];
         $new_faucet->use_per_day_minutes = (int) $a_row['use_per_day_minutes'];
         $new_faucet->use_per_day_minutes_reduce_to = (int) $a_row['use_per_day_minutes_reduce_to'];

         $new_faucet->load_actions($a_row);
         $new_faucet->action_free_reduce_time = $new_faucet->ro_actions[0];
         $new_faucet->action_upgrade_low_flow = $new_faucet->ro_actions[2];
         $new_faucet->action_upgrade_standard = $new_faucet->ro_actions[3];

         return $new_faucet;
      }

   }
?>
