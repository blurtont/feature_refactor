<?php
   include_once "RO_Device.php";

   class RO_RLD_Oven extends RO_Device
   {
      const name_table = "RLD_Ovens";
      
      public $type_oven;
      public $is_own_toaster_oven;
      public $use_per_week_hours;
      public $uses_per_week;
      public $is_own_microwave;
      public $fraction_use_microwave;
      public $fraction_willing_use_microwave;
      public $is_use_preheat;
      public $is_oven_coasting;

      public $action_free_no_preheat;
      public $action_free_oven_coasting;
      public $action_upgrade_gas_convection_oven;
      public $action_upgrade_electric_convection_oven;
      public $action_upgrade_flashbake_oven;
      public $action_upgrade_toaster_oven;
      public $action_upgrade_microwave_oven;
      
      static function create_default($id_room)
      {
         return RO_RLD_Oven::load(Helper_RTLDs::create_device_and_actions($id_room, "RLD_Ovens", 23));
      }

      static function update(RO_RLD_Oven $device)
      {
         $mysqli = connecti();
         RO_Action::update_actions($device->ro_actions);

         $query = sprintf("UPDATE RLD_Ovens
                           SET id_room_RLD_Ovens='%d',
                               id_residence_RLD_Ovens='%d',
                               is_info_entered='%d',
                               type_oven='%d',
                               is_own_toaster_oven='%d',
                               use_per_week_hours_10x='%d',
                               uses_per_week='%d',
                               is_own_microwave='%d',
                               fraction_use_microwave_100x='%d',
                               fraction_willing_use_microwave_100x='%d',
                               is_use_preheat='%d',
                               is_oven_coasting='%d'
                           WHERE id_device='%d'",
                           mysql_real_escape_string($device->id_room),
                           mysql_real_escape_string($device->id_residence),
                           mysql_real_escape_string($device->is_info_entered),
                           mysql_real_escape_string($device->type_oven),
                           mysql_real_escape_string($device->is_own_toaster_oven),
                           mysql_real_escape_string($device->use_per_week_hours*10),
                           mysql_real_escape_string($device->uses_per_week),
                           mysql_real_escape_string($device->is_own_microwave),
                           mysql_real_escape_string($device->fraction_use_microwave*100),
                           mysql_real_escape_string($device->fraction_willing_use_microwave*100),
                           mysql_real_escape_string($device->is_use_preheat),
                           mysql_real_escape_string($device->is_oven_coasting),
                           mysql_real_escape_string($device->id_device));
         if (!($mysqli->query($query)))
         {
            error_log("Failed to update RO_RLD_Oven with id=".$device->id_device." .".$mysqli->error);
            return null;
         }
      
      }

      static function load_all_in_room($id_room)
      {
         $mysqli = connecti();
         $new_devices = array();
         $query = sprintf("SELECT * FROM RLD_Ovens WHERE id_room_RLD_Ovens='%d'",
                     mysql_real_escape_string($id_room));
         if(!($result = $mysqli->query($query))) 
         { 
            error_log("Failed to select Ovens. ".$mysqli->error);
            return null;
         }
         while($a_row = $result->fetch_assoc())
         {
            array_push($new_devices, RO_RLD_Oven::copy_to_device($a_row));
         }
         return $new_devices;
      }

      static function load($id_device)
      {
         $mysqli = connecti();
         $query = sprintf("SELECT * FROM RLD_Ovens WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if(!($result = $mysqli->query($query)))
         {
            error_log("Failed to select RLD_Oven with id_device ".$id_device." .".$mysqli->error);
            return null;
         }
         if ($a_row = $result->fetch_assoc())
         {
            return RO_RLD_Oven::copy_to_device($a_row);
         }
         else
         {
            error_log ("There is no Oven with ID ".$id_device);
            return null;
         }
      }

      static function copy_to_device($a_row)
      {
         $new_device = new RO_RLD_Oven();

         $new_device->id_device = (int)$a_row['id_device'];
         $new_device->id_room = (int)$a_row['id_room_RLD_Ovens'];
         $new_device->id_residence = (int) $a_row['id_residence_RLD_Ovens'];
         $new_device->is_info_entered = (int) $a_row['is_info_entered'];

         $new_device->type_oven = (int)$a_row['type_oven'];
         $new_device->is_own_toaster_oven = (int)$a_row['is_own_toaster_oven'];
         $new_device->use_per_week_hours = 0.1*$a_row['use_per_week_hours_10x'];
         $new_device->uses_per_week = (int)$a_row['uses_per_week'];
         $new_device->is_own_microwave = (int)$a_row['is_own_microwave'];
         $new_device->fraction_use_microwave = 0.01*$a_row['fraction_use_microwave_100x'];
         $new_device->fraction_willing_use_microwave = 0.01*$a_row['fraction_willing_use_microwave_100x'];
         $new_device->is_use_preheat = (int)$a_row['is_use_preheat'];
         $new_device->is_oven_coasting = (int)$a_row['is_oven_coasting'];

         $new_device->load_actions($a_row);

         $new_device->action_free_no_preheat = $new_device->ro_actions[0];
         $new_device->action_free_oven_coasting = $new_device->ro_actions[1];
         $new_device->action_upgrade_gas_convection_oven = $new_device->ro_actions[2];
         $new_device->action_upgrade_electric_convection_oven = $new_device->ro_actions[3];
         $new_device->action_upgrade_flashbake_oven = $new_device->ro_actions[4];
         $new_device->action_upgrade_toaster_oven = $new_device->ro_actions[5];
         $new_device->action_upgrade_microwave_oven = $new_device->ro_actions[6];
      
         return $new_device;
      }
   }
?>
