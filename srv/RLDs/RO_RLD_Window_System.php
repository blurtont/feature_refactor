<?php
   include_once "RO_Device.php";

   class RO_RLD_Window_System extends RO_Device
   {
      const name_table = "RLD_Window_Systems";
      
      public $type_window;
      public $type_cover_window;
      public $area;

      public $action_upgrade_single_pane; 
      public $action_upgrade_double_pane; 
      public $action_upgrade_triple_pane;
      public $action_upgrade_plastic_film; 
      public $action_upgrade_solar_curtain;
      public $action_upgrade_storm_window;
      public $action_upgrade_no_covers; 

      static function create_default($id_room)
      {
         return RO_RLD_Window_System::load(Helper_RTLDs::create_device_and_actions($id_room, "RLD_Window_Systems", 31));
      }

      static function update($device)
      {
         $mysqli = connecti();
         RO_Action::update_actions($device->ro_actions);

         $query = sprintf("UPDATE RLD_Window_Systems SET
                           id_room_RLD_Window_Systems='%d',
                           id_residence_RLD_Window_Systems='%d',
                           is_info_entered='%d',
                           type_window='%d',
                           type_cover_window='%d',
                           area_sf='%d'
                           WHERE id_device='%d'",
                           mysql_real_escape_string($device->id_room),
                           mysql_real_escape_string($device->id_residence),
                           mysql_real_escape_string($device->is_info_entered),
                           mysql_real_escape_string($device->type_window),
                           mysql_real_escape_string($device->type_cover_window),
                           mysql_real_escape_string($device->area),
                           mysql_real_escape_string($device->id_device));
         if (!($mysqli->query($query))) 
         {
            error_log("failed to update window system with id=".$device->id_device." ".$mysqli->error);
            return null;
         }
      }

      static function load_all_in_room($id_room)
      {
         $mysqli = connecti();
         $new_devices = array();
         $query = sprintf("SELECT * FROM RLD_Window_Systems WHERE id_room_RLD_Window_Systems='%d'",
                     mysql_real_escape_string($id_room));
         if(!($result = $mysqli->query($query))) 
         {
            error_log("Failed to select Windows Systems. ".$mysqli->error);
            return null;
         }
         while($a_row = $result->fetch_assoc())
         {
            array_push($new_devices, RO_RLD_Window_System::copy_to_device($a_row));
         }
         return $new_devices;
      }

      static function load($id_device)
      {
         $mysqli = connecti();
         $query = sprintf("SELECT * FROM RLD_Window_Systems WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if(!($result = $mysqli->query($query))) 
         {
            error_log("Failed to select Windows System with id_device ".$id_device." .".$mysqli->error);
            return null;
         }
         if ($a_row = $result->fetch_assoc())
         {
            return RO_RLD_Window_System::copy_to_device($a_row);
         }
         else
         {
            error_log ("There is no Window System with ID ".$id_device);
            return null;
         }
      }

      static function copy_to_device($a_row)
      {
         $new_device = new RO_RLD_Window_System();
            
         $new_device->id_device = (int)$a_row['id_device'];
         $new_device->id_room = (int)$a_row['id_room_RLD_Window_Systems'];
         $new_device->id_residence = (int)$a_row['id_residence_RLD_Window_Systems'];
         $new_device->is_info_entered = (int) $a_row['is_info_entered'];

         $new_device->type_window = (int)$a_row['type_window'];
         $new_device->area = (int)$a_row['area_sf'];
         $new_device->type_cover_window = (int)$a_row['type_cover_window'];

         $new_device->load_actions($a_row);

         $new_device->action_upgrade_single_pane = $new_device->ro_actions[0];
         $new_device->action_upgrade_double_pane = $new_device->ro_actions[1];
         $new_device->action_upgrade_triple_pane = $new_device->ro_actions[2];
         $new_device->action_upgrade_plastic_film = $new_device->ro_actions[3];
         $new_device->action_upgrade_solar_curtain = $new_device->ro_actions[4];
         $new_device->action_upgrade_storm_window = $new_device->ro_actions[5];
         $new_device->action_upgrade_no_covers = $new_device->ro_actions[6];

         return $new_device;
      }
   }
?>
