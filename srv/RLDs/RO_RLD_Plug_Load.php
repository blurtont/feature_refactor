<?php
   include_once "RO_Device.php";

   class RO_RLD_Plug_Load extends RO_Device
   {
      const name_table = "RLD_Plug_Loads";
      
      public $is_LCD_television;
      public $is_CRT_television;
      public $is_plasma_television;
      public $is_sound_system;
      public $is_gaming_system;
      public $is_printer;
      public $is_desktop_computer;
      public $is_laptop_computer;
      public $is_satellite_cable_box;
      public $is_use_smartstrip;
      public $is_unplug_all;
      public $hours_per_day_standby;

      public $action_free_turn_off_power_strip;
      public $action_free_leave_on_power_strip;
      public $action_upgrade_smart_strip;

      static function create_default($id_room)
      {
         return RO_RLD_Plug_Load::load(Helper_RTLDs::create_device_and_actions($id_room, "RLD_Plug_Loads", 24));
      }

      static function update(RO_RLD_Plug_Load $device)
      {
         $mysqli = connecti();
         RO_Action::update_actions($device->ro_actions);
         
         $query = sprintf("UPDATE RLD_Plug_Loads
                           SET id_room_RLD_Plug_Loads='%d',
                               id_residence_RLD_Plug_Loads='%d',
                               is_info_entered='%d',
                               is_LCD_television='%d',
                               is_CRT_television='%d',
                               is_plasma_television='%d',
                               is_sound_system='%d',
                               is_gaming_system='%d',
                               is_printer='%d',
                               is_desktop_computer='%d',
                               is_laptop_computer='%d',
                               is_satellite_cable_box='%d',
                               is_use_smartstrip='%d',
                               is_unplug_all='%d',
                               hours_per_day_standby_10x='%d'
                           WHERE id_device='%d'",
                           mysql_real_escape_string($device->id_room),
                           mysql_real_escape_string($device->id_residence),
                           mysql_real_escape_string($device->is_info_entered),
                           mysql_real_escape_string($device->is_LCD_television),
                           mysql_real_escape_string($device->is_CRT_television),
                           mysql_real_escape_string($device->is_plasma_television),
                           mysql_real_escape_string($device->is_sound_system),
                           mysql_real_escape_string($device->is_gaming_system),
                           mysql_real_escape_string($device->is_printer),
                           mysql_real_escape_string($device->is_desktop_computer),
                           mysql_real_escape_string($device->is_laptop_computer),
                           mysql_real_escape_string($device->is_satellite_cable_box),
                           mysql_real_escape_string($device->is_use_smartstrip),
                           mysql_real_escape_string($device->is_unplug_all),
                           mysql_real_escape_string($device->hours_per_day_standby*10),
                           mysql_real_escape_string($device->id_device));
         if (!($mysqli->query($query))) 
         {
            error_log("failed to update RO_RLD_Plug_Load with id=".$device->id_device." .".$mysqli->error);
            return null;
         }
      }

      static function load_all_in_room($id_room)
      {
         $mysqli = connecti();
         $new_devices = array();
         $query = sprintf("SELECT * FROM RLD_Plug_Loads WHERE id_room_RLD_Plug_Loads='%d'",
                     mysql_real_escape_string($id_room));
         if(!($result = $mysqli->query($query)))
         {
            error_log("Failed to select Plug Loads. ".$mysqli->error);
            return null;
         }
         while($a_row = $result->fetch_assoc())
         {
            array_push($new_devices, RO_RLD_Plug_Load::copy_to_device($a_row));
         }
         return $new_devices;
      }

      static function load($id_device)
      {
         $mysqli = connecti();
         $query = sprintf("SELECT * FROM RLD_Plug_Loads WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if(!($result = $mysqli->query($query))) 
         {
            error_log("Failed to select Plug Load with id_device ".$id_device." .".$mysqli->error);
            return null;
         }
         if ($a_row = $result->fetch_assoc())
         {
            return RO_RLD_Plug_Load::copy_to_device($a_row);
         }
         else
         {
            error_log ("There is no Plug Load with ID ".$id_device);
            return null;
         }
      }

      static function copy_to_device($a_row)
      {
        $new_device = new RO_RLD_Plug_Load();

        $new_device->id_device = (int)$a_row['id_device'];
        $new_device->id_room = (int)$a_row['id_room_RLD_Plug_Loads'];
        $new_device->id_residence = (int)$a_row['id_residence_RLD_Plug_Loads'];
        $new_device->is_info_entered = (int) $a_row['is_info_entered'];

        $new_device->is_LCD_television = (int)$a_row['is_LCD_television'];
        $new_device->is_CRT_television = (int)$a_row['is_CRT_television'];
        $new_device->is_plasma_television = (int)$a_row['is_plasma_television'];
        $new_device->is_sound_system = (int)$a_row['is_sound_system'];
        $new_device->is_gaming_system = (int)$a_row['is_gaming_system'];
        $new_device->is_printer = (int)$a_row['is_printer'];
        $new_device->is_desktop_computer = (int)$a_row['is_desktop_computer'];
        $new_device->is_laptop_computer = (int)$a_row['is_laptop_computer'];
        $new_device->is_satellite_cable_box = (int)$a_row['is_satellite_cable_box'];
        $new_device->is_use_smartstrip = (int)$a_row['is_use_smartstrip'];
        $new_device->is_unplug_all = (int)$a_row['is_unplug_all'];
        $new_device->hours_per_day_standby = 0.1*$a_row['hours_per_day_standby_10x'];

        $new_device->load_actions($a_row);
               
        $new_device->action_free_turn_off_power_strip = $new_device->ro_actions[0];
        $new_device->action_free_leave_on_power_strip = $new_device->ro_actions[1];
        $new_device->action_upgrade_smart_strip = $new_device->ro_actions[2];
        return $new_device;
      }
   }
?>
