<?php
   include_once "RO_Device.php";

   class RO_RLD_Dish_Washer extends RO_Device
   {
      const name_table = "RLD_Dish_Washers";
                      
      public $type_dishwasher;
      public $fraction_fully_loaded;
      public $number_loads_per_week;
      public $is_electric_dry_used;

      public $action_free_fully_load;
      public $action_free_no_electric_dry;
      public $action_upgrade_new_energy_star;
      public $action_upgrade_standard;

      static function create_default($id_room)
      {
         return RO_RLD_Dish_Washer::load(Helper_RTLDs::create_device_and_actions($id_room, "RLD_Dish_Washers", 13));
      }

      /* save an existing device in the DB */
      static function update($device)
      {   
         RO_Action::update_actions($device->ro_actions);

         $mysqli = connecti();
                            
         $query = sprintf("UPDATE RLD_Dish_Washers SET
                     id_room_RLD_Dish_Washers='%d',
                     id_residence_RLD_Dish_Washers='%d',
                     is_info_entered='%d',
                     type_dishwasher='%d',
                     fraction_fully_loaded_100x='%d',
                     number_loads_per_week='%d',
                     is_electric_dry_used='%d'
                     WHERE id_device='%d'",
                     mysql_real_escape_string($device->id_room),
                     mysql_real_escape_string($device->id_residence),
                     mysql_real_escape_string($device->is_info_entered),
                     mysql_real_escape_string($device->type_dishwasher),
                     mysql_real_escape_string($device->fraction_fully_loaded*100),
                     mysql_real_escape_string($device->number_loads_per_week),
                     mysql_real_escape_string($device->is_electric_dry_used),
                     mysql_real_escape_string($device->id_device));
         if(!($mysqli->query($query)))
         {
            error_log("ERROR: failed to update dish washer with id ".$mysqli->error);
            return null;
         }
      }

      static function load_all_in_room($id_room)
      {
         $mysqli = connecti();

         $new_dish_washers = array();
         $query = sprintf("SELECT * FROM RLD_Dish_Washers WHERE id_room_RLD_Dish_Washers='%d'",
                     mysql_real_escape_string($id_room));
         if (!($result = $mysqli->query($query)))
         {
            error_log("ERROR: Failed to select dish washers. ".$mysqli->error);
            return null;
         }
         while($a_row = $result->fetch_assoc())
         {
            array_push($new_dish_washers, RO_RLD_Dish_Washer::copy_to_device($a_row));
         }
         return $new_dish_washers;
      }

      static function load($id_device)
      {
         $mysqli = connecti();
         $query = sprintf("SELECT * FROM RLD_Dish_Washers WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if (!($result = $mysqli->query($query)))
         {
            error_log("Failed to select dish washer with id_device ".$id_device." .".$mysqli->error.$query);
         }
         if ($a_row = $result->fetch_assoc())
         {
            return RO_RLD_Dish_Washer::copy_to_device($a_row);
         }
         else
         {
            error_log("ERROR: There is no dish_washer".$id_device);
         }
      }

      static function copy_to_device($a_row)
      {
         $new_dish_washer = new RO_RLD_Dish_Washer();

         $new_dish_washer->id_device = (int) $a_row['id_device'];
         
         $new_dish_washer->id_room = (int) $a_row['id_room_RLD_Dish_Washers'];
         $new_dish_washer->id_residence = (int) $a_row['id_residence_RLD_Dish_Washers'];
         $new_dish_washer->is_info_entered = (int) $a_row['is_info_entered'];
         
         $new_dish_washer->type_dishwasher = (int) $a_row['type_dishwasher'];
         $new_dish_washer->fraction_fully_loaded = $a_row['fraction_fully_loaded_100x']/100.0;
         $new_dish_washer->number_loads_per_week = (int) $a_row['number_loads_per_week'];
         $new_dish_washer->is_electric_dry_used = $a_row['is_electric_dry_used'];

         $new_dish_washer->load_actions($a_row);

         $new_dish_washer->action_free_fully_load= $new_dish_washer->ro_actions[0];
         $new_dish_washer->action_free_no_electric_dry = $new_dish_washer->ro_actions[1];
         $new_dish_washer->action_upgrade_new_energy_star = $new_dish_washer->ro_actions[2];
         $new_dish_washer->action_upgrade_standard = $new_dish_washer->ro_actions[3];
         return $new_dish_washer; 
      }

   }
?>
