<?php
   include_once "RO_Device.php";

   class RO_RLD_Clothes_Dryer extends RO_Device
   {
      const name_table = "RLD_Clothes_Dryers";

      public $type_dryer;
      public $fraction_line_dry;
      public $loads_per_week;
      public $is_spin_twice_in_wash_cycle;

      public $action_free_clothes_line_dry;
      public $action_free_spin_twice_with_clothes_washer;
      public $action_upgrade_spinner;
      public $action_upgrade_dryer_with_moisture_sensor;
      public $action_upgrade_dryer_standard;

      static function create_default($id_room)
      {
         return RO_RLD_Clothes_Dryer::load(Helper_RTLDs::create_device_and_actions($id_room, "RLD_Clothes_Dryers", 8));
      }

      /* save an existing device in the DB */
      static function update($device)
      {
         $mysqli = connecti();
         RO_Action::update_actions($device->ro_actions);

         $query = sprintf("UPDATE RLD_Clothes_Dryers SET
                     id_room_RLD_Clothes_Dryers='%d',
                     id_residence_RLD_Clothes_Dryers='%d',
                     is_info_entered='%d',
                     type_dryer='%d',
                     fraction_line_dry_100x='%d',
                     loads_per_week='%d',
                     is_spin_twice_in_wash_cycle='%d'
                     WHERE id_device='%d'",
                     mysql_real_escape_string($device->id_room),
                     mysql_real_escape_string($device->id_residence),
                     mysql_real_escape_string($device->is_info_entered),
                     mysql_real_escape_string($device->type_dryer),
                     mysql_real_escape_string($device->fraction_line_dry * 100),
                     mysql_real_escape_string($device->loads_per_week),
                     mysql_real_escape_string($device->is_spin_twice_in_wash_cycle),
                     mysql_real_escape_string($device->id_device));
         if (!$mysqli->query($query))
         {
            error_log("ERROR: Failed to update RLD_Clothes_Dryer. ".$mysqli->error);
            return;
         }
      }

      /* return array of clothes dryers in $id_room */
      static function load_all_in_room($id_room)
      {
         $mysqli = connecti();
         $new_clothes_dryers = array();

         /* get clothes dryers in $id_room */
         $query = sprintf("SELECT * FROM RLD_Clothes_Dryers WHERE id_room_RLD_Clothes_Dryers='%d'",
                     mysql_real_escape_string($id_room));
         if (!($result_devices = $mysqli->query($query)))
         {
            error_log("ERROR: Failed to select clothes dryers. ".$mysqli->error);
            return null;
         }
         while ($a_row = $result_devices->fetch_assoc())
         {
            array_push($new_clothes_dryers, RO_RLD_Clothes_Dryer::copy_to_device($a_row));
         }
         return $new_clothes_dryers;
      }

      /* return RO_RLD_Clothes_Dryer with $id_device */
      static function load($id_device)
      {
         $mysqli = connecti();
         $query = sprintf("SELECT * FROM RLD_Clothes_Dryers WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if (!($result = $mysqli->query($query)))
         {
            error_log("Failed to select clothes dryer. ".$mysqli->error);
            return null;
         }
         if ($a_row = $result->fetch_assoc())
         {
            return RO_RLD_Clothes_Dryer::copy_to_device($a_row);
         }
         else
         {
            error_log("ERROR: there is no Clothes Dryer ".$id_device);
         }
      }

      /* copy $a_row to new device */
      static function copy_to_device($a_row)
      {
         $new_clothes_dryer = new RO_RLD_Clothes_Dryer();
         
         $new_clothes_dryer->id_device = (int) $a_row['id_device'];
         $new_clothes_dryer->id_room = (int) $a_row['id_room_RLD_Clothes_Dryers'];
         $new_clothes_dryer->id_residence = (int) $a_row['id_residence_RLD_Clothes_Dryers'];
         $new_clothes_dryer->is_info_entered = (int) $a_row['is_info_entered'];

         $new_clothes_dryer->type_dryer = (int) $a_row['type_dryer'];
         $new_clothes_dryer->fraction_line_dry = $a_row['fraction_line_dry_100x']/100.0;
         $new_clothes_dryer->loads_per_week = (int) $a_row['loads_per_week'];
         $new_clothes_dryer->is_spin_twice_in_wash_cycle = (int) $a_row['is_spin_twice_in_wash_cycle'];

         $new_clothes_dryer->load_actions($a_row);

         $new_clothes_dryer->action_free_clothes_line_dry = $new_clothes_dryer->ro_actions[0];
         $new_clothes_dryer->action_free_spin_twice_with_clothes_washer = $new_clothes_dryer->ro_actions[1];
         $new_clothes_dryer->action_upgrade_spinner = $new_clothes_dryer->ro_actions[2]; 
         $new_clothes_dryer->action_upgrade_dryer_with_moisture_sensor = $new_clothes_dryer->ro_actions[3]; 
         $new_clothes_dryer->action_upgrade_dryer_standard = $new_clothes_dryer->ro_actions[4]; 
         return $new_clothes_dryer;
     }
     
   }
?>
