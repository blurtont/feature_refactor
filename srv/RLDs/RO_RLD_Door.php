<?php
   include_once "RO_Device.php";

   class RO_RLD_Door extends RO_Device
   {
      const name_table = "RLD_Doors";
      
      public $type_door;
      public $has_storm_door;
      public $area_sf;

      public $action_upgrade_wood_hollow_core;
      public $action_upgrade_wood_solid_one_inch;
      public $action_upgrade_wood_solid_one_and_one_half_inches;
      public $action_upgrade_solid_one_and_three_quarters_inches;
      public $action_upgrade_wood_solid_two_inches;
      public $action_upgrade_metal;
      public $action_upgrade_metal_insulating;
      public $action_upgrade_storm;

      static function create_default($id_room)
      {
         return RO_RLD_Door::load(Helper_RTLDs::create_device_and_actions($id_room, "RLD_Doors", 14));
      }

      /* save an existing device in the DB */
      static function update($device)
      {   
         RO_Action::update_actions($device->ro_actions);

         $mysqli = connecti();
                               
         $query = sprintf("UPDATE RLD_Doors SET
                     id_room_RLD_Doors='%d',
                     id_residence_RLD_Doors='%d',
                     is_info_entered='%d',
                     type_door='%d'
                     has_storm_door='%d'
                     area_sf='%d'
                     WHERE id_device='%d'",
                     mysql_real_escape_string($device->id_room),
                     mysql_real_escape_string($device->id_residence),
                     mysql_real_escape_string($device->is_info_entered),
                     mysql_real_escape_string($device->type_door),
                     mysql_real_escape_string($device->has_storm_door),
                     mysql_real_escape_string($device->area_sf),
                     mysql_real_escape_string($device->id_device));
         if (!($mysqli->query($query))) 
         { 
            error_log("failed to update door".$mysqli->error);
         }
      }

      static function load_all_in_room($id_room)
      {
         $mysqli = connecti();
         $new_doors = array();
         $query = sprintf("SELECT * FROM RLD_Doors WHERE id_room_RLD_Doors='%d'",
                     mysql_real_escape_string($id_room));
         if(!($result = $mysqli->query($query))) 
         { 
            error_log("Failed to select doors. ".$mysqli->error);
            return null;
         }
         while($a_row = $result->fetch_assoc())
         {
            array_push($new_doors, RO_RLD_Door::copy_to_device($a_row));
         }
         return $new_doors;
      }

      static function load($id_device)
      {
         $mysqli = connecti();

         $query = sprintf("SELECT * FROM RLD_Doors WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if(!($result = $mysqli->query($query))) 
         { 
            error_log("Failed to select Door with id_device ".$id_device." .".$mysqli->error);
            return null;
         }
         if ($a_row = $result->fetch_assoc())
         {
            return RO_RLD_Door::copy_to_device($a_row);
         }
         else
         {
            error_log("ERROR: There is no door ".$id_device);
            return null;
         }
      }

      static function copy_to_device($a_row)
      {
         $new_door = new RO_RLD_Door();

         $new_door->id_device = (int) $a_row['id_device'];
         $new_door->id_room = (int) $a_row['id_room_RLD_Doors'];
         $new_door->id_residence = (int) $a_row['id_residence_RLD_Doors'];
         $new_door->is_info_entered = (int) $a_row['is_info_entered'];

         $new_door->type_door = (int) $a_row['type_door'];
         $new_door->has_storm_door = (int) $a_row['has_storm_door'];
         $new_door->area_sf = (int) $a_row['area_sf'];

         $new_door->load_actions($a_row);

         return $new_door;
      }

   }
?>
