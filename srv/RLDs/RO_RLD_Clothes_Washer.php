<?php
   include_once "RO_Device.php";

   class RO_RLD_Clothes_Washer extends RO_Device
   {
      const name_table = "RLD_Clothes_Washers";
      
      public $loads_per_week;
      public $fraction_hot_water_wash;
      public $fraction_warm_water_wash;
      public $type_washer;

      public $action_free_cold_water_wash;
      public $action_upgrade_front_loading_clothes_washer;
      public $action_upgrade_top_loading_clothes_washer;

      static function create_default($id_room)
      {
         return RO_RLD_Clothes_Washer::load(Helper_RTLDs::create_device_and_actions($id_room, "RLD_Clothes_Washers", 9));
      }

      /* save an existing device in the DB */
      static function update($device)
      {   
         RO_Action::update_actions($device->ro_actions);

         $mysqli = connecti();
         $query = sprintf("UPDATE RLD_Clothes_Washers SET
                     id_room_RLD_Clothes_Washers='%d',
                     id_residence_RLD_Clothes_Washers='%d',
                     is_info_entered='%d',
                     loads_per_week='%d',
                     fraction_hot_water_wash_100x='%d',
                     fraction_warm_water_wash_100x='%d',
                     type_washer='%d'
                     WHERE id_device='%d'",
                     mysql_real_escape_string($device->id_room),
                     mysql_real_escape_string($device->id_residence),
                     mysql_real_escape_string($device->is_info_entered),
                     mysql_real_escape_string($device->loads_per_week),
                     mysql_real_escape_string($device->fraction_hot_water_wash*100),
                     mysql_real_escape_string($device->fraction_warm_water_wash*100),
                     mysql_real_escape_string($device->type_washer),
                     mysql_real_escape_string($device->id_device));
         if (!($mysqli->query($query)))
         {
            error_log("ERROR: failed to update clothes washer with id ". $id_device);
         }
      }
      /* return an array of clothes washers in $id_room */
      static function load_all_in_room($id_room)
      {
         $mysqli = connecti();
         $new_clothes_washers = array();

         /* get clothes washers in $id_room */
         $query = sprintf("SELECT * FROM RLD_Clothes_Washers WHERE id_room_RLD_Clothes_Washers='%d'",
                     mysql_real_escape_string($id_room));
         if (!($result = $mysqli->query($query)))
         {
            error_log("ERROR: Failed to select clothes washers. ".mysql_error());
         }

         while($a_row = $result->fetch_assoc())
         {
            array_push($new_clothes_washers, RO_RLD_Clothes_Washer::copy_to_device($a_row));
         }

         return $new_clothes_washers;
      }

      /* return RO_RLD_Clothes_Washer with $id_device */
      static function load($id_device)
      {
         $mysqli = connecti();
         $query = sprintf("SELECT * FROM RLD_Clothes_Washers WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if (!($result = $mysqli->query($query)))
         {
            error_log("ERROR: Failed to select clothes washer. ".$myqsli->error);
         }
         if ($a_row = $result->fetch_assoc())
         {
            return RO_RLD_Clothes_Washer::copy_to_device($a_row);
         }
         else
         {
            error_log("ERROR: There is no Clothes Washer ".$id_device);
         }
      }

      /* copy $a_row to new device */
      static function copy_to_device($a_row)
      {
         $new_clothes_washer = new RO_RLD_Clothes_Washer();
         
         $new_clothes_washer->id_device  = (int) $a_row['id_device'];
         $new_clothes_washer->id_room = (int) $a_row['id_room_RLD_Clothes_Washers'];
         $new_clothes_washer->id_residence = (int) $a_row['id_residence_RLD_Clothes_Washers'];
         $new_clothes_washer->is_info_entered = (int) $a_row['is_info_entered'];
         
         $new_clothes_washer->loads_per_week = (int) $a_row['loads_per_week'];
         $new_clothes_washer->fraction_hot_water_wash = $a_row['fraction_hot_water_wash_100x'] / 100.0;
         $new_clothes_washer->fraction_warm_water_wash = $a_row['fraction_warm_water_wash_100x'] / 100.0;
         $new_clothes_washer->type_washer = (int) $a_row['type_washer'];

         $new_clothes_washer->load_actions($a_row);

         $new_clothes_washer->action_free_cold_water_wash = $new_clothes_washer->ro_actions[0];
         $new_clothes_washer->action_upgrade_front_loading_clothes_washer = $new_clothes_washer->ro_actions[1];
         $new_clothes_washer->action_upgrade_top_loading_clothes_washer = $new_clothes_washer->ro_actions[2];
         return $new_clothes_washer;
     }

   }
?>
