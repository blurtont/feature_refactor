<?php
   include_once "RO_Device.php";

   class RO_RLD_Light extends RO_Device
   {
      const name_table = "RLD_Lights";
      
      public $use_per_day_hours;
      public $use_per_day_hours_reduce_to;
      public $is_turn_off_not_in_room;
      public $fraction_fluorescent;
      public $fraction_LED;
      public $fraction_incandescent;

      public $action_free_turn_out_when_not_in_room;
      public $action_free_leave_on_during_day;
      public $action_upgrade_fluorescent;
      public $action_upgrade_LED;
      public $action_upgrade_incandescent;

      static function create_default($id_room)
      {
         return RO_RLD_Light::load(Helper_RTLDs::create_device_and_actions($id_room, "RLD_Lights", 22));
      }

     /* save an existing device in the DB */
      static function update($device)
      {   
         RO_Action::update_actions($device->ro_actions);
       
         $mysqli = connecti();
         $query = sprintf("UPDATE RLD_Lights SET
                     id_room_RLD_Lights='%d',
                     id_residence_RLD_Lights='%d',
                     is_info_entered='%d',
                     use_per_day_hours_10x='%d',
                     use_per_day_hours_reduce_to_10x='%d',
                     is_turn_off_not_in_room='%d',
                     fraction_incandescent_100x='%d',
                     fraction_fluorescent_100x='%d',
                     fraction_led_100x='%d'
                     WHERE id_device='%d'",
                     mysql_real_escape_string($device->id_room),
                     mysql_real_escape_string($device->id_residence),
                     mysql_real_escape_string($device->is_info_entered),
                     mysql_real_escape_string($device->use_per_day_hours*10),
                     mysql_real_escape_string($device->use_per_day_hours_reduce_to*10),
                     mysql_real_escape_string($device->is_turn_off_not_in_room),
                     mysql_real_escape_string($device->fraction_incandescent*100),
                     mysql_real_escape_string($device->fraction_fluorescent*100),
                     mysql_real_escape_string($device->fraction_LED*100),
                     mysql_real_escape_string($device->id_device));
         if (!($mysqli->query($query))) 
         { 
            error_log("failed to update lights ".$mysqli->error);
         }
      }
      static function load_all_in_room($id_room)
      {
         $mysqli = connecti();

         $new_devices = array();
         $query = sprintf("SELECT * FROM RLD_Lights WHERE id_room_RLD_Lights='%d'",
                     mysql_real_escape_string($id_room));
         if(!($result = $mysqli->query($query)))
         {
            error_log("ERROR: Failed to select lights. ".$mysqli->error);
            return null;
         }
         while($a_row = $result->fetch_assoc())
         {
            array_push($new_devices, RO_RLD_Light::copy_to_device($a_row));
         }
         return $new_devices;
      }

      static function load($id_device)
      {
         $mysqli = connecti();
         $query = sprintf("SELECT * FROM RLD_Lights WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if(!($result = $mysqli->query($query))) 
         { 
            error_log("Failed to select Light with id_device ".$id_device." .".$mysqli->error);
         }
         if ($a_row = $result->fetch_assoc())
         {
            return RO_RLD_Light::copy_to_device($a_row);
         }
         else
         {
            error_log ("There is no light ".$id_device);
         }
      }

      static function copy_to_device($a_row)
      {
         $new_light = new RO_RLD_Light();

         $new_light->id_device = (int) $a_row['id_device'];
         $new_light->id_room = (int) $a_row['id_room_RLD_Lights'];
         $new_light->id_residence = (int) $a_row['id_residence_RLD_Lights'];
         $new_light->is_info_entered = (int) $a_row['is_info_entered'];
         
         $new_light->use_per_day_hours = $a_row['use_per_day_hours_10x']/10.0;
         $new_light->use_per_day_hours_reduce_to = $a_row['use_per_day_hours_reduce_to_10x']/10.0;
         $new_light->is_turn_off_not_in_room = (int) $a_row['is_turn_off_not_in_room'];
         $new_light->fraction_incandescent = $a_row['fraction_incandescent_100x']/100.0;
         $new_light->fraction_fluorescent = $a_row['fraction_fluorescent_100x']/100.0;
         $new_light->fraction_LED = $a_row['fraction_led_100x']/100.0;

         $new_light->load_actions($a_row);

         $new_light->action_free_turn_out_when_not_in_room = $new_light->ro_actions[0];
         $new_light->action_free_leave_on_during_day = $new_light->ro_actions[1];
         $new_light->action_upgrade_fluorescent = $new_light->ro_actions[2];
         $new_light->action_upgrade_LED = $new_light->ro_actions[3];
         $new_light->action_upgrade_incandescent = $new_light->ro_actions[4];
 
         return $new_light;
      }

   }

?>
