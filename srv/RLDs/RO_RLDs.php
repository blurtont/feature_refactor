<?php
include_once 'INCLUDE_RO_RLDs.php';

class RO_RLDs
{
   public $rld_clothes_dryers;
   public $rld_clothes_washers;
   public $rld_coffee_makers;
   public $rld_cooktops;
   public $rld_dish_washers;
   public $rld_doors;
   public $rld_faucets;
   public $rld_lights;
   public $rld_local_cooling_systems;
   public $rld_local_heating_systems;
   public $rld_ovens;
   public $rld_refrigerators;
   public $rld_showers;
   public $rld_window_systems;
   public $rld_plug_loads;

   static function load_rlds($id_room)
   {
      $new_rlds = new RO_RLDs();
      $new_rlds->rld_clothes_dryers = RO_RLD_Clothes_Dryer::load_all_in_room($id_room);
      $new_rlds->rld_clothes_washers = RO_RLD_Clothes_Washer::load_all_in_room($id_room);
      $new_rlds->rld_coffee_makers = RO_RLD_Coffee_Maker::load_all_in_room($id_room);
      $new_rlds->rld_cooktops = RO_RLD_Cooktop::load_all_in_room($id_room);
      $new_rlds->rld_dish_washers = RO_RLD_Dish_Washer::load_all_in_room($id_room);
      $new_rlds->rld_doors = RO_RLD_Door::load_all_in_room($id_room);
      $new_rlds->rld_faucets = RO_RLD_Faucet::load_all_in_room($id_room);
      $new_rlds->rld_lights = RO_RLD_Light::load_all_in_room($id_room);
      $new_rlds->rld_local_cooling_systems = RO_RLD_Local_Cooling_System::load_all_in_room($id_room);
      $new_rlds->rld_local_heating_systems = RO_RLD_Local_Heating_System::load_all_in_room($id_room);
      $new_rlds->rld_ovens = RO_RLD_Oven::load_all_in_room($id_room);
      $new_rlds->rld_refrigerators = RO_RLD_Refrigerator::load_all_in_room($id_room);
      $new_rlds->rld_showers = RO_RLD_Shower::load_all_in_room($id_room);
      $new_rlds->rld_window_systems = RO_RLD_Window_System::load_all_in_room($id_room); 
      $new_rlds->rld_plug_loads = RO_RLD_Plug_Load::load_all_in_room($id_room);
      return $new_rlds;
   }

   public function get_devices()
   {
      $devices = array_merge($this->rld_clothes_dryers,
                        $this->rld_clothes_washers,
                        $this->rld_coffee_makers,
                        $this->rld_cooktops,
                        $this->rld_dish_washers,
                        $this->rld_doors,
                        $this->rld_faucets,
                        $this->rld_lights,
                        $this->rld_local_cooling_systems,
                        $this->rld_local_heating_systems,
                        $this->rld_ovens,
                        $this->rld_refrigerators,
                        $this->rld_showers,
                        $this->rld_window_systems,
                        $this->rld_plug_loads);
      return $devices;
   }
   
   public function duplicate($id_room_dup)
   {
      $devices = $this->get_devices();
      foreach ($devices as $device)
      {
         $device->duplicate($id_room_dup);   
      }
   }

   static function update($ro_rlds)
   {
      $devices = $ro_rlds->get_devices();
      foreach($devices as $device)
      {
	 if($device->invalidated)
	 {
	     $device::update($device);
	 }
      }
   }

   public function remove()
   {   
      $devices = $this->get_devices();
      foreach($devices as $device)
      {   
         $device->remove();
      }  
   }
}
?>
