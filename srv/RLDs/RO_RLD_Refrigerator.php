<?php
   include_once "RO_Device.php";
   
   class RO_RLD_Refrigerator extends RO_Device
   {
      const name_table = "RLD_Refrigerators";
      
      public $type_size;
      public $energy_factor;
      public $energy_factor_max;
      public $is_primary;
      public $is_willing_decomission;

      public $action_free_decommission_extra_fridge;
      public $action_upgrade_standard_fridge;
      public $action_upgrade_energy_star_fridge;

      static function create_default($id_room)
      {
         return RO_RLD_Refrigerator::load(Helper_RTLDs::create_device_and_actions($id_room, "RLD_Refrigerators", 25));
      }

      static function update(RO_RLD_Refrigerator $device)
      {
         $mysqli = connecti();
         RO_Action::update_actions($device->ro_actions);

         $query = sprintf("UPDATE RLD_Refrigerators
                           SET id_room_RLD_Refrigerators='%d',
                               id_residence_RLD_Refrigerators='%d',
                               is_info_entered='%d',
                               type_size='%d',
                               energy_factor_100x='%d',
                               energy_factor_max_10x='%d',
                               is_primary='%d',
                               is_willing_decomission='%d'
                           WHERE id_device='%d'",
                           mysql_real_escape_string($device->id_room),
                           mysql_real_escape_string($device->id_residence),
                           mysql_real_escape_string($device->is_info_entered),
                           mysql_real_escape_string($device->type_size),
                           mysql_real_escape_string($device->energy_factor*100),
                           mysql_real_escape_string($device->energy_factor_max*10),
                           mysql_real_escape_string($device->is_primary),
                           mysql_real_escape_string($device->is_willing_decomission),
                           mysql_real_escape_string($device->id_device));
         if (!($mysqli->query($query))) 
         {
            error_log("Failed to update RO_RLD_Refrigerator with id=".$device->id_device." .".myqsl_error());
            return null;
         }
      }

      static function load_all_in_room($id_room)
      {
         $mysqli = connecti();
         $new_devices = array();
         $query = sprintf("SELECT * FROM RLD_Refrigerators WHERE id_room_RLD_Refrigerators='%d'",
                     mysql_real_escape_string($id_room));
         if(!($result = $mysqli->query($query))) 
         { 
            error_log("Failed to select Refrigerators. ".$mysqli->error);
            return null;
         }
         while($a_row = $result->fetch_assoc())
         {
            array_push($new_devices, RO_RLD_Refrigerator::copy_to_device($a_row));
         }
         return $new_devices;
      }

      static function load($id_device)
      {
         $mysqli = connecti();
         $query = sprintf("SELECT * FROM RLD_Refrigerators WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if(!($result = $mysqli->query($query))) 
         {
            error_log("Failed to select Refrigerator with id_device ".$id_device." .".$mysqli->error);
            return null;
         }
         if ($a_row = $result->fetch_assoc())
         {
            return RO_RLD_Refrigerator::copy_to_device($a_row);
         }
         else
         {
            error_log ("There is no Refrigerator with ID ".$id_device);
            return null;
         }
      }

      static function copy_to_device($a_row)
      {
         $new_device = new RO_RLD_Refrigerator();

         $new_device->id_device = (int)$a_row['id_device'];
         $new_device->id_room = (int)$a_row['id_room_RLD_Refrigerators'];
         $new_device->id_residence = (int)$a_row['id_residence_RLD_Refrigerators'];
         $new_device->is_info_entered = (int) $a_row['is_info_entered'];
         $new_device->is_owner_only = true;

         $new_device->type_size = $a_row['type_size'];
         $new_device->energy_factor = 0.01*$a_row['energy_factor_100x'];
         $new_device->energy_factor_max = 0.1*$a_row['energy_factor_max_10x'];
         $new_device->is_primary = (int)$a_row['is_primary'];
         $new_device->is_willing_decomission = (int)$a_row['is_willing_decomission'];

         $new_device->load_actions($a_row);
         $new_device->action_free_decommission_extra_fridge = $new_device->ro_actions[0];
         $new_device->action_upgrade_standard_fridge = $new_device->ro_actions[1];
         $new_device->action_upgrade_energy_star_fridge = $new_device->ro_actions[2];
                  
         return $new_device;
      }
   }
?>
