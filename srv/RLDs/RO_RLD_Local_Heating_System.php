<?php
   include_once "RO_Device.php";

   class RO_RLD_Local_Heating_System extends RO_Device
   {
      const name_table = "RLD_Local_Heating_Systems";
      
      public $type_heat;
      public $is_use_space_heater;
      public $efficiency;
      public $is_space_heated_room_only;
      public $hours_use_daily_average;
      
      public $action_free_quit_space_heating_room;
      public $action_upgrade_heat_pump_high_efficiency;
      public $action_upgrade_heat_pump_standard_efficiency;
      public $action_upgrade_baseboard;
      public $action_upgrade_space_heater;

      static function create_default($id_room)
      {
         return RO_RLD_Local_Heating_System::load(Helper_RTLDs::create_device_and_actions($id_room, "RLD_Local_Heating_Systems", 20));
      }

      static function update($device)
      {
         RO_Action::update_actions($device->ro_actions);

         $mysqli = connecti();
         $query = sprintf("UPDATE RLD_Local_Heating_Systems
                           SET id_room_RLD_Local_Heating_Systems='%d',
                               id_residence_RLD_Local_Heating_Systems='%d',
                               is_info_entered='%d',
                               type_heat='%d',
                               is_use_space_heater='%d',
                               efficiency='%d',
                               is_space_heated_room_only='%d',
                               hours_use_daily_average='%d'
                           WHERE id_device='%d'",
                           mysql_real_escape_string($device->id_room),
                           mysql_real_escape_string($device->id_residence),
                           mysql_real_escape_string($device->is_info_entered),
                           mysql_real_escape_string($device->type_heat),
                           mysql_real_escape_string($device->is_use_space_heater),
                           mysql_real_escape_string($device->efficiency),
                           mysql_real_escape_string($device->is_space_heated_room_only),
                           mysql_real_escape_string($device->hours_use_daily_average),
                           mysql_real_escape_string($device->id_device));
         if (!($mysqli->query($query)))
         {
            error_log("Failed to update RO_RLD_Local_Heating_System with id=".$device->id_device." .".$mysqli->error);
         }
      
      }

      static function load_all_in_room($id_room)
      {
         $mysqli = connecti();
         $new_local_heating_systems = array();
         $query = sprintf("SELECT * FROM RLD_Local_Heating_Systems WHERE id_room_RLD_Local_Heating_Systems='%d'",
                     mysql_real_escape_string($id_room));
         if(!($result = $mysqli->query($query))) 
         { 
            error_log("Failed to select local heating systems. ".$mysqli->error);
            return null;
         }
         while($a_row = $result->fetch_assoc())
         {
            array_push($new_local_heating_systems, RO_RLD_Local_Heating_System::copy_to_device($a_row));
         }
         return $new_local_heating_systems;
      }

      static function load($id_device)
      {
         $mysqli = connecti();
         $query = sprintf("SELECT * FROM RLD_Local_Heating_Systems WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if(!($result = $mysqli->query($query))) 
         { 
            error_log("Failed to select local heating system  with id_device ".$id_device." .".$mysqli->error);
            return null;
         }
         if ($a_row = $result->fetch_assoc())
         {
            return RO_RLD_Local_Heating_System::copy_to_device($a_row);
         }
         else
         {
            error_log ("There is no local heating system ".$id_device);
            return null;
         }
      }

      static function copy_to_device($a_row)
      {
         $new_local_heating_system = new RO_RLD_Local_Heating_System();
         
         $new_local_heating_system->id_device = (int) $a_row['id_device'];
         $new_local_heating_system->id_room = (int) $a_row['id_room_RLD_Local_Heating_Systems'];
         $new_local_heating_system->id_residence = (int) $a_row['id_residence_RLD_Local_Heating_Systems'];
         $new_local_heating_system->is_info_entered = (int) $a_row['is_info_entered'];
         
         $new_local_heating_system->type_heat = (int) $a_row['type_heat'];
         $new_local_heating_system->is_use_space_heater = (int) $a_row['is_use_space_heater'];
         $new_local_heating_system->efficiency = (int) $a_row['efficiency'];
         $new_local_heating_system->is_space_heated_room_only = (int) $a_row['is_space_heated_room_only'];
         $new_local_heating_system->hours_use_daily_average = (int) $a_row['hours_use_daily_average'];

         $new_local_heating_system->load_actions($a_row);
         return $new_local_heating_system;
      }
   }
?>
