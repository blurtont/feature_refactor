<?php
   include_once "RO_Device.php";

   class RO_RLD_Coffee_Maker extends RO_Device
   {
      const name_table = "RLD_Coffee_Makers";
     
      public $type_coffee_maker;
      public $pots_per_week;
      public $cups_per_pot;
      public $cups_per_pot_used;
      public $is_turn_off_after_brewing; 
      public $is_insulated_carafe;
      public $warm_time_hours;

      public $action_free_turns_off_after_brewing;
      public $action_free_leave_on_after_brewing;
      public $action_upgrade_single_cup;
      public $action_upgrade_carafe_insulated;
      public $action_upgrade_standard;

      static function create_default($id_room)
      {
         return RO_RLD_Coffee_Maker::load(Helper_RTLDs::create_device_and_actions($id_room, "RLD_Coffee_Makers", 10));
      }

     /* save an existing device in the DB */
      static function update($device)
      {   
         RO_Action::update_actions($device->ro_actions);

         $mysqli = connecti();
       
         $query = sprintf("UPDATE RLD_Coffee_Makers SET
                     id_room_RLD_Coffee_Makers='%d',
                     id_residence_RLD_Coffee_Makers='%d',
                     is_info_entered='%d',
                     type_coffee_maker='%d',
                     num_pots_per_week='%d',
                     num_cups_per_pot='%d',
                     num_cups_used='%d',
                     off_after_brew='%d',
                     is_insulated_carafe='%d',
                     warm_time_hours_10x='%d'
                     WHERE id_device='%d'",
                     mysql_real_escape_string($device->id_room),
                     mysql_real_escape_string($device->id_residence),
                     mysql_real_escape_string($device->is_info_entered),
                     mysql_real_escape_string($device->type_coffee_maker),
                     mysql_real_escape_string($device->pots_per_week),
                     mysql_real_escape_string($device->cups_per_pot),
                     mysql_real_escape_string($device->cups_per_pot_used),
                     mysql_real_escape_string($device->is_turn_off_after_brewing),
                     mysql_real_escape_string($device->is_insulated_carafe),
                     mysql_real_escape_string($device->warm_time_hours*10),
                     mysql_real_escape_string($device->id_device));
         if (!$mysqli->query($query))
         {
            error_log("ERROR: failed to update coffee maker with id ".$mysqli-error);
         }
      }   

      /* return array of coffee makers in $id_room */
      static function load_all_in_room($id_room)
      {
         $new_coffee_makers = array();
         $mysqli = connecti();
         /* get coffee makers in $id_room */
         $query = sprintf("SELECT * FROM RLD_Coffee_Makers WHERE id_room_RLD_Coffee_Makers='%d'",
                     mysql_real_escape_string($id_room));
         if (!($result = $mysqli->query($query)))
         {
            error_log("Failed to select coffee makers. ".$mysqli->error);
         }
         while ($a_row = $result->fetch_assoc())
         {
            array_push($new_coffee_makers, RO_RLD_Coffee_Maker::copy_to_device($a_row));
         }
         return $new_coffee_makers;
      }

      /* return RO_RLD_Coffee_Maker with $id_device */
      static function load($id_device)
      {
         $mysqli = connecti();
         $query = sprintf("SELECT * FROM RLD_Coffee_Makers WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if (!($result = $mysqli->query($query)))
         {
            error_log("ERROR: Failed to select coffee maker with id_device ".$id_device." .".$mysqli->error);
            return null;
         }
         if ($a_row = $result->fetch_assoc())
         {
            return RO_RLD_Coffee_Maker::copy_to_device($a_row);
         }
         else
         {
            error_log("There is no Coffee Maker ".$id_device);
            return null;
         }
     }

     /* copy $a_row to new device */
     static function copy_to_device($a_row)
     {
         $new_coffee_maker = new RO_RLD_Coffee_Maker();

         $new_coffee_maker->id_device = (int) $a_row['id_device'];
         $new_coffee_maker->id_room = (int) $a_row['id_room_RLD_Coffee_Makers'];
         $new_coffee_maker->id_residence = (int) $a_row['id_residence_RLD_Coffee_Makers'];
         $new_coffee_maker->is_info_entered = (int) $a_row['is_info_entered'];

         $new_coffee_maker->type_coffee_maker = (int) $a_row['type_coffee_maker'];
         $new_coffee_maker->pots_per_week = (int) $a_row['num_pots_per_week'];
         $new_coffee_maker->cups_per_pot = (int) $a_row['num_cups_per_pot'];
         $new_coffee_maker->cups_per_pot_used = (int) $a_row['num_cups_used'];
         $new_coffee_maker->is_turn_off_after_brewing = (int) $a_row['off_after_brew'];
         $new_coffee_maker->is_insulated_carafe = (int) $a_row['is_insulated_carafe'];
         $new_coffee_maker->warm_time_hours = $a_row['warm_time_hours_10x']/10.0;

         $new_coffee_maker->load_actions($a_row);
         $new_coffee_maker->action_free_turns_off_after_brewing = $new_coffee_maker->ro_actions[0];
         $new_coffee_maker->action_free_leave_on_after_brewing = $new_coffee_maker->ro_actions[1];
         $new_coffee_maker->action_upgrade_single_cup = $new_coffee_maker->ro_actions[2];
         $new_coffee_maker->action_upgrade_carafe_insulated = $new_coffee_maker->ro_actions[3];
         $new_coffee_maker->action_upgrade_standard = $new_coffee_maker->ro_actions[4];
         
         return $new_coffee_maker;
     }

   }
?>
