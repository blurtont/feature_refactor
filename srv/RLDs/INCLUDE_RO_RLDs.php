<?php
include_once 'RO_RLD_Clothes_Dryer.php';
include_once 'RO_RLD_Clothes_Washer.php';
include_once 'RO_RLD_Coffee_Maker.php';
include_once 'RO_RLD_Cooktop.php';
include_once 'RO_RLD_Dish_Washer.php';
include_once 'RO_RLD_Door.php';
include_once 'RO_RLD_Faucet.php';
include_once 'RO_RLD_Light.php';
include_once 'RO_RLD_Local_Cooling_System.php';
include_once 'RO_RLD_Local_Heating_System.php';
include_once 'RO_RLD_Oven.php';
include_once 'RO_RLD_Plug_Load.php';
include_once 'RO_RLD_Refrigerator.php';
include_once 'RO_RLD_Shower.php';
include_once 'RO_RLD_Window_System.php';
?>
