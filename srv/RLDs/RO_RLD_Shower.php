<?php
   include_once "RO_Device.php";

   class RO_RLD_Shower extends RO_Device
   {
      const name_table = "RLD_Showers";
      
      public $type_shower;
      public $length_shower_minutes;
      public $number_showers_week;
      public $type_bath;
      public $number_baths_week;
      
      public $action_free_reduce_time;
      public $action_free_worst_time;
      public $action_upgrade_low_flow;
      public $action_upgrade_standard;
      public $action_free_reduce_bath_fill;
      public $action_free_increase_bath_fill;
      public $action_free_bath_to_shower;
      public $action_free_reduce_shower_number;
      public $action_free_reduce_bath_number;

      static function create_default($id_room)
      {
         return RO_RLD_Shower::load(Helper_RTLDs::create_device_and_actions($id_room, "RLD_Showers", 26));
      }

      static function update(RO_RLD_Shower $device)
      {
         $mysqli = connecti();
         RO_Action::update_actions($device->ro_actions);
         $query = sprintf("UPDATE RLD_Showers
                           SET id_room_RLD_Showers='%d',
                               id_residence_RLD_Showers='%d',
                               is_info_entered='%d',
                               type_shower='%d',
                               length_shower_minutes='%d',
                               number_showers_week='%d',
                               type_bath='%d',
                               number_baths_week='%d'
                           WHERE id_device='%d'",
                           mysql_real_escape_string($device->id_room),
                           mysql_real_escape_string($device->id_residence),
                           mysql_real_escape_string($device->is_info_entered),
                           mysql_real_escape_string($device->type_shower),
                           mysql_real_escape_string($device->length_shower_minutes),
                           mysql_real_escape_string($device->number_showers_week),
                           mysql_real_escape_string($device->type_bath),
                           mysql_real_escape_string($device->number_baths_week),
                           mysql_real_escape_string($device->id_device));
         if (!($mysqli->query($query))) 
         { 
            error_log("Failed to update RO_RLD_Shower with id=".$device->id_device." .".$mysqli->error);
            return null;
         }
      }

      static function load_all_in_room($id_room)
      {
         $mysqli = connecti();
         $new_devices = array();
         $query = sprintf("SELECT * FROM RLD_Showers WHERE id_room_RLD_Showers='%d'",
                     mysql_real_escape_string($id_room));
         if(!($result = $mysqli->query($query)))
         { 
            error_log("Failed to select Showers. ".$mysqli->error);
            return null;
         }
         while($a_row = $result->fetch_assoc())
         {
            array_push($new_devices, RO_RLD_Shower::copy_to_device($a_row));
         }
         return $new_devices;
      }

      static function load($id_device)
      {
         $mysqli = connecti();
         $query = sprintf("SELECT * FROM RLD_Showers WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if(!($result = $mysqli->query($query)))
         { 
            error_log("Failed to select Shower with id_device ".$id_device." .".$mysqli->error);
            return null;
         }
         if ($a_row = $result->fetch_assoc())
         {
            return RO_RLD_Shower::copy_to_device($a_row);
         }
         else
         {
            error_log ("There is no Shower with ID ".$id_device);
            return null;
         }
      }

      static function copy_to_device($a_row)
      {
         $new_device = new RO_RLD_Shower();

         $new_device->id_device = (int)$a_row['id_device'];
         $new_device->id_room = (int)$a_row['id_room_RLD_Showers'];
         $new_device->id_residence = (int) $a_row['id_residence_RLD_Showers'];
         $new_device->is_info_entered = (int) $a_row['is_info_entered'];

         $new_device->type_shower = (int)$a_row['type_shower'];
         $new_device->length_shower_minutes = (int)$a_row['length_shower_minutes'];
         $new_device->number_showers_week = (int)$a_row['number_showers_week'];
         $new_device->type_bath = (int)$a_row['type_bath'];
         $new_device->number_baths_week = (int)$a_row['number_baths_week'];

         $new_device->load_actions($a_row);
         
         $new_device->action_free_reduce_time = $new_device->ro_actions[0];
         $new_device->action_free_worst_time = $new_device->ro_actions[1];
         $new_device->action_upgrade_low_flow = $new_device->ro_actions[2];
         $new_device->action_upgrade_standard = $new_device->ro_actions[3];
         $new_device->action_free_reduce_bath_fill = $new_device->ro_actions[4];
         $new_device->action_free_increase_bath_fill = $new_device->ro_actions[5];
         $new_device->action_free_bath_to_shower = $new_device->ro_actions[6];
         $new_device->action_free_reduce_shower_number = $new_device->ro_actions[7];
         $new_device->action_free_reduce_bath_number = $new_device->ro_actions[8];
               
         return $new_device;
      }
   }
?>
