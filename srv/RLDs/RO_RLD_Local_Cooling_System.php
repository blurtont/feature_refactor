<?php
   include_once "RO_Device.php";

   class RO_RLD_Local_Cooling_System extends RO_Device
   {
      const name_table = "RLD_Local_Cooling_Systems";
      
      public $efficiency;
      public $type_ac;
      public $is_willing_use_room_unit_exclusively;
      public $use_room_unit_exclusively;
      public $is_own_room_unit;

      public $action_free_quit_room_AC_use;
      public $action_free_quit_central_AC_use_room_units;
      public $action_upgrade_AC_room_high_efficiency;
      public $action_upgrade_AC_room_standard_efficiency;
      public $action_upgrade_AC_room_low_efficiency;

      static function create_default($id_room)
      {
         return RO_RLD_Local_Cooling_System::load(Helper_RTLDs::create_device_and_actions($id_room, "RLD_Local_Cooling_Systems", 12));
      }

      static function update(RO_RLD_Local_Cooling_System $device)
      {
         RO_Action::update_actions($device->ro_actions);

         $mysqli = connecti();
         $query = sprintf("UPDATE RLD_Local_Cooling_Systems
                           SET id_room_RLD_Local_Cooling_Systems='%d',
                               id_residence_RLD_Local_Cooling_Systems='%d',
                               is_info_entered='%d',
                               efficiency='%d',
                               type_ac='%d',
                               is_willing_use_room_unit_exclusively='%d',
                               use_room_unit_exclusively='%d',
                               is_own_room_unit='%d'
                           WHERE id_device='%d'",
                           mysql_real_escape_string($device->id_room),
                           mysql_real_escape_string($device->id_residence),
                           mysql_real_escape_string($device->is_info_entered),
                           mysql_real_escape_string($device->efficiency),
                           mysql_real_escape_string($device->type_ac),
                           mysql_real_escape_string($device->is_willing_use_room_unit_exclusively),
                           mysql_real_escape_string($device->use_room_unit_exclusively),
                           mysql_real_escape_string($device->is_own_room_unit),
                           mysql_real_escape_string($device->id_device));
         if (!($mysqli->query($query))) 
         { 
            error_log("Failed to update RO_RLD_Local_Cooling_System with id=".$device->id_device." .".$mysqli->error);
         }
      
      }

      static function load_all_in_room($id_room)
      {
         $mysqli = connecti();
         $new_local_cooling_systems = array();
         $query = sprintf("SELECT * FROM RLD_Local_Cooling_Systems WHERE id_room_RLD_Local_Cooling_Systems='%d'",
                     mysql_real_escape_string($id_room));
         if(!($result = $mysqli->query($query))) 
         {
            error_log("Failed to select local cooling systems. ".$mysqli->error);
            return null;
         }
         while($a_row = $result->fetch_assoc())
         {
            array_push($new_local_cooling_systems, RO_RLD_Local_Cooling_System::copy_to_device($a_row));
         }
         return $new_local_cooling_systems;
      }

      static function load($id_device)
      {
         $mysqli = connect();
         $query = sprintf("SELECT * FROM RLD_Local_Cooling_Systems WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if(!($result = $mysqli->query($query))) 
         { 
            error_log("Failed to select local cooling system with id_device ".$id_device." .".$mysqli->error);
            return null;
         }
         if ($a_row = $result->fetch_assoc())
         {
            return RO_RLD_Local_Cooling_System::copy_to_device($a_row);
         }
         else
         {
            error_log ("There is no local cooling system".$id_device);
         }
      }

      static function copy_to_device($a_row)
      {
         $new_local_cooling_system = new RO_RLD_Local_Cooling_System();
         
         $new_local_cooling_system->id_device = (int) $a_row['id_device'];
         $new_local_cooling_system->id_room = (int) $a_row['id_room_RLD_Local_Cooling_Systems'];
         $new_local_cooling_system->id_residence = (int) $a_row['id_residence_RLD_Local_Cooling_Systems'];
         $new_local_cooling_system->is_info_entered = (int) $a_row['is_info_entered'];
         
         $new_local_cooling_system->efficiency = (int) $a_row['efficiency'];
         $new_local_cooling_system->type_ac = (int) $a_row['type_ac'];
         $new_local_cooling_system->is_willing_use_room_unit_exclusively = (int) $a_row['is_willing_use_room_unit_exclusively'];
         $new_local_cooling_system->use_room_unit_exclusively = (int) $a_row['use_room_unit_exclusively'];
         $new_local_cooling_system->is_own_room_unit = (int) $a_row['is_own_room_unit'];
         
         $new_local_cooling_system->load_actions($a_row);
         return $new_local_cooling_system;
      }
   }
?>
