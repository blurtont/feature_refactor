<?php
   include_once "RO_Device.php";

   class RO_RLD_Cooktop extends RO_Device
   {
      const name_table = "RLD_Cooktops";
            
      public $type_cooktop;
      public $hours_per_week;
      public $uses_per_week;
      public $burners_per_use_average;
      public $is_own_pressure_cooker;
      public $is_own_insulated_pot;
      public $fraction_use_pan_lid;
      public $fraction_use_insulated_pot;
      public $fraction_use_pressure_cooker;
      public $fraction_willing_use_pressure_cooker;

      public $action_free_use_pan_lids;
      public $action_upgrade_insulated_pot;
      public $action_upgrade_pressure_cooker;
      public $action_upgrade_standard_pot;

      static function create_default($id_room)
      {
         return RO_RLD_Cooktop::load(Helper_RTLDs::create_device_and_actions($id_room, "RLD_Cooktops", 11));
      }

      /* save an existing device in the DB */
      static function update($device)
      {   
         RO_Action::update_actions($device->ro_actions);
         
         $mysqli = connecti();

         $query = sprintf("UPDATE RLD_Cooktops SET
                     id_room_RLD_Cooktops='%d',
                     id_residence_RLD_Cooktops='%d',
                     is_info_entered='%d',
                     type_cooktop='%d',
                     hours_per_week_10x='%d',
                     uses_per_week='%d',
                     burners_per_use_average_10x='%d',
                     is_own_pressure_cooker='%d',
                     is_own_insulated_pot='%d',
                     fraction_use_pan_lid_100x='%d',
                     fraction_use_insulated_pot_100x='%d',
                     fraction_use_pressure_cooker_100x='%d',
                     fraction_willing_use_pressure_cooker_100x='%d'
                     WHERE id_device='%d'",
                     mysql_real_escape_string($device->id_room),
                     mysql_real_escape_string($device->id_residence),
                     mysql_real_escape_string($device->is_info_entered),
                     mysql_real_escape_string($device->type_cooktop),
                     mysql_real_escape_string($device->hours_per_week*10),
                     mysql_real_escape_string($device->uses_per_week),
                     mysql_real_escape_string($device->burners_per_use_average*10),
                     mysql_real_escape_string($device->is_own_pressure_cooker),
                     mysql_real_escape_string($device->is_own_insulated_pot),
                     mysql_real_escape_string($device->fraction_use_pan_lid*100),
                     mysql_real_escape_string($device->fraction_use_insulated_pot*100),
                     mysql_real_escape_string($device->fraction_use_pressure_cooker*100),
                     mysql_real_escape_string($device->fraction_willing_use_pressure_cooker*100),
                     mysql_real_escape_string($device->id_device));
         if (!$mysqli->query($query))
         {
            error_log("ERROR: failed to update cooktop with id".$device->id_device." .".$mysqli->error);
         }
      }   


      static function load_all_in_room($id_room)
      {
         $mysqli = connecti();
         $new_cooktops = array();
         $query = sprintf("SELECT * FROM RLD_Cooktops WHERE id_room_RLD_Cooktops='%d'",
                     mysql_real_escape_string($id_room));
         if (!($result = $mysqli->query($query)))
         {
            error_log("ERROR: Failed to select cooktops. ".$mysqli->error());
         }
         while($a_row = $result->fetch_assoc())
         {
            array_push($new_cooktops, RO_RLD_Cooktop::copy_to_device($a_row));
         }
         return $new_cooktops;
      }

      static function load($id_device)
      {
         $mysqli = connecti();
         $query = sprintf("SELECT * FROM RLD_Cooktops WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if (!($result = $mysqli->query($query)))
         {
            error_log("ERROR: Failed to select cooktop with id_device ".$id_device." .".$mysqli->error);
            return null;
         }
         if ($a_row = $result->fetch_assoc())
         {
            return RO_RLD_Cooktop::copy_to_device($a_row);
         }
         else
         {
            error_log("ERROR: There is no cooktop ".$id_device);
            return null;
         }
      }

      static function copy_to_device($a_row)
      {
         $new_cooktop = new RO_RLD_Cooktop();
         
         $new_cooktop->id_device = (int) $a_row['id_device'];
         $new_cooktop->id_room = (int) $a_row['id_room_RLD_Cooktops'];
         $new_cooktop->id_residence = (int) $a_row['id_residence_RLD_Cooktops'];
         $new_cooktop->is_info_entered = (int) $a_row['is_info_entered'];
         
         $new_cooktop->type_cooktop = (int) $a_row['type_cooktop'];
         $new_cooktop->hours_per_week = $a_row['hours_per_week_10x']/10.0;
         $new_cooktop->uses_per_week = (int) $a_row['uses_per_week'];
         $new_cooktop->burners_per_use_average = $a_row['burners_per_use_average_10x']/10.0;
         $new_cooktop->is_own_pressure_cooker = (int) $a_row['is_own_pressure_cooker'];
         $new_cooktop->is_own_insulated_pot = (int) $a_row['is_own_insulated_pot'];
         $new_cooktop->fraction_use_pan_lid = $a_row['fraction_use_pan_lid_100x']/100.0;
         $new_cooktop->fraction_use_insulated_pot = $a_row['fraction_use_insulated_pot_100x']/100.0;
         $new_cooktop->fraction_use_pressure_cooker = $a_row['fraction_use_pressure_cooker_100x']/100.0;
         $new_cooktop->fraction_willing_use_pressure_cooker = $a_row['fraction_willing_use_pressure_cooker_100x']/100.0;

         $new_cooktop->load_actions($a_row);

         $new_cooktop->action_free_use_pan_lids = $new_cooktop->ro_actions[0];
         $new_cooktop->action_upgrade_insulated_pot = $new_cooktop->ro_actions[1]; 
         $new_cooktop->action_upgrade_pressure_cooker = $new_cooktop->ro_actions[2]; 
         $new_cooktop->action_upgrade_standard_pot = $new_cooktop->ro_actions[3]; 
         return $new_cooktop;
      }

   }
?>
