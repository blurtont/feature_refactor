<?php
   include_once "globals.php";
   include_once "RO_HVAC_Baseline.php";

   class Service_HVAC_Baseline
   {
      public function insert($ro_HVAC_baseline, $id_residence, $id_user)
      {
         RO_HVAC_Baseline::insert($ro_HVAC_baseline, $id_residence, $id_user);
      }

      public function get_latest($id_residence)
      {
         return RO_HVAC_Baseline::get_latest($id_residence);
      }

      public function get_first($id_residence)
      {
         return RO_HVAC_Baseline::get_first($id_residence);
      }

   }

