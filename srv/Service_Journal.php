<?php
   include_once "globals.php";
   include_once "Type_Interaction.php";
   include_once "Type_Page_Journal.php";

   class Service_Journal
   {
      static function view_journal_page($id_user, $id_residence, $type_page_journal)
      {
         $interaction = new Interaction();
         $interaction->id_user = $id_user;
         $interaction->id_type_interaction = Type_Interaction::JOURNAL_PAGE_VIEW;
         $interaction->id_type_page_journal = $type_page_journal;
         $interaction->insert();
      }
   }
?>
