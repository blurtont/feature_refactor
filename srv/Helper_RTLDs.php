<?php
   include_once "globals.php";
   include_once "RO_Action.php";
  
   class Helper_RTLDs
   {
      static function create_device_and_actions($id_room, $name_device_table, $id_type_device)
      {
         /* connect to DB */
         $mysqli = connecti();

         /* create a default device */
         $query = sprintf("INSERT INTO %s (id_room_%s) VALUES ('%d')",
                     mysql_real_escape_string($name_device_table),
                     mysql_real_escape_string($name_device_table),  
                     mysql_real_escape_string($id_room));
         if (!($result = $mysqli->query($query)))
         {
            error_log("Failed to create device in ".$name_device_table.". ".$mysqli->error);
            return null;
         }
         $id_device = $mysqli->insert_id;

         /* get non-deprecated actions for device */
         $query = sprintf("SELECT id_type_action FROM Type_Actions WHERE id_type_device_Type_Actions='%d' AND is_deprecated='0'",
                     mysql_real_escape_string($id_type_device));
         if (!($result = $mysqli->query($query)))
         {
            error_log("Failed to get non-deprecated actions for id_type_device:".$id_type_device.". ".$mysqli->error);
            return null;
         }

         /* for each id_type_action, create a new Action. store the mysql_insert_ids */
         $index_action = 0;
         while ($a_row = $result->fetch_array(MYSQLI_NUM))
         {
            $query = sprintf("INSERT INTO Actions (id_type_action_Actions, id_type_device_Actions) VALUES ('%d', '%d')",
                        mysql_real_escape_string($a_row[0]),
                        mysql_real_escape_string($id_type_device));
            if (!($mysqli->query($query)))
            {
               error_log("ERROR: Failed to insert action for ".$name_device_table." id_type_action:".$a_row[0]." .".$mysqlI->error());
            }
            $id_actions[$index_action] = $mysqli->insert_id;
            $id_type_actions[$index_action++] = $a_row[0];
         }

         /* for each $id_action[] update the id_action_#  in $name_device_table with $id_device */
         $index_action = 0;
         foreach ($id_actions as $id_action)
         {
            $query = sprintf("UPDATE %s SET id_action_%d='%d' WHERE id_device='%d'",
                        mysql_real_escape_string($name_device_table),
                        mysql_real_escape_string($id_type_actions[$index_action++]),
                        mysql_real_escape_string($id_action),
                        mysql_real_escape_string($id_device));
            if (!($mysqli->query($query)))
            {
               error_log("Failed to update id_action_".$id_type_actions[$index_action-1]." on ".$name_device_table.". ".$mysqli->error);
            }
         }
         return $id_device;
      }

      //TODO: remove this funtion, call RO_Action::load directly from all RTLDs load
      static function load_action($id_action)
      {
         return RO_Action::load($id_action);
      }

   }

?>
