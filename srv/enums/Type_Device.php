<?php
class Type_Device
{
   const BASEMENT_CRAWLSPACE_FLOOR = 2;
   const BASEMENT_FLOOR = 3;
   const BASEMENT_WALL = 4;
   const BIOMASS = 5; /* TODO: does this exist? */
   const CEILING = 7;
   const CLOTHES_DRYER = 8;
   const CLOTHES_WASHER = 9;
   const COFFEE_MAKER = 10;
   const COOKTOP = 11;
   const COOLING_SYSTEM = 1;
   const DISH_WASHER = 13;
   const DOOR = 14;
   const DUCTWORK = 15;
   const ELECTRIC_HEATING = 19; /* TODO: does this exist? just heating_system, no? */
   const FAUCET = 16;
   const HEATING_SYSTEM = 18;
   const HOUSE_SEAL = 21;
   const LIGHT = 22;
   const LOCAL_COOLING = 12;
   const LOCAL_HEATING = 20;
   const OVEN =23;
   const PLUG_LOAD = 24;
   const REFRIGERATOR = 25;
   const SHOWER = 26;
   const SLAB_FLOOR = 17;
   const THERMOSTAT = 27;
   const WALL = 29;
   const WALL_SHEET_INSULATION = 28;
   const WATER_HEATER = 30;
   const WINDOW_SYSTEM = 31;

   static function get_type(RO_Device $ro_device)
   {
      switch($ro_device::name_table)
      {
         case 'RLD_Clothes_Dryers':
            return Type_Device::CLOTHES_DRYER;  
         case 'RLD_Coffee_Makers':
            return Type_Device::COFFEE_MAKER;
         case 'RLD_Cooktops':
            return Type_Device::COOKTOP; 
         case 'RLD_Dish_Washers':
            return Type_Device::DISH_WASHER;
         case 'RLD_Doors':
            return Type_Device::DOOR;
         case 'RLD_Faucets':
            return Type_Device::FAUCET;
         case 'RLD_Lights':
            return Type_Device::LIGHT;
         case 'RLD_Local_Cooling_Systems':
            return Type_Device::LOCAL_COOLING;
         case 'RLD_Local_Heating_Systems':
            return Type_Device::LOCAL_HEATING;
         case 'RLD_Ovens':
            return Type_Device::OVEN;
         case 'RLD_Plug_Loads':
            return Type_Device::PLUG_LOAD;
         case 'RLD_Refrigerators':
            return Type_Device::REFRIGERATOR; 
         case 'RLD_Showers':
            return Type_Device::SHOWER;
         case 'RLD_Window_Systems':
            return Type_Device::WINDOW_SYSTEM;
         case 'TLD_Basement_Crawlspace_Floors':
            return Type_Device::BASEMENT_CRAWLSPACE_FLOOR;
         case 'TLD_Basement_Walls':           
            return Type_Device::BASEMENT_WALL;
         case 'TLD_Ceilings':
            return Type_Device::CEILING;
         case 'TLD_Cooling_Systems':
            return Type_Device::COOLING_SYSTEM;
         case 'TLD_Ductworks':
            return Type_Device::DUCTWORK;
         case 'TLD_Heating_Systems':
            return Type_Device::HEATING_SYSTEM;
         case 'TLD_House_Seals':
            return Type_Device::HOUSE_SEAL;
         case 'TLD_Slabs':
            return Type_Device::SLAB_FLOOR;
         case 'TLD_Thermostats':
            return Type_Device::THERMOSTAT;
         case 'TLD_Walls':
            return Type_Device::WALL;
         case 'TLD_Water_Heaters':
            return Type_Device::WATER_HEATER;
         }
   }
}
