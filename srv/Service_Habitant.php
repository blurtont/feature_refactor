<?php
   include_once "globals.php";
   include_once "RO_Residence.php";
   include_once "RO_User.php";

   class Service_Habitant
   {
      public function is_questionnaire_residence_complete($id_user, $id_residence)      
      {
         $mysqli = connecti();

         $query = sprintf("SELECT is_questionnaire_residence_complete FROM Habitants
                           WHERE id_user_Habitants='%d' && id_residence_Habitants='%d'",
                          mysql_real_escape_string($id_user),
                          mysql_real_escape_string($id_residence));
         if (!($result = $mysqli->query($query)))
         {
            error_log ("failed to get habitant with id_user=".$id_user." and id_residence=".$id_residence." ".$mysqli->error);
            return null;
         }

         if($a_row = $result->fetch_assoc())
         {      
            return (int) $a_row['is_questionnaire_residence_complete'];
         }
         else
         {
            error_log("id_user/id_residence combination ".$id_user."/".$id_residence." not found.");
            return null;
         }
      }

      public function is_questionnaire_user_complete($id_user, $id_residence)
      {
         $mysqli = connecti();

         $query = sprintf("SELECT is_questionnaire_user_complete FROM Habitants
                           WHERE id_user_Habitants='%d' && id_residence_Habitants='%d'",
                          mysql_real_escape_string($id_user),
                          mysql_real_escape_string($id_residence));
         if (!($result = $mysqli->query($query)))
         {
            error_log ("failed to get habitant with id_user=".$id_user." and id_residence=".$id_residence." ".$mysqli->error);
            return null;
         }
      
         if($a_row = $result->fetch_assoc())
         {
            return (int) $a_row['is_questionnaire_user_complete'];
         }
         else
         {
            error_log("id_user/id_residence combination ".$id_user."/".$id_residence." not found.");
            return null;
         }
      }

      public function set_questionnaire_residence_complete($value, $id_user, $id_residence)
      {
         $mysqli = connecti();

         $query = sprintf("UPDATE Habitants SET is_questionnaire_residence_complete='%d'
                           WHERE id_user_Habitants='%d' && id_residence_Habitants='%d'",
                           mysql_real_escape_string($value),
                           mysql_real_escape_string($id_user),
                           mysql_real_escape_string($id_residence));
         
         if (!$mysqli->query($query))
         {
            error_log ("unable to update is_questionnaire_residence_complete in Habitants 
                     where id_user_Habitants ==".$id_user.
                     " && id_residence_Habitants == ".$id_residence_Habitants." ".$mysqli->error);
            return null;
         }
      }

      public function set_questionnaire_user_complete($value, $id_user, $id_residence)
      {
         $mysqli = connecti();

         $query = sprintf("UPDATE Habitants SET is_questionnaire_user_complete='%d'
                           WHERE id_user_Habitants='%d' && id_residence_Habitants='%d'",
                           mysql_real_escape_string($value),
                           mysql_real_escape_string($id_user),
                           mysql_real_escape_string($id_residence));

         if (!$mysqli->query($query))
         {
            error_log ("unable to update is_questionnaire_user_complete in Habitants 
                     where id_user_Habitants ==".$id_user.
                     " && id_residence_Habitants == ".$id_residence_Habitants);
         }
      }
   }
?>
