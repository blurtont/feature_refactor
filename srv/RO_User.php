<?php
class RO_User
{
	public $userID;
   public $id_facebook;
	public $nameFirst;
	public $nameLast;
   public $time_create_account;
   public $unix_time_create_account;
	public $email;
	public $password;
   public $phone_number;
   public $address_street;
   public $motivation_fraction_money;
   public $type_usage_self_perception;

   public $is_do_it_yourself;

   public $is_tutorial_homepage_complete;
   public $is_tutorial_page_community_complete;
   public $is_tutorial_page_results_complete;
   public $is_tutorial_page_todo_complete;
   public $is_tutorial_page_utility_complete;

   public $notification_email_updates;
   public $notification_text_updates;
   public $notification_email_challenges;
   public $notification_text_challenges;
   public $notification_email_promotions;
   public $notification_text_promotions;

   public $hear_about;
   public $interest_partnering;

   public $zip;
	public $is_active;

   public $errorType;
	public $errorMessage;

   public function remove_deprecated($email)
   {
      $mysqli = connecti();
      $query = sprintf("DELETE FROM Users WHERE email='%s'",
                        mysql_real_escape_string($email));
      if (!($mysqli->query($query))) 
      { 
         error_log ("delete user failed");
         return null;
      }

      $query = sprintf("DELETE FROM Habitants WHERE id_user_Habitants='%d'",
                        mysql_real_escape_string($this->userID));
      if (!($mysqli->query($query)))
      {
         error_log ("delete habitant failed");
         return null;
      }
   }
  
   public function duplicate($new_email, $new_pass)
   {
      $mysqli = connecti( );
      $query = sprintf( "INSERT INTO Users (email, 
                                            pass, 
                                            firstName, 
                                            lastName, 
                                            unix_time_create_account, 
                                            zip, 
                                            address_street, 
                                            motivation_fraction_money, 
                                            is_do_it_yourself, 
                                            type_usage_self_perception, 
                                            is_tutorial_homepage_complete,
                                            is_tutorial_page_community_complete,
                                            is_tutorial_page_results_complete,
                                            is_tutorial_page_todo_complete,
                                            is_tutorial_page_utility_complete)
                         VALUES ('%s', '%s', '%s', '%s', '%s', '%d', '%d', '%d', '%d', '%d')",
   				         mysql_real_escape_string( $new_email ),
				            hash( "sha512", mysql_real_escape_string( $new_pass ) ),
				            mysql_real_escape_string(ucfirst(strtolower( $this->nameFirst ) ) . "_COPY" ),
				            mysql_real_escape_string(ucfirst(strtolower( $this->nameLast ) ) . "_COPY" ),
                        mysql_real_escape_string($this->unix_time_create_account),
                        mysql_real_escape_string($this->zip),
                        mysql_real_escape_string($this->address_street),
                        mysql_real_escape_string($this->motivation_fraction_money),
                        mysql_real_escape_string($this->is_do_it_yourself),
                        mysql_real_escape_string($this->type_usage_self_perception),
                        mysql_real_escape_string($this->is_tutorial_homepage_complete),
                        mysql_real_escape_string($this->is_tutorial_page_community_complete),
                        mysql_real_escape_string($this->is_tutorial_page_results_complete),
                        mysql_real_escape_string($this->is_tutorial_page_todo_complete),
                        mysql_real_escape_string($this->is_tutorial_page_utility_complete));
		if ( !( $result = $mysqli->query( $query ) ) )
      {
			error_log( "unable to compslete query. " . $mysqli->error );
         return null;
      }

      return $mysqli->insert_id;
   }

   static function update_common(RO_User $user)
   {
      $mysqli = connecti();
      $query = sprintf("UPDATE Users SET firstName='%s', 
                                         lastName='%s', 
                                         phone_number='%d',
                                         motivation_fraction_money='%d', 
                                         is_do_it_yourself='%d', 
                                         type_usage_self_perception='%d',
                                         is_tutorial_homepage_complete='%d',
                                         is_tutorial_page_community_complete='%d',
                                         is_tutorial_page_results_complete='%d',
                                         is_tutorial_page_todo_complete='%d',
                                         is_tutorial_page_utility_complete='%d',
                                         zip='%d',
                                         address_street='%s',
                                         hear_about='%s',
                                         interest_partnering='%d',
                                         notification_email_updates='%d',
                                         notification_text_updates='%d',
                                         notification_email_challenges='%d',
                                         notification_text_challenges='%d',
                                         notification_email_promotions='%d',
                                         notification_text_promotions='%d'
                       WHERE userid='%d'", 
                       mysql_real_escape_string($user->nameFirst),
                       mysql_real_escape_string($user->nameLast),
                       mysql_real_escape_string($user->phone_number),
                       mysql_real_escape_string($user->motivation_fraction_money),
                       mysql_real_escape_string($user->is_do_it_yourself),
                       mysql_real_escape_string($user->type_usage_self_perception),
                       mysql_real_escape_string($user->is_tutorial_homepage_complete),
                       mysql_real_escape_string($user->is_tutorial_page_community_complete),
                       mysql_real_escape_string($user->is_tutorial_page_results_complete),
                       mysql_real_escape_string($user->is_tutorial_page_todo_complete),
                       mysql_real_escape_string($user->is_tutorial_page_utility_complete),
                       mysql_real_escape_string($user->zip),
                       mysql_real_escape_string($user->address_street),
                       mysql_real_escape_string($user->hear_about),
                       mysql_real_escape_string($user->interest_partnering),
                       mysql_real_escape_string($user->notification_email_updates),
                       mysql_real_escape_string($user->notification_text_updates),
                       mysql_real_escape_string($user->notification_email_challenges),
                       mysql_real_escape_string($user->notification_text_challenges),
                       mysql_real_escape_string($user->notification_email_promotions),
                       mysql_real_escape_string($user->notification_text_promotions),
                       mysql_real_escape_string($user->userID));
       if (!($mysqli->query($query))) 
       {
          error_log ("user common update failed. | ".$query." | ".$mysqli->error);
          return null;
       }
   }

   static function update_new_email(RO_User $ro_user, $email_address_new)
   {
      $mysqli = connecti();
   
      $key_email_reset = randString(50);

      /* update user */
      $query = sprintf("UPDATE Users SET key_email_reset='%s' WHERE userid='%d'",
                       mysql_real_escape_string($key_email_reset),
                       mysql_real_escape_string($ro_user->userID));
      if (!($mysqli->query($query))) 
      {
         error_log("RO_User::update_new_email, Failed to update key_email_reset. ".$mysqli->error);
         return null;
      }

      /* send verification/notification emails */
      /* fork
         parent: returns.
         child: send emails
      */
      $pid = pcntl_fork();
      if ($pid == -1) 
      {   
         error_log("RO_User::update_new_email, fork failed. No emails sent.");
         $user->password = ""; 
         return false;
      }   
      else if ($pid) /* the parent */
      {   
         /* and return the user object */
         $user->password = ""; 
         return true;
      }   
      else /* the child */
      {  
         $url_verify = 'http://'.get_server_address().'/verify_change_email.php?email='.$ro_user->email.'&email_new='.$email_address_new.'&key='.$key_email_reset;
         $body = get_email_verify_new_email($ro_user->nameFirst, $url_verify);
         $subject = "Dropoly - Please verify email address change";
         sendDropolyEmail($email_address_new, $ro_user->nameFirst, $body, $subject);
      }
   }
   
   static function remove_account(RO_User $ro_user)
   {
      $mysqli = connecti();
   
      $key_account_delete = randString(50);

      /* update key_account_delete */
      $query = sprintf("UPDATE Users SET key_account_delete='%s' WHERE userid='%d'",
                       mysql_real_escape_string($key_account_delete),
                       mysql_real_escape_string($ro_user->userID));
      if (!($mysqli->query($query))) 
      {
         error_log("RO_User::remove_account, Failed to update key_account_delete.".$mysqli->error);
         return null;
      }

      /* send verification/notification emails */
      /* fork
         parent: returns.
         child: send emails
      */
      $pid = pcntl_fork();
      if ($pid == -1) 
      {   
         error_log("RO_User::remove_account, fork failed. No emails sent.");
         $user->password = ""; 
         return false;
      }   
      else if ($pid) /* the parent */
      {   
         /* and return the user object */
         $user->password = ""; 
         return true;
      }   
      else /* the child */
      {
         $url_verify = 'http://'.get_server_address().'/verify_delete_account.php?email='.$ro_user->email.'&key='.$key_account_delete;
         $body = get_email_verify_delete_account($ro_user->nameFirst, $url_verify);
         $subject = "Dropoly - Please verify your account deletion";
         sendDropolyEmail($ro_user->email, $ro_user->nameFirst, $body, $subject);
      }
   }
}
?>
