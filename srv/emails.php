<?php
include_once 'globals.php';

function getEmailConfirmEmail($firstName, $keyURL)
{

   return '
<html>
<head>
<title>Welcome to Dropoly!</title>
</head>

<body>
<table width="600" height="400">
<tr>
<td background="http://dropoly.com/emailassets/emailTemplate3.png" background-repeat: no-repeat;  text="#663300" vlink="#CC0033" alink="#CC0033" leftmargin="50px" topmargin="50px">
<p style="font-family: Arial, Helvetica, sans-serif; font-size:14px; line-height:22px; color:#630; margin:90px 0 0 0; padding:0 0 0 80px;" >
<b>Hello again!</b><br />
We need you to verify your email address to complete your <br />
move in to the Dropoly community. To do that, just click the link <br />
below and you\'ll be ready to start playing for real change!<br /><br />
<a href="'.$keyURL.'"><b>Click here to verify!</b></a> 
  <br/>
  
  <br />
  Game On! <br />
  <b>Your friends at Dropoly</b>
</p></td>
</table>
</body>
</html>';
}

function getEmailReactivation($firstName, $keyURL)
{

   return '
<html>
<head>
<title>Welcome to Dropoly!</title>
</head>

<body>
<table width="600" height="400">
<tr>
<td background="http://dropoly.com/emailassets/emailTemplate3.png" background-repeat: no-repeat;  text="#663300" vlink="#CC0033" alink="#CC0033" leftmargin="50px" topmargin="50px">
<p style="font-family: Arial, Helvetica, sans-serif; font-size:14px; line-height:22px; color:#630; margin:90px 0 0 0; padding:0 0 0 80px;" >
<b>Welcome back!</b><br />
We need you to verify your email address to reactivate your <br />
Dropoly account. To do that, just click the link <br />
below and you\'ll be ready to start playing for real change again!<br /><br />
<a href="'.$keyURL.'"><b>Click here to verify!</b></a> 
  <br/>
  
  <br />
  Game On! <br />
  <b>Your friends at Dropoly</b>
</p></td>
</table>
</body>
</html>';
}

function get_email_verify_new_email($firstName, $url_verify)
{
   return '
<html>
<head>
<title>Please verify your new email</title>
</head>

<body>
<table width="600" height="400">
<tr>
<td background="http://dropoly.com/emailassets/emailTemplate3.png" background-repeat: no-repeat;  text="#663300" vlink="#CC0033" alink="#CC0033" leftmargin="50px" topmargin="50px">
<p style="font-family: Arial, Helvetica, sans-serif; font-size:14px; line-height:22px; color:#630; margin:90px 0 0 0; padding:0 0 0 80px;" >
<b>Hey '.$firstName.'!</b><br />
We need to you to verify your email address to complete your <br />
change email request. To do that, just click the link <br />
below and you\'re account will be updated. <br />
Email us at support@dropoly.com<br />
if you have any problems.<br /><br />
<a href="'.$url_verify.'"><b>Click here to verify!</b></a> 
  <br/>
  
  <br />
  Game On! <br />
  <b>Your friends at Dropoly</b>
</p></td>
</table>
</body>
</html>';
}

function get_email_notify_new_email($firstName, $new_email)
{
   return '
<html>
<head>
<title>Notification - dropoly email change request</title>
</head>

<body>
<table width="600" height="400">
<tr>
<td background="http://dropoly.com/emailassets/emailTemplate3.png" background-repeat: no-repeat;  text="#663300" vlink="#CC0033" alink="#CC0033" leftmargin="50px" topmargin="50px">
<p style="font-family: Arial, Helvetica, sans-serif; font-size:14px; line-height:22px; color:#630; margin:90px 0 0 0; padding:0 0 0 80px;" >
<b>Hey '.$firstName.'!</b><br />
This is a notification that your dropoly.com email address <br />
has been requested to change to '.$new_email.'. <br />
If this isn\'t correct please email us at support@dropoly.com .<br /><br />
  <br/>
  
  <br />
  Game On! <br />
  <b>Your friends at Dropoly</b>
</p></td>
</table>
</body>
</html>';
}

function get_email_success_new_email($firstName, $new_email)
{
   return '
<html>
<head>
<title>Notification - dropoly email change request</title>
</head>

<body>
<table width="600" height="400">
<tr>
<td background="http://dropoly.com/emailassets/emailTemplate3.png" background-repeat: no-repeat;  text="#663300" vlink="#CC0033" alink="#CC0033" leftmargin="50px" topmargin="50px">
<p style="font-family: Arial, Helvetica, sans-serif; font-size:14px; line-height:22px; color:#630; margin:90px 0 0 0; padding:0 0 0 80px;" >
<b>Hey '.$firstName.'!</b><br />
This is a notification that your dropoly.com email address <br />
has been successfully changed to '.$new_email.'. <br />
If this isn\'t correct please email us at support@dropoly.com .<br /><br />
  <br/>
  
  <br />
  Game On! <br />
  <b>Your friends at Dropoly</b>
</p></td>
</table>
</body>
</html>';
}

function getEmailActivateAccount($fullName, $emailAddress, $keyURL, $hear_about)
{

   return '
<html>
<head>
<title>'.$fullName.' has joined Dropoly.com</title>
</head>

<body>
<table width="600" height="400">
<tr>
<td background="http://dropoly.com/emailassets/emailTemplate3.png" background-repeat: no-repeat;  text="#663300" vlink="#CC0033" alink="#CC0033" leftmargin="50px" topmargin="50px">
<p style="font-family: Arial, Helvetica, sans-serif; font-size:14px; line-height:22px; color:#630; margin:90px 0 0 0; padding:0 0 0 80px;" >
<b>Dropoly Administrator,</b><br/><br/>
'.$fullName.' has joined '.get_server_address().' with the email address <br />
'.$emailAddress.'.<br />How '.$fullName.' heard about us:<br /><i>'.$hear_about.'</i><br />
<a href="'.$keyURL.'"><b>click here to activate '.$fullName.'\'s account</b></a> 
  <br/>
  
  <br />
  Best regards,<br />
  <b>Dropoly System Admin</b>
</p></td>
</table>
</body>
</html>';
}

function getEmailWelcome($firstname)
{

   return '
<html>
<head>
<title>Welcome to Dropoly!</title>
</head>

<body style="height: 100%">
<table width="600" height="400">
<tr>
<td background="http://dropoly.com/emailassets/emailTemplate.png" background-repeat: no-repeat;  text="#663300" vlink="#CC0033" alink="#CC0033" leftmargin="50px" topmargin="50px">
<p style="font-family: Arial, Helvetica, sans-serif; font-size:14px; line-height:24px; color:#630; margin:80px 0 0 0; padding:0 0 0 80px;" >
<b>Hello there!</b><br />
  Your account has been verified! We will give you time to settle in and <br/>
  set up house,  but then, it\'s time to start playing against your friends<br/>
  and neighbors for real green! A few of your neighbors got a head start, </br>
  but we\'re confident you will catch up quickly. </br>
  <br />
  See you soon!
  <br />
  Your friends at Dropoly
  </p>
</td>
</tr>
</table>
</body>
</html>';
}

function getEmailAccountActive($firstname)
{
   return '
<html style="height: 100%">
<head>
<title>Welcome to Dropoly!</title>
</head>

<body style="height: 100%">
<table width="600" height="400">
<tr>
<td background="http://dropoly.com/emailassets/emailTemplate2.png" background-repeat: no-repeat;  text="#663300" vlink="#CC0033" alink="#CC0033" leftmargin="50px" topmargin="50px">
<p style="font-family: Arial, Helvetica, sans-serif; font-size:14px; line-height:24px; color:#630; margin:80px 0 0 0; padding:0 0 0 80px;" >
<b>Hey '.$firstname.'!</b><br />
   Welcome to Dropoly! Your Dropoly account is now active! Log on <br/> 
   now at '.get_server_address().' and open the door to your personal Welcome Party!<br/>  
   We love hearing from our friends. If you have any comments or <br />
    suggestions, drop us a line at info@dropoly.com. <br/>

  <br />
  Game On! <br />
  <b>Your friends at Dropoly</b>
  </p>
</td>
</tr>
</table>
</body>
</html>';
}

function getEmailResetPasswordCode($firstname, $resetcode)
{

return '
<html style="height: 100%">
<head>
<title>Welcome to Dropoly!</title>
</head>

<body style="height: 100%">
<table width="600" height="400">
<tr>
<td background="http://dropoly.com/emailassets/emailTemplate.png" background-repeat: no-repeat;  text="#663300" vlink="#CC0033" alink="#CC0033" leftmargin="50px" topmargin="50px">
<p style="font-family: Arial, Helvetica, sans-serif; font-size:14px; line-height:24px; color:#630; margin:80px 0 0 0; padding:0 0 0 80px;" >
<b>Hey '.$firstname.',</b><br />
  Your password reset code is: <br/>
  <b>'.$resetcode.'</b><br/>
  <br /><br />
  See you soon! <br />
  <b>Your friends at Dropoly</b>
  </p>
</td>
</tr>
</table>
</body>
</html>';
}

function get_email_verify_delete_account($firstName, $url_verify)
{
   return '
<html>
<head>
<title>Please verify you would like to delete your account at Dropoly</title>
</head>

<body>
<table width="600" height="400">
<tr>
<td background="http://dropoly.com/emailassets/emailTemplate3.png" background-repeat: no-repeat;  text="#663300" vlink="#CC0033" alink="#CC0033" leftmargin="50px" topmargin="50px">
<p style="font-family: Arial, Helvetica, sans-serif; font-size:14px; line-height:22px; color:#630; margin:90px 0 0 0; padding:0 0 0 80px;" >
<b>Hey '.$firstName.'!</b><br />
To complete your account deletion request, just click the link <br />
below and you\'re account will be removed. <br />
Email us at support@dropoly.com<br />
if you have any problems.<br /><br />
<a href="'.$url_verify.'"><b>Click here to verify!</b></a> 
  <br/>
  
  <br />
  Game On! <br />
  <b>Your friends at Dropoly</b>
</p></td>
</table>
</body>
</html>';
}
