<?php

class RO_Location
{
   public $address;
   public $zip_code;
   public $city;
   public $state;

   public $latitude;
   public $longitude;
}
