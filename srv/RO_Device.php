<?php
   include_once 'globals.php';
   include_once 'Helper_RTLDs.php';
   include_once 'RO_Action.php';
   include_once 'Type_Device.php';

   class RO_Device
   {
      public $id_device;
      public $id_residence;
      public $id_room;
      public $is_info_entered;

      public $is_owner_only = false;

      public $ro_actions;
      public $invalidated;

      public function remove()
      {
         $mysqli = connecti();
         foreach($this->ro_actions as $ro_action)
         {   
            RO_Action::remove($ro_action); //TODO: make this not static
         }

         $query = sprintf("DELETE FROM %s WHERE id_device='%d'",
                           mysql_real_escape_string($this::name_table),
                           mysql_real_escape_string($this->id_device));
         if (!$mysqli->query($query))
         {
            error_log("failed to remove device with id_device='%d' from '%s'".$this->id_device.$this::name_table);
            return null;
         }
      }

      public function load_actions($a_row)
      {
         $mysqli = connecti();
         $this->ro_actions = array();
         $index = 0;
         while(array_key_exists('id_action_'.$index, $a_row))
         {
            if($a_row['id_action_'.$index] == 0)
            {
               array_push($this->ro_actions, RO_Action::create(Type_Device::get_type($this), $index));
               $query = sprintf("UPDATE %s SET id_action_%d='%d'
                                 WHERE id_device='%d'",
                                 mysql_real_escape_string($this::name_table),
                                 mysql_real_escape_string($index),
                                 mysql_real_escape_string($this->ro_actions[$index]->id_action),
                                 mysql_real_escape_string($this->id_device));
               if (!$mysqli->query($query))
               {
                  error_log($mysqli->error);
               }
            }
            else
            {
               array_push($this->ro_actions, RO_Action::load($a_row['id_action_'.$index]));
            }
            $index++;
         }
      }

      public function duplicate($id_room_dup)
      {
         $mysqli = connecti();
         /* create new device of type */
         $dup_device = clone $this;
         $dup_device->id_room = $id_room_dup;
         /* create new instance in db */
         $query = sprintf("INSERT INTO %s (id_room_%s) VALUES ('%d')", 
                           mysql_real_escape_string($dup_device::name_table),
                           mysql_real_escape_string($dup_device::name_table),
                           mysql_real_escape_string($id_room_dup));
         if (!$mysqli->query($query))
         {
            error_log("failed to insert dup device into ".$dup_device::name_table.$mysqli->error);
         }
         $dup_device->id_device = $mysqli->insert_id;

         $dup_device->ro_actions = array();
         $dup_action_ids = array();
         foreach($this->ro_actions as $ro_action)
         {
            array_push($dup_action_ids, RO_Action::dup($ro_action));
         }

         $query = sprintf("UPDATE %s SET ", $dup_device::name_table);

         $index = 0;
         foreach ($dup_action_ids as $dup_action_id)
         {
            $query = $query.sprintf("id_action_%d='%d', ", $index, $dup_action_id);
            ++$index;
         }

         $query = substr($query, 0, strlen($query)-2);
         $query = $query.sprintf(" WHERE id_device='%d'", $dup_device->id_device);
         if (!$mysqli->query($query))
         {
            error_log ("failed to update dup device of type ".$dup_device::name_table.$mysqli->error);
            return null;
         }

         /* TODO: change all device update function to member function (i.e. not static) */
         $dup_device::update($dup_device);
      }
   }
?>
