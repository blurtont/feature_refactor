<?php
   include_once 'globals.php';
   include_once 'leaderboard/leaderboard.php';

   class RO_Leaderboard
   {
      public $leaders_savings_monthly_zip;
      public $leaders_actions_zip;
      public $leaders_interactions_zip;

      public $leaders_savings_monthly_local;
      public $leaders_actions_local;
      public $leaders_interactions_local;

      public $leaders_savings_monthly_global;
      public $leaders_actions_global;
      public $leaders_interactions_global;

      static function load_zip($zip)
      {
         $result = new RO_Leaderboard();

         $result->leaders_savings_monthly_zip = get_top_savings_monthly($zip, -1, -1);
         $result->leaders_actions_zip = get_top_completed_from_todo($zip, -1, -1);
         $result->leaders_interactions_zip = get_top_interactions($zip, -1, -1);
         
         $mysqli = connecti();
         $query = sprintf("SELECT id_group_zip_code_Group_Members_Zip_Codes
                           FROM Group_Members_Zip_Codes
                           WHERE zip_code_Group_Members_Zip_Codes='%d'",
                           mysql_real_escape_string($zip));
         if (!($resulti=$mysqli->query($query)))
         {
            error_log("ro_leaderboard::load_zip - unable to load zip group");
         }
         else
         {
            if ($a_row = $resulti->fetch_array())
            {
               $group_zip = $a_row[0];
               $result->leaders_savings_monthly_local = get_top_savings_monthly(-1, $group_zip, -1);
               $rsult->leaders_actions_local = get_top_completed_from_todo(-1, $group_zip, -1);
               $result->leaders_interactions_local = get_top_interactions(-1, $group_zip, -1);
            }
         }

         $result->leaders_savings_monthly_global = get_top_savings_monthly();
         $result->leaders_actions_global = get_top_completed_from_todo();
         $result->leaders_interactions_global = get_top_interactions();

         return $result;   
      }

   }

