<?php
   include_once 'globals.php';

function get_top_savings_monthly( $zip_code = -1, $id_group_zip_code = -1, $id_state = -1 )
{
   $query = "SELECT CONCAT(UCASE(LEFT(firstName, 1)), LCASE(SUBSTRING(firstName, 2))) as name_first, 
                    CONCAT(UCASE(LEFT(lastName, 1)), '.') as name_last,
                    kWh 
             FROM Users, Habitants, Savings_Monthlies, Residences WHERE ";
   $query .= the_zip_code_query( $zip_code, $id_group_zip_code, 'zip_code_Residences', $id_state );
   $query .= " id_residence=id_residence_Habitants
               AND userid=id_user_Habitants 
               AND id_residence_Habitants=`id_residence_Savings_Monthlies` 
               AND firstName!='' AND lastName!=''
               AND `date`> DATE_SUB(NOW(), INTERVAL 60 day)
               ORDER BY kWh DESC 
               LIMIT 10";
   return the_results( $query, 'get_top_savings_monthly' );
}

function get_top_completed_from_todo( $zip_code = -1, $id_group_zip_code = -1, $id_state = -1 )
{
   $query = "SELECT CONCAT(UCASE(LEFT(firstName, 1)), LCASE(SUBSTRING(firstName, 2))) as name_first, 
                    CONCAT(UCASE(LEFT(lastName, 1)), '.') as name_last,
                    Count(*) AS num_complete_from_todo 
             FROM Actions, Habitants, Users, Residences WHERE ";
   $query .= the_zip_code_query( $zip_code, $id_group_zip_code, 'zip_code_Residences', $id_state );
   $query .= " id_residence=id_residence_Habitants
               AND userid=id_user_Habitants 
               AND id_residence_Habitants=id_residence_Actions 
               AND firstName!='' AND lastName!=''
               AND completed=1 
               AND completed_from_todo=1 
               GROUP BY id_residence_Actions
               ORDER BY num_complete_from_todo DESC
               LIMIT 10";
   //error_log($query);
   return the_results( $query, 'get_top_completed_from_todo' );
}

function get_top_interactions($zip_code=-1, $id_group_zip_code=-1, $id_state=-1)
{
   $query = "SELECT CONCAT(UCASE(LEFT(firstName, 1)), LCASE(SUBSTRING(firstName, 2))) as name_first, 
                    CONCAT(UCASE(LEFT(lastName, 1)), '.') as name_last,
                    COUNT(*) num_interactions 
             FROM Interactions, Users";

   if ( $id_state != -1 )
   {
      $query .= ", Zip_Codes ";
   }

   $query .= " WHERE ";
   $query .= the_zip_code_query( $zip_code, $id_group_zip_code, 'zip', $id_state );
   $query .= "`id_user_Interactions`!= 0 AND id_user_Interactions = userid AND firstName!='' AND lastName!=''
             GROUP BY `id_user_Interactions` 
             ORDER BY num_interactions DESC
             LIMIT 10";
   //error_log($query);
   return the_results( $query, 'get_top_interactions' );
}

/* some boilerplate code to return the mysql query result as an array */
function the_results( $query, $error )
{
   $mysqli = connecti();

   if (!($result = $mysqli->query($query)))
   {
      error_log("Failed to leaderboard.php: ".$error.$mysqli->error);
      return null;
   }

   $results = array();
   while($a_row = $result->fetch_array(MYSQLI_NUM))
   {
      array_push($results, $a_row);
   }

   return $results;

}

function the_zip_code_query($zip_code=-1, $id_group_zip_code=-1, $zip_column_name, $id_state=-1)
{
   if ($zip_code != -1)
   {
     return sprintf("%s='%d' AND ", 
                  mysql_real_escape_string($zip_column_name),
                  mysql_real_escape_string($zip_code));
   }
   else if ($id_group_zip_code != -1)
   {
      return sprintf("%s IN (SELECT zip_code_Group_Members_Zip_Codes 
                                 FROM Group_Members_Zip_Codes 
                                 WHERE id_group_zip_code_Group_Members_Zip_Codes='%d') AND ",
                       mysql_real_escape_string($zip_column_name),
                       mysql_real_escape_string($id_group_zip_code));
   }
   else if ($id_state != -1)
   {
      return sprintf("%s=zip_code AND id_state_Zip_Codes=%d",
                     mysql_real_escape_string($zip_column_name),
                     mysql_real_escape_string($id_state));
   }
   else
   {
      return "";
   }
}

