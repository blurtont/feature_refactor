<?php
include_once 'RO_User.php';
include_once 'globals.php';
include_once 'emails.php';
include_once 'Type_Interaction.php';
include_once 'Service_Residence.php';
include_once 'Service_State.php';
include_once 'Service_Utility.php';
include_once 'Service_Zip_Code.php';

class Service_User
{

   /* login/create with Facebook */
   public function login_or_create_with_facebook(RO_User $user, $facebook_dump)
   {
      $mysqli = connecti();
      $user->errorMessage = "none";
      $user->errorType = -1;

      /* check if the user is already a user */
      $query = sprintf("SELECT * FROM Users WHERE id_facebook = '%s'",
                  mysql_real_escape_string($user->id_facebook));

      if (!($result = $mysqli->query($query)))
      {
         error_log("ERROR: failed to run query to select facebook user. ".$mysqli->error);
         return null;
      }

      if ($row = $result->fetch_assoc()) /* return user */
      {
         error_log("Facebook login successful!");
			$user->userID = (int) $row["userid"];
         Service_User::copy_to_user($row, $user);
         return $user;
      }
      else /* create account */
      {
         /* generate passResetKey */
         $passResetKey = randString(50);
         $query = sprintf("INSERT INTO Users (id_facebook, facebook_dump, firstName, lastName, passResetKey, unix_time_create_account)
                           values ('%s', '%s', '%s', '%s', '%s', UNIX_TIMESTAMP())",
   				  mysql_real_escape_string($user->id_facebook),
                 mysql_real_escape_string($facebook_dump),
	   			  mysql_real_escape_string(ucfirst(strtolower($user->nameFirst))),
		   		  mysql_real_escape_string(ucfirst(strtolower($user->nameLast))),
                 mysql_real_escape_string($passResetKey));
		   if (!($result = $mysqli->query($query)))
         {
   			error_log("unable to complete query. ".$mysqli->error);
            return null;
         }

         if ($id_user = $mysqli->insert_id)
         {
            $user->userID = $mysqli->insert_id;
            error_log("Facebook create user successful!");
            $service_user = new Service_User();
            return $service_user->login_no_pass($user);
         }
         else
         {
            error_log("ERROR: there was an error getting the insert id on facebook create user");
         }
      }

      /* should never get here */
	   return null;
	}

	// create method
	public function createUser(RO_User $user, $hear_about)
	{
		$mysqli = connecti();
		$user->email = strtolower($user->email);
		
		$user->errorMessage = "none";
      $user->errorType = -1;

		/* check if email is already registered */
		$query = sprintf("SELECT * FROM Users WHERE email = '%s'",
				 mysql_real_escape_string($user->email));
		if (!($result = $mysqli->query($query)))
      {
			 error_log("unable to complete query. ".$mysqli->error);
          return null;
      }
		if ($row = $result->fetch_array()) 
		{
	      $user->is_active = $row["is_active"];
         $user->userID = $row["userid"];
         $user->nameFirst = $row["firstName"];

         if($user->is_active > 0)
         {
			   /* there is already an account associated with $user->email */
			   $user->errorMessage = "We're sorry, but '".$user->email."' is already registered. If you already registered this email address, please wait for your account to be activated.";
            $user->errorType = 1;
			   return $user;
		   }
         else
         {
            //send account reactivation verification email
            $verifyEmailKey = randString(50);
            $user->errorMessage = "Our record shows that you had an account with us in the past. To reactivate your account, please follow the link in the verfication email just sent to your address. Welcome back!";
            $user->errorType = 8;//need an errorType
            Service_User::email_send_verify_reactivation($user);
            return $user;
         }
      }

      /* check if zip is valid */
      if (!Service_Zip_Code::is_valid_zip_code($user->zip))
      {
         /* invalid zip code */
         $user->errorMessage = "We're sorry, but we couldn't find the zip code you entered. Please try again.";
         $user->errorType = 6;
         return $user;
      }

      $mysqli = connecti();
      /* generate verifyEmailKey */
      $verifyEmailKey = randString(50); /* 36^50 = 6.53318624 x 10^77 */

      /* generate passResetKey */
      $passResetKey = randString(50);
      
      $query = sprintf("INSERT INTO Users (email, pass, firstName, lastName, zip, 
                                           verifyEmailKey, passResetKey, hear_about, unix_time_create_account)
              values ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', UNIX_TIMESTAMP())",
				  mysql_real_escape_string($user->email),
				  hash("sha512", mysql_real_escape_string($user->password)),
				  mysql_real_escape_string(ucfirst(strtolower($user->nameFirst))),
				  mysql_real_escape_string(ucfirst(strtolower($user->nameLast))),
              mysql_real_escape_string($user->zip),
              mysql_real_escape_string($verifyEmailKey),
              mysql_real_escape_string($passResetKey),
              mysql_real_escape_string($hear_about));
		if (!($result = $mysqli->query($query)))
      {
			error_log("unable to compslete query. ".$mysqli->error);
         return null;
      }


		/* use the id we just created to se the id o fthe original user object) */
		$user->userID = $mysqli->insert_id;
      $user->nameFirst = ucfirst(strtolower($user->nameFirst));
      $user->nameLast = ucfirst(strtolower($user->nameLast));
		
      /* close the mysql conneciton */
      $mysqli->close();
      
      /* record interaction */
      $interaction = new Interaction();
      $interaction->id_user = $user->userID;
      $interaction->id_type_interaction = Type_Interaction::ACCOUNT_CREATE;
      $interaction->insert();
		$user->password = "";
		return $user;
	}


   public function email_send_verify_reactivation(RO_User $user)
   {
      $mysqli = connecti();
      $query = sprintf("SELECT * FROM Users WHERE userid='%d'", mysql_real_escape_string($user->userID));
      if (!($result = $mysqli->query($query)))
      {
         error_log("failed to send reactivation email. ".$mysqli->error);
      }

      if ($a_row = $result->fetch_assoc())
      {
         $verifyEmailKey = $a_row['verifyEmailKey'];
      }
      else
      {
         error_log ("Failed to look up user with id=".$user->userID);
      }

      $verifyLink = 'http://'.get_server_address().'/verify.php?address='.$user->email.'&key='.$verifyEmailKey;
      $body = getEmailReactivation($user->nameFirst, $verifyLink);
      $subject = "Dropoly.com - Please verify your email address";
      sendDropolyEmail($user->email, $user->nameFirst, $body, $subject);
   }

   public static function remove($rem_email)//RO_User $user)
   {
      $mysqli = connecti();
      $user = Service_User::login_with_email($rem_email);
      if($user == NULL)
      {
         return false;
      }
      $state = Service_State::load_state($user);
      $user->remove();
      Service_Residence::remove($state->residence);
      Service_Utility::remove($state->utility_bills, $state->residence->id_residence);
      return true;
   }

   public static function duplicate($email, $new_email, $new_pass)
   {
      $mysqli = connecti();
      $user = Service_User::login_with_email($email);

      if ($user == NULL)
      {
         return false;
      }

      $ro_residence = Service_Residence::load($user->userID);
      $ro_utility_bills = Service_Utility::load($ro_residence->id_residence);
      $new_userid = $user->duplicate($new_email, $new_pass);
      
      $id_residence_dup = Service_Residence::duplicate($ro_residence, $new_userid);
      error_log("dupped residence.");
      Service_Utility::duplicate($ro_utility_bills, $id_residence_dup);

      return true;
   }

	/* loginUser method */
	public function loginUser(RO_User $user)
	{
		$mysqli = connecti();
		
		$user->email = strtolower($user->email);
		$user->errorMessage = "none";
      $user->errorType = -1;

		$query = sprintf( "SELECT userid,
                                firstName,
                                lastName,
                                unix_time_create_account,
                                motivation_fraction_money,
                                is_do_it_yourself,
                                type_usage_self_perception,
                                is_tutorial_homepage_complete,
                                is_tutorial_page_community_complete,
                                is_tutorial_page_results_complete,
                                is_tutorial_page_todo_complete,
                                is_tutorial_page_utility_complete,
                                zip,
                                hear_about,
                                interest_partnering,
                                phone_number,
                                address_street,
                                notification_email_updates,
                                notification_text_updates,
                                notification_email_challenges,
                                notification_text_challenges,
                                notification_email_promotions,
                                notification_text_promotions,
                                is_active
                         FROM Users WHERE pass = '%s' AND email = '%s' AND is_active = '1'",
				            hash("sha512", mysql_real_escape_string($user->password)),
				            mysql_real_escape_string($user->email));

      /* clear password */
      $user->password = "";

      if (!($result = $mysqli->query($query)))
      {
			error_log("ERROR: Login: unable to complete query. ".$mysqli->error);
			$user->errorMessage = "server error, please try again";
         $user->errorType = 5;
         return $user;
      }

      /* catch errors */
		if ($row = $result->fetch_assoc())
		{
			$user->userID = (int) $row["userid"];
         $user->nameFirst = $row["firstName"];
         $user->nameLast = $row["lastName"];
         $user->unix_time_create_account = $row["unix_time_create_account"];
         $user->motivation_fraction_money = (int) $row["motivation_fraction_money"];
         $user->is_do_it_yourself = (int) $row["is_do_it_yourself"];
         $user->type_usage_self_perception = $row["type_usage_self_perception"];
         $user->is_tutorial_homepage_complete = (int) $row["is_tutorial_homepage_complete"];
         $user->is_tutorial_page_community_complete = (int) $row["is_tutorial_page_community_complete"];
         $user->is_tutorial_page_results_complete = (int) $row["is_tutorial_page_results_complete"];
         $user->is_tutorial_page_todo_complete = (int) $row["is_tutorial_page_todo_complete"];
         $user->is_tutorial_page_utility_complete = (int) $row["is_tutorial_page_utility_complete"];
         $user->zip = (int) $row["zip"];
         $user->hear_about = $row["hear_about"];
         $user->interest_partnering = (int) $row["interest_partnering"];
         $user->phone_number = (int) $row['phone_number'];
         $user->address_street = $row['address_street'];
         $user->notification_email_updates = (int) $row['notification_email_updates'];
         $user->notification_text_updates = (int) $row['notification_text_updates'];
         $user->notification_email_challenges = (int) $row['notification_email_challenges'];
         $user->notification_text_challenges = (int) $row['notification_text_challenges'];
         $user->notification_email_promotions = (int) $row['notification_email_promotions'];
         $user->notification_text_promotions = (int) $row['notification_text_promotions'];
         $user->is_active = (int) $row['is_active'];
		}
	   else
		{
			$user->errorMessage = "invalid email or password";
         $user->errorType = 5;
         return $user;
		}

      /* if no errors, user is valid and logged in */
      /* record interaction */
      $interaction = new Interaction();
      $interaction->id_user = $user->userID;
      $interaction->id_type_interaction = Type_Interaction::ACCOUNT_LOGIN;
      $interaction->insert();

      /* log latest ip */
      $query = sprintf("UPDATE Users SET ip_latest='%d' WHERE userid='%d'",
                  mysql_real_escape_string(ip2long($_SERVER['REMOTE_ADDR'])),
                  mysql_real_escape_string($user->userID));
      if (!$mysqli->query($query))
      {
         error_log("Failed to execute update ip_latest query. ".$mysqli->error);
      }

		return $user;
	}

	/* login_no_pass method */
	public function login_no_pass(RO_User $user_param)
	{
		$mysqli = connecti();
      $user = new RO_User();
		$user->userID = $user_param->userID;

		$user->errorMessage = "none";
      $user->errorType = -1;

		$query = sprintf("SELECT * FROM Users WHERE userid='%d'",
				  mysql_real_escape_string($user->userID));
      
      /* clear password */
      $user->password = "";

		if (!($result = $mysqli->query($query)))
      {
			error_log("ERROR: Service_User login_no_pass: unable to complete query. ".$mysqli->error);
      }

      /* catch errors */
		if ($row = $result->fetch_assoc())
		{
         Service_User::copy_to_user($row, $user);
		}
	   else
		{
			$user->errorMessage = "invalid email or password";
         $user->errorType = 5;
         error_log("failed to load user");
		}
      /* if no errors, user is valid and logged in */
      
      /* record interaction */
      $interaction = new Interaction();
      $interaction->id_user = $user->userID;
      $interaction->id_type_interaction = Type_Interaction::ACCOUNT_LOGIN;
      $interaction->insert();

      return $user;
	}

   public static function login_with_email($email)
   {
      $mysqli = connecti();
      $query = sprintf("SELECT * FROM Users WHERE email='%s'",
                  mysql_real_escape_string($email));
      if (!($result = $mysqli->query($query)))
      {
         error_log("ERROR: Service_User, Failed to login_with_email. ".$mysqli->error);
      }

      if ($row = $result->fetch_assoc())
      {
         $user = new RO_User();
         return (Service_User::copy_to_user($row, $user));
      }
      else
      {
         return NULL;
      }
   }

   static function copy_to_user($row, RO_User $user)
   {
         $user->userID = (int) $row["userid"];
         $user->email = $row["email"];
         $user->id_facebook = $row["id_facebook"];
         $user->nameFirst = $row["firstName"];
         $user->nameLast = $row["lastName"];
         $user->unix_time_create_account = $row["unix_time_create_account"];
         $user->motivation_fraction_money = (int) $row["motivation_fraction_money"];
         $user->is_do_it_yourself = (int) $row["is_do_it_yourself"];
         $user->type_usage_self_perception = (int) $row["type_usage_self_perception"];
         $user->is_tutorial_homepage_complete = (int) $row["is_tutorial_homepage_complete"];
         $user->is_tutorial_page_community_complete = (int) $row["is_tutorial_page_community_complete"];
         $user->is_tutorial_page_results_complete = (int) $row["is_tutorial_page_results_complete"];
         $user->is_tutorial_page_todo_complete = (int) $row["is_tutorial_page_todo_complete"];
         $user->is_tutorial_page_utility_complete = (int) $row["is_tutorial_page_utility_complete"];
         $user->zip = (int) $row["zip"];
         $user->hear_about = $row["hear_about"];
         $user->interest_partnering = (int) $row["interest_partnering"];
         $user->phone_number = (int) $row['phone_number'];
         return $user;
   }

   function resendVerifyEmail(RO_User $user)
   {
      $mysqli = connecti();
		$user->email = strtolower($user->email);
      $user->errorMessage = "none";

      $query = sprintf("SELECT email, verifyEmailKey FROM Users WHERE email='%s' AND pass='%s'",
                  mysql_real_escape_string($user->email),    
    				   hash("sha512", mysql_real_escape_string($user->password)));

      /* clear password */
      $user->password = "";

      if(!($result = $mysqli->query($query)))
      {
         error_log("failed to run query. ".$mysqli->error);
		}
      if ($row = $result->mysqli->fetch_assoc($result))
		{
         /* found the user */
         $verifyEmailKey = $row['verifyEmailKey'];
         $verifyLink = get_server_address().'/verify.php?address='.$user->email.'&key='.$verifyEmailKey;
         /* send email */
         $body = getEmailConfirmEmail($user->nameFirst, $verifyLink);
         $subject = "Dropoly.com - Please verify your email address";

         sendDropolyEmail($user->email, $user->nameFirst, $body, $subject);
      }
      else
      {
         $user->errorMessage = "Invalid login.";
         $user->errorType = 5;
      }
		/* and return the user object */
		return $user;
	}

   /* generate and end reset password key */
   function sendResetPasswordKey(RO_User $user)
   {
      $mysqli = connecti();
		$user->email = strtolower($user->email);
      $user->errorMessage = "none";

      /* TODO: build query to get the key for the email of user */
      $query = sprintf("SELECT email, firstName FROM Users WHERE email='%s'",
                  mysql_real_escape_string($user->email));    
      if (!($result = $mysqli->query($query)))
      {
         error_log("failed to run lookup query. ".$mysqli->error);
         return null;
      }
		
      if ($row = $result->fetch_assoc())
		{
         /* found the user */
         $passResetKey = randString(50);

         /* update passResetKey in db */

         $query = sprintf("UPDATE Users SET passResetKey='%s' WHERE email='%s'",
                     mysql_real_escape_string($passResetKey),
                     mysql_real_escape_string($user->email));

         if (!$mysqli->query($query))
         {
            error_log('failed update key. '.$mysqli->error);
            return null;
         }

         /* send email */
         $body = getEmailResetPasswordCode($user->nameFirst, $passResetKey);
         $subject = "Dropoly.com - Password reset code";
         
         sendDropolyEmail($user->email, $user->nameFirst, $body, $subject);
      }
      else
      {
         $user->errorMessage = "invalid login.";
         $user->errorType = 5;
      }

      $interaction = new Interaction();
      $interaction->id_user = $user->userID;
      $interaction->id_type_interaction = Type_Interaction::ACCOUNT_PASSWORD_SEND_RESET_CODE;
      $interaction->insert();

      return $user;
   }

   function resetPassword(RO_User $user, $passResetKey)
   {
      $mysqli = connecti();
		$user->email = strtolower($user->email);
      $user->errorMessage = "none";

      /* TODO: build query to get the key for the email of user */
      $query = sprintf("SELECT email FROM Users WHERE email='%s' AND passResetKey='%s'",
                  mysql_real_escape_string($user->email),
                  mysql_real_escape_string($passResetKey));
      if (!($result = $mysqli->query($query)))
      {
         error_log("failed to lookup. ".$mysqli->error);
         return null;
      }
		
      if ($row = $result->fetch_assoc())
		{
         /* found the user */
         /* update the password */
         $query = sprintf("UPDATE Users SET pass='%s' WHERE email='%s'AND passResetKey='%s'",
                     hash("sha512", mysql_real_escape_string($user->password)),
                     mysql_real_escape_string($user->email),
                     mysql_real_escape_string($passResetKey));

         if (!($mysqli->query($query)))
         {
            error_log("failed to update. ".$mysqli->error);
            return null;
         }

         /* update passResetKey */
         $passResetKey = randString(50);
         $query = sprintf("UPDATE Users SET passResetKey='%s' WHERE email='%s'",
                     mysql_real_escape_string($passResetKey),
                     mysql_real_escape_string($user->email));

         if (!($mysqli->query($query)))
         {
            error_log('ERROR: fail to update key. '.$mysqli->error);
            return null;
         }
      }
      else
      {
         $user->errorMessage = "invalid login. ";
         $user->errorType = 5;
      }

      $interaction = new Interaction();
      $interaction->id_user = $user->userID;
      $interaction->id_type_interaction = Type_Interaction::ACCOUNT_PASSWORD_RESET;
      $interaction->insert();

      return $user;
   }

   function update_new_password(RO_User $ro_user, $password_old, $password_new)
   {
      $mysqli = connecti();
      $query = sprintf("UPDATE Users SET pass='%s' WHERE email='%s' AND pass='%s'",
            hash("sha512", mysql_real_escape_string($password_new)),
            mysql_real_escape_string(strtolower($ro_user->email)),
            hash("sha512", mysql_real_escape_string($password_old)));

      $result = $mysqli->query($query);
      if( !$result )
      {
         error_log("ERROR: failed to update user password".$mysqli->error);
         return False;
      }
      else
      {
         return $mysqli->affected_rows == 1;
      }
   }

   public function email_send_welcome(RO_User $user)
   {
      $mysqli = connecti();
      $body = getEmailWelcome($user->nameFirst);
      $subject = "Welcome To Dropoly!";

      sendDropolyEmail($user->email, $user->nameFirst, $body, $subject);
   }

   public function email_send_verify(RO_User $user)
   {
      $mysqli = connecti();
      $query = sprintf("SELECT * FROM Users WHERE userid='%d'", mysql_real_escape_string($user->userID));
      if (!($result = $mysqli->query($query)))
      {
         error_log("failed to send verification email. ".$mysqli->error);
      }

      if ($a_row = $result->fetch_assoc())
      {
         $verifyEmailKey = $a_row['verifyEmailKey'];
      }
      else
      {
         error_log ("Failed to look up user with id=".$user->userID);
      }

      $verifyLink = get_server_address().'/verify.php?address='.$user->email.'&key='.$verifyEmailKey;
      $body = getEmailConfirmEmail($user->nameFirst, $verifyLink);
      $subject = "Dropoly.com - Please verify your email address";
      sendDropolyEmail($user->email, $user->nameFirst, $body, $subject);
   }
   
   public function update_common(RO_User $user)
   {
      $user->email = strtolower($user->email);
      RO_User::update_common($user);
   }

   public function update_new_email(RO_User $user, $email_address_new)
   {
      return RO_User::update_new_email($user, $email_address_new);
   }
   
   public function remove_account(RO_User $user)
   {
      return RO_User::remove_account($user);
   }

}
?>
