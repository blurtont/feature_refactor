<?php
   include_once 'RO_RLDs.php';
   include_once 'RO_TLDs.php';

   class RO_Room
   {
      public $room_name; /* the room name */
      public $id_room;
      public $type_room;
      public $type_size;

      const SMALL = 1;
      const MEDIUM = 2;
      const LARGE = 3;
      const WHOLE_FLOOR = 4;

      public $ro_devices_remote; /* array of ALL devices */

      static function create_default($id_residence, $type_room)
      { 
         $mysqli = connecti();
         /* create room instance */
         $query = sprintf("INSERT INTO Rooms (id_residence_Rooms, type_room_Rooms) VALUES ('%d', '%d')",
                     mysql_real_escape_string($id_residence),
                     mysql_real_escape_string($type_room));
         if (!($mysqli->query($query)))
         {
            error_log (sprintf("Failed to create new room of type '%d' in RO_Room. ", $type_room).$mysqli->error);
            return null;
         }
         $id_room = $mysqli->insert_id;

         /* switch over room type to determine which RLDs and TLDs to add and add them*/
         switch($type_room)
         {
            case 1: /* attic */
               $room_name = "Attic";
               $type_size = RO_Room::WHOLE_FLOOR;
               break;

            case 2: /* basement */
               $room_name = "Basement";
               $type_size = RO_Room::WHOLE_FLOOR;
               break;

            case 3:
            case 4:
               $room_name = "Bedroom";
               $type_size = RO_Room::MEDIUM;
               break;

            case 5:
               $room_name = "Dining Room";
               $type_size = RO_Room::MEDIUM;
               break;

            case 6:
               $room_name = "Half Bathroom";
               $type_size = RO_Room::SMALL;
               break;

            case 7:
               $room_name = "Kitchen";
               $type_size = RO_Room::LARGE;
               break;

            case 8:
               $room_name = "Living Room";
               $type_size = RO_Room::LARGE;
               break;

            case 9:
               $room_name = "Bathroom";
               $type_size = RO_Room::SMALL;
               break;

            case 10:
               $room_name = "Master Bedroom";
               $type_size = RO_Room::LARGE;
               break;

            case 11:
               $room_name = "Home Office";
               $type_size = RO_Room::MEDIUM;
               break;

            case 12:
               $room_name = "Living Room";
               $type_size = RO_Room::MEDIUM;
               break;

            case 13:
               $room_name = "Upstairs Bathroom";
               $type_size = RO_Room::SMALL;
               break;

            case 14:
               $room_name = "Utilities";
               $type_size = RO_Room::MEDIUM;
               break;

            default:
               $room_name = "ERROR: Undefined Room Type";
               return;
         }
         $query = sprintf("UPDATE Rooms 
                           SET room_name='%s', type_size_Rooms='%d' 
                           WHERE id_room='%d'",
                           mysql_real_escape_string($room_name),
                           mysql_real_escape_string($type_size),
                           mysql_real_escape_string($id_room));
         if (!($mysqli->query($query)))
         {
            error_log ("Failed to set room_name ".$room_name.$mysqli->error);
            return null;
         }
         
         $ro_room = RO_Room::load($id_room);
         return $ro_room;
      }  

      static function load_rooms($id_residence)
      {
         $mysqli = connecti();
         $ro_rooms = array();

         /* get rooms */
         $query = sprintf("SELECT * FROM Rooms 
                           WHERE id_residence_Rooms='%d'",
                     mysql_real_escape_string($id_residence));
         if (!($result = $mysqli->query($query)))
         {
            error_log("Failed to get rooms. ".$mysqli->error);
            return null;
         }
         while ($a_row = $result->fetch_assoc())
         {
            $new_room = new RO_Room();
            $new_room->id_room = (int) $a_row['id_room'];
            $new_room->type_room = (int) $a_row['type_room_Rooms'];
            $new_room->type_size = (int) $a_row['type_size_Rooms'];
            $new_room->room_name = $a_row['room_name']; 

            /* load devices */
            $rlds = RO_RLDs::load_rlds($new_room->id_room);
            $tlds = RO_TLDs::load_tlds($new_room->id_room);
            $new_room->ro_devices_remote = array_merge($rlds->get_devices(), $tlds->get_devices());
            array_push($ro_rooms, $new_room);
         }

         /* return RO_Residence */
         return $ro_rooms;
      }

      static function load($id_room)
      {
         $ro_room = new RO_Room();
         $mysqli = connecti();

         $query = sprintf("SELECT * FROM Rooms WHERE id_room='%d'",
                           mysql_real_escape_string($id_room));
         if (!($result = $mysqli->query($query)))
         {
            error_log("RO_Room.load(id_room): failed to select room with id: ".$id_room.$mysqli->error);
            return null;
         }
         if ($a_row = $result->fetch_assoc())
         {
            $ro_room->id_room = (int)$a_row['id_room'];
            $ro_room->type_room = (int)$a_row['type_room_Rooms'];
            $ro_room->type_size = (int)$a_row['type_size_Rooms'];
            $ro_room->room_name = $a_row['room_name'];
            $rlds = RO_RLDs::load_rlds($ro_room->id_room);
            $tlds = RO_TLDs::load_tlds($ro_room->id_room);
            $ro_room->ro_devices_remote = array_merge($rlds->get_devices(), $tlds->get_devices());
            return $ro_room;
         }
         else
         {
            error_log("unable to load room with id_room=".$id_room);
         }
      }

      static function duplicate_room($ro_room, $id_residence_dup)
      {
         $mysqli = connecti();
         /* create duplicate room */
         $query = sprintf("INSERT INTO Rooms (id_residence_Rooms) VALUES ('%d')", mysql_real_escape_string($id_residence_dup));;
         if (!($mysqli->query($query))) 
         { 
            error_log (sprintf("Failed to create duplicate room of type '%d' in RO_Room. ".$ro_room->type_room).$mysqli->error);
            return null;
         }
         $id_room_dup = $mysqli->insert_id;

         $ro_room->id_room = $id_room_dup;
         /* update residence -- this will redundantly store all devices and actions, don't care */
         $query = sprintf("UPDATE Rooms
                           SET type_room_Rooms='%d',
                               type_size_Rooms='%d',
                               room_name='%s'
                           WHERE id_room='%d'",
                           mysql_real_escape_string($ro_room->type_room),
                           mysql_real_escape_string($ro_room->type_size),
                           mysql_real_escape_string($ro_room->room_name),
                           mysql_real_escape_string($ro_room->id_room));
         if (!($mysqli->query($query)))
         {
            error_log("Failed to update Rooms with id_room=".$ro_room->id_room." .".$mysqli->error);
            return null;
         }

         /* duplicate rlds and tlds */
         trace("ERROR: need to rewrite RO_Room::duplicate_room to handle ro_devices_remote instead of tlds/rlds");
         $ro_room->rlds->duplicate($id_room_dup);
         $ro_room->tlds->duplicate($id_room_dup);

         //hackily doing this here since devices don't care what residence they're in but their actions do
         $devices = array_merge($ro_room->rlds->get_devices(), $ro_room->tlds->get_devices());
         foreach($devices as $device)
         {
            foreach($device->ro_actions as $ro_action)
            {
               $ro_action->id_residence_Actions = $id_residence_dup;
               RO_Action::update($ro_action);
            }
         } 
         return $ro_room;
      }

      static function remove(RO_Room $ro_room)
      {
         $mysqli = connecti();
         foreach($ro_room->ro_devices_remote as $ro_device)
         {
            $ro_device->remove();
         }

         $query = sprintf("DELETE FROM Rooms WHERE id_room='%d'",
                          mysql_real_escape_string($ro_room->id_room));
         if (!($mysqli->query($query))) 
         { 
            error_log ("failed to delete room with id_room=".$id_room);
            return null;
         }
      }

      static function update(Array $ro_rooms)
      {
         $mysqli = connecti();

         RO_Room::update_shallow_multiple($ro_rooms);

         foreach($ro_rooms as $ro_room)
         {
            foreach($ro_room->ro_devices_remote as $ro_device)
            {
               if ($ro_device->invalidated)
               {
                  $ro_device::update($ro_device);
               }
            }
         }
      }

      static function update_shallow($ro_room)
      {
         $mysqli = connecti();
         $query = sprintf("UPDATE Rooms
                           SET type_room_Rooms='%d',
                               room_name='%s'
                           WHERE id_room='%d'",
                           mysql_real_escape_string($ro_room->type_room),
                           mysql_real_escape_string($ro_room->room_name),
                           mysql_real_escape_string($ro_room->id_room));
         if (!($mysqli->query($query))) 
         {
            error_log("Failed to update Rooms with id_room=".$ro_room->id_room." .".$mysqli->error);
            return null;
         }
         return true;
      }

      static function update_shallow_multiple(Array $ro_rooms)
      {
         $mysqli = connecti();
         foreach($ro_rooms as $ro_room)
         {
            $query = sprintf("UPDATE Rooms
                              SET type_room_Rooms='%d',
                                  room_name='%s'
                              WHERE id_room='%d'",
                              mysql_real_escape_string($ro_room->type_room),
                              mysql_real_escape_string($ro_room->room_name),
                              mysql_real_escape_string($ro_room->id_room));
            if (!($mysqli->query($query))) 
            {
               error_log("Failed to update Rooms with id_room=".$ro_room->id_room." .".$mysqli->error);
               return null;
            }
         }
         return true;
      }

      static function duplicate($ro_rooms, $id_residence_dup)
      {
         foreach($ro_rooms as $ro_room)
         {
            RO_Room::duplicate_room($ro_room, $id_residence_dup);
         }
      }

   }
?>
