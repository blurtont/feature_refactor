<?php
include_once "globals.php";

class RO_Utility_Bill
{
   public $id_utility_bill;
   public $type_utility_bill;
   public $unix_time;
   public $energy;

   static function create_default( $ro_residence )
   {
      $mysqli = connecti( );
      if ( $ro_residence->id_residence !== 0 )
      {
         $old_bills = RO_Utility_Bill::load( $ro_residence->id_residence );
         RO_Utility_Bill::remove( $old_bills );

         $utility_bills = RO_Utility_Bill::load( 0 );
         foreach( $utility_bills as $utility_bill )
         {
            $utility_bill->id_residence_Utility_Bills = $ro_residence->id_residence;
            $utility_bill->energy *= $ro_residence->square_footage / 2000;
            $query = sprintf( "INSERT INTO Utility_Bills (id_residence_Utility_Bills, 
                                                          id_type_utility_bill_Utility_Bills, 
                                                          energy_native_units, 
                                                          unix_time)
                               VALUES ('%d', '%d', '%d', '%s')",
                              mysql_real_escape_string( $utility_bill->id_residence_Utility_Bills ),
                              mysql_real_escape_string( $utility_bill->type_utility_bill ),
                              mysql_real_escape_string( $utility_bill->energy ),
                              mysql_real_escape_string( $utility_bill->unix_time ) );
            if ( !( $mysqli->query( $query ) ) )
            {
               error_log( "ERROR: failed to insert utility bill. " . $mysqli->error );
               return null;
            }
         }

         $remove_types_array = array( );
         if( !$ro_residence->is_fuel_type_natural_gas )
         {
            array_push( $remove_types_array, 1 );
         }
         if( !$ro_residence->is_fuel_type_electric )
         {
            array_push( $remove_types_array, 2 );
         }
         if( !$ro_residence->is_fuel_type_propane )
         {
            array_push( $remove_types_array, 3 );
         }
         if( !$ro_residence->is_fuel_type_fuel_oil )
         {
            array_push( $remove_types_array, 4 );
         }

         RO_Utility_Bill::remove_all_of_types( $ro_residence->id_residence, $remove_types_array );
      }
      return RO_Utility_Bill::load( $ro_residence->id_residence );
   }

   static function create( $id_residence, $type_utility, $energy, $meter_date_zend_date )
   {
      $mysqli = connecti( );
      $query = sprintf( "INSERT INTO Utility_Bills (id_residence_Utility_Bills, 
                                                    id_type_utility_bill_Utility_Bills, 
                                                    energy_native_units, 
                                                    unix_time)
                         VALUES ('%d', '%d', '%d', UNIX_TIMESTAMP('%s'))",
                        mysql_real_escape_string( $id_residence ),
                        mysql_real_escape_string( $type_utility ),
                        mysql_real_escape_string( $energy ),
                        mysql_real_escape_string( $meter_date_zend_date->toString( 'yyyy-MM-dd HH:mm:ss' ) ) );

      if ( !( $mysqli->query( $query ) ) ) 
      { 
         error_log( "ERROR: Failed to insert utility bill. " . $mysqli->error );
         return null;
      }
   }

   public function delete( )
   {
      $mysqli = connecti( );
      $query = sprintf( "DELETE FROM Utility_Bills WHERE id_utility_bill='%d'",
                        mysql_real_escape_string( $this->id_utility_bill ) );

      if ( !( $mysqli->query( $query ) ) ) 
      { 
         error_log( "ERROR: failed to delete utility bill. " . $mysqli->error );
         return null;
      }
   }

   static function load( $id_residence )
   {
      $mysqli = connecti( );
      $query = sprintf( "SELECT id_utility_bill,
                                id_type_utility_bill_Utility_Bills,
                                unix_time,
                                energy_native_units
                         FROM Utility_Bills 
                         WHERE id_residence_Utility_Bills='%d'
                         ORDER BY unix_time ASC",
                        mysql_real_escape_string( $id_residence ) );

      if ( !( $result = $mysqli->query( $query ) ) )
      {
         error_log ( "failed to select utility bills for id_residence = " . $id_residence.$mysqli->error );
         return null;
      }

      $utility_bills = array( );
      while ( $a_row = $result->fetch_assoc( ) )
      {
         $ro_utility_bill = new RO_Utility_Bill( );
         $ro_utility_bill->id_utility_bill = (int) $a_row[ 'id_utility_bill' ];
         $ro_utility_bill->type_utility_bill = (int) $a_row[ 'id_type_utility_bill_Utility_Bills' ];
         $ro_utility_bill->unix_time = $a_row[ 'unix_time' ];
         $ro_utility_bill->energy = (int) $a_row[ 'energy_native_units' ];

         array_push( $utility_bills, $ro_utility_bill );
      }
      return $utility_bills;
   }

   public function duplicate( $id_residence_dup )
   {
      $mysqli = connecti();
      $query = sprintf( "INSERT INTO Utility_Bills (id_residence_Utility_Bills, 
                                                    id_type_utility_bill_Utility_Bills, 
                                                    unix_time, 
                                                    energy_native_units)
                         VALUES ('%d', '%d', '%d', '%d')",
                        mysql_real_escape_string( $id_residence_dup ),
                        mysql_real_escape_string( $this->type_utility_bill ),
                        mysql_real_escape_string( $this->unix_time ),
                        mysql_real_escape_string( $this->energy ) );

      if ( !( $mysqli->query( $query ) ) )
      {
         error_log( "failed to insert utility bills for duplicate" . $mysqli->error );
         return null;
      }
      $this->id_utility_bill = $mysqli->insert_id;
   }

   static function remove( $utility_bills )
   {
      foreach( $utility_bills as $utility_bill )
      {
         $utility_bill->delete( );
      }
   }

   static function remove_all_of_types( $id_residence, $fuel_types )
   {
      $mysqli = connecti( );
      foreach ( $fuel_types as $fuel_type )
      {
         $query = sprintf( "DELETE FROM Utility_Bills 
                            WHERE id_residence_Utility_Bills='%d' 
                            AND id_type_utility_bill_Utility_Bills='%d'",
                           mysql_real_escape_string( $id_residence ),
                           mysql_real_escape_string( $fuel_type ) );
         if ( !( $mysqli->query( $query ) ) ) 
         {
            error_log( "ERROR: RO_Utility_Bill.php: failed to remove_all_of_types. " . $mysqli->error );
            return null;
         }
      }
   }

   static function update( $utility_bills )
   {
      $mysqli = connecti( );
      foreach( $utility_bills as $utility_bill )
      {   
         $query = sprintf( "UPDATE Utility_Bills 
                            SET id_type_utility_bill_Utility_Bills='%d',
                                unix_time='%d',
                                energy_native_units='%d'
                            WHERE id_utility_bill='%d'",
                           mysql_real_escape_string( $utility_bill->type_utility_bill ),
                           mysql_real_escape_string( $utility_bill->unix_time ),
                           mysql_real_escape_string( $utility_bill->energy ),
                           mysql_real_escape_string( $utility_bill->id_utility_bill ) );
         if ( !( $mysqli->query( $query ) ) )
         {
            error_log ( "failed to update utility bill with id = " . $utility_bill->id_utility_bill . $mysqli->error );
            return null;
         }
      }  
   }

   static function update_new($utility_bills, $id_residence)
   {
      /* brute force! remove all utility bills and insert as new */
      /* the problem here is the complexity of the GUI... 
         much better to brute force here at minor increase in server cost (instead of overly complete client side dev) */

      $mysqli = connecti( );
      $query = sprintf( "DELETE FROM Utility_Bills 
                         WHERE id_residence_Utility_Bills='%d'", 
                        mysql_real_escape_string( $id_residence ) );

      if ( !( $mysqli->query( $query ) ) ) 
      { 
         error_log( "ERROR: RO_Utility_Bill->update_new: failed to remove all old utility bills. " . $mysqli->error );
         return null;
      }

      foreach( $utility_bills as $utility_bill )
      {
         $query = sprintf( "INSERT INTO Utility_Bills (id_residence_Utility_Bills, 
                                                       id_type_utility_bill_Utility_Bills, 
                                                       energy_native_units, 
                                                       unix_time)
                            VALUES ('%d', '%d', '%d', '%d')",
                           mysql_real_escape_string( $id_residence ),
                           mysql_real_escape_string( $utility_bill->type_utility_bill ),
                           mysql_real_escape_string( $utility_bill->energy ),
                           mysql_real_escape_string( $utility_bill->unix_time) );

          if ( !( $mysqli->query( $query ) ) ) 
          {
             error_log ("ERROR: RO_Utility_Bill->update_new: failed to insert new bill. ".$mysqli->error);
             return null;
          }
      }
   }
}
?>
