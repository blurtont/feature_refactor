<?php
   include_once 'RO_User.php';
   include_once 'globals.php';
   include_once 'emails.php';

   function verify_account_delete($email, $key)
   {
      $email = urlencode_email($email);
      $mysqli = connecti();
      $outputMessage = "We're sorry, there was an error in the request.";
      if (strlen($key) != 50)
      {
         error_log ('incorrect account delete key length');
         return $outputMessage;   
      }

      $query = sprintf("SELECT userid, firstName FROM Users WHERE email='%s' AND key_account_delete='%s'",
                  mysql_real_escape_string($email),
                  mysql_real_escape_string($key));

      if (!($result = $mysqli->query($query))) 
      {
         error_log("failed to run query. ".$mysqli->error);
         return null;
      }

      if ($row = $result->fetch_assoc())
      {
         /* the email/key_email_reset matched, update email */
         $query = sprintf("UPDATE Users SET is_active='0', key_account_delete=null WHERE userID='%d'",
                     mysql_real_escape_string($row['userid']));
         
         if (!($mysqli->query($query)))
         {
            error_log('verify_account_delete, failed to update Users table.'.$mysqli->error);
            return null;
         }

         $outputMessage = "Your account ".$email." has succesfully been deleted.";
         $pid = pcntl_fork();
         if ($pid == -1) 
         { 
            error_log("Verify_account_delete: fork failed");
            return $outputMessage;
         }   
         else if ($pid)
         {   
            /* parent flow control continues */
            return $outputMessage;
         }
         else
         {
            return $outputMessage;
         }
      }
   }
?>
