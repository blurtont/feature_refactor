<?php
include_once 'RO_TMY3_Datapoint.php';
include_once 'globals.php';

class Service_TMY3
{
   public function get_TMY3($zip_code)
   {
      $mysqli = connecti_weather2(); /* globals.php */

      /* get station ids from zip code */
      $query_get_station_ids = sprintf("SELECT id_usaf_TMY3_Zip_Codes FROM Zip_Codes
                                        WHERE zip_code = '%d'",
                                        $zip_code);

      if (!($result = $mysqli->query($query_get_station_ids)))
      {
         error_log("Failed to run query: ".$query_get_station_ids."\n".$mysqli->error);
         return null;
      }

      if ($row = $result->fetch_assoc())
      {
         $id_usaf_TMY3_Zip_Codes = $row['id_usaf_TMY3_Zip_Codes'];
      }
      else
      {
         error_log("Failed to lookup zip code ".$request->zip."\n");
         return null;
      }

      /* get TMY3 data*/
      $query_temperatures = sprintf("SELECT unix_time, 
                                            temperature_air_degree_celsius_x10
                                     FROM Weather_Hourly_TMY3
                                     WHERE id_usaf = '%d'",
                                     mysql_real_escape_string($id_usaf_TMY3_Zip_Codes));

      if (!($result = $mysqli->query($query_temperatures)))
      {
         error_log("Failed to run query: ".$query_temperatures."\n".$mysqli->error);
         return null;
      }

      $datapoints = array();
      while ($row = $result->fetch_array(MYSQLI_NUM))
      {
         $datapoint = new RO_TMY3_Datapoint();
         $datapoint->unix_time = $row[0];
         $datapoint->temperature_air_degree_celsius_x10 = $row[1];
         array_push($datapoints, $datapoint);
      }

      return $datapoints;
   }

}
      
?>
