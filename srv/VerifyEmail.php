<?php
   include_once 'globals.php';
   include_once 'emails.php';

   function reactivateEmail($emailAddress, $key)
   {
      $mysqli = connecti();

      $outputMessage = "";

      $query = sprintf("UPDATE Users SET is_active=1 WHERE email='%s' AND verifyEmailKey='%s'",
		      mysql_real_escape_string($emailAddress),
		      mysql_real_escape_string($key));

      if(!($result = $mysqli->query($query)))
      {
	 error_log('Failed to reactivate user');
	 $outputMessage .= "There was an error attempting to reactivate your account.";
      }
      else
      {
	 error_log('User reactivated successfully');
	 $outputMessage .= "Your account has been reactivated successfully.";
      }

      return $outputMessage;
   }

   function verifyEmail($emailAddress, $key)
   {
      $mysqli = connecti();

      $outputMessage = "";

      $query = sprintf("SELECT userID, email, firstName, emailVerified, accountActivated FROM Users WHERE email='%s' AND verifyEmailKey='%s'",
                  mysql_real_escape_string($emailAddress),
                  mysql_real_escape_string($key));
      if (!($result = $mysqli->query($query))) 
      { 
         error_log("failed to run query. ".$mysqli->error);
         return  null;
      }

      if ($row = $result->fetch_assoc())
      {
         /* the email/key pair has been matchedd */
         if ($row['emailVerified'])
         {
            $outputMessage .= "Your email address is verified and you are now ready to enter the world of dropoly. What are you waiting for? Click on the button below to enter.";

	    /* TODO Take this out as soon as new function has been implemented!!! */ 

	    $query = sprintf("UPDATE Users SET is_active=1 WHERE email='%s'",
			mysql_real_escape_string($emailAddress));

		if(!($mysqli->query($query)))
		{
			error_log('Failed to reactivate user');
		}
		else
		{
			error_log('Account reactivated');
		}
         }
         else
         {
            /* set the emailActivated flag to true */
            $query = sprintf("UPDATE Users SET emailVerified=1 WHERE email='%s' AND verifyEmailKey='%s'",
                        mysql_real_escape_string($emailAddress),
                        mysql_real_escape_string($key));
            if (!($mysqli->query($query)))
            {
               error_log('failed to run query. '.$mysqli->error);
               return null;
            }
            $outputMessage .= "Your email address is verified and you are now ready to enter the world of dropoly. What are you waiting for? Click on the button below to enter.";
         }
      }
      else
      {
         $outputMessage .= "We're sorry, there was an error in the request.";
      }
      
      return $outputMessage;
   }
?>
