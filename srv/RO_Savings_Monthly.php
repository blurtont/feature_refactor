<?php 
include_once 'globals.php';
include_once 'RO_Energy.php';

class RO_Savings_Monthly extends RO_Energy
{
   public $id_savings_monthly;
   public $id_residence;
   public $date;
   public $unix_time;
   public $type_fuel;
   public $kWh;
   public $BTU;

   const BTU_PER_KWH = 3412.12163;
   const KWH_PER_BTU = .00029307107;

   static function load($id_residence)
   {
      $mysqli = connecti();
      $query = sprintf("SELECT id_savings_monthly,
                               id_residence_Savings_Monthlies,
                               type_fuel_Savings_Monthlies,
                               unix_time
                        FROM Savings_Monthlies WHERE id_residence_Savings_Monthlies='%d'",
                       mysql_real_escape_string($id_residence));
      if (!($result = $mysqli->query($query)))
      {
         error_log("Failed to select montly savings. ".$mysqli->error);
         return null;
      }
      $ro_savings_monthlies = Array();

      while($a_row = $result->fetch_assoc())
      {
         $ro_savings = new RO_Savings_Monthly();
         $ro_savings->id_savings_monthly = (int) $a_row['id_savings_monthly'];
         $ro_savings->id_residence = (int) $a_row['id_residence_Savings_Monthlies'];
         $ro_savings->type_fuel = (int) $a_row['type_fuel_Savings_Monthlies'];
         $ro_savings->unix_time = $a_row[ 'unix_time' ];
         $ro_savings->kWh = (int) $a_row['kWh'];
         $ro_savings->BTU = BTU_PER_KWH * $a_row['kWh'];
         array_push($ro_savings_monthlies, $ro_savings);
      }
      return $ro_savings_monthlies;
   }

   static function update(Array $ro_savings_monthlies)
   {
      if(count($ro_savings_monthlies) > 0)
      {
         $mysqli = connecti();
         $query = sprintf("DELETE FROM Savings_Monthlies WHERE id_residence_Savings_Monthlies='%d'", 
                          mysql_real_escape_string($ro_savings_monthlies[0]->id_residence));
         if (!($mysqli->query($query)))
         {
            error_log("ERROR: failed to delete from Savings_Monthlies. ".$mysqli->error);
            return null;
         }
   
         foreach( $ro_savings_monthlies as $ro_savings )
         {   
            $query = sprintf( "INSERT INTO Savings_Monthlies (id_residence_Savings_Monthlies, 
                                                              type_fuel_Savings_Monthlies,
                                                              unix_time, 
                                                              kWh) 
                               VALUES ('%d', '%d', '%s', '%d')",
                              mysql_real_escape_string( $ro_savings->id_residence ),
                              mysql_real_escape_string( $ro_savings->type_fuel ),
                              mysql_real_escape_string( $ro_savings->unix_time ),
                              mysql_real_escape_string( $ro_savings->kWh ) );
            if (!$mysqli->query($query))
            {
               error_log("ERROR: failed to insert into Savings_Monthlies on update. ".$mysqli->error);
               return null;
            }
         }
      }
   }
}
?>
