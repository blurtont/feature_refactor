<?php
   include_once 'RO_Residence.php';

class RO_State_Global
{
   public $user; /* User.php */
   public $residence; /* RO_Residence.php */
   public $utility_bills;

   public $HVAC_baseline_first;
   public $HVAC_baseline_latest;
   public $latest_calculation_result;

   public $ro_leaderboard;

   public function __construct()
   {
      $this->residence = new RO_Residence;
   }
}
?>
