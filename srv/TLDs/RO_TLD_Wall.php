<?php
   include_once "RO_Device.php";

   class RO_TLD_Wall extends RO_Device
   {
      const name_table = "TLD_Walls";
      
      public $type_wall;
      public $type_wall_cavity_insulation;
      public $type_wall_sheet_insulation;
      public $fraction;

      public $action_free_no_insulation_model;
      public $action_upgrade_cavity_insulate_cellulose;
      public $action_upgrade_cavity_insulate_spray_foam;
      public $action_upgrade_cavity_insulate_fiberglass_batt;
      public $action_upgrade_sheet_one_inch;
      public $action_upgrade_sheet_two_inch;
      public $action_upgrade_sheet_three_inch;
      public $action_upgrade_sheet_four_inch;
      public $action_upgrade_sheet_five_inch;
      public $action_upgrade_sheet_six_inch;

      static function create_default($id_room)
      {
         return RO_TLD_Wall::load(Helper_RTLDs::create_device_and_actions($id_room, "TLD_Walls", 29));
      }

      static function update(RO_TLD_Wall $device)
      {
         $mysqli = connecti();
         RO_Action::update_actions($device->ro_actions);

         $query = sprintf("UPDATE TLD_Walls
                           SET id_room_TLD_Walls='%d',
                               id_residence_TLD_Walls='%d',
                               is_info_entered='%d',
                               type_wall='%d',
                               type_wall_cavity_insulation='%d',
                               type_wall_sheet_insulation='%d',
                               fraction_x100='%d'
                           WHERE id_device='%d'",
                           mysql_real_escape_string($device->id_room),
                           mysql_real_escape_string($device->id_residence),
                           mysql_real_escape_string($device->is_info_entered),
                           mysql_real_escape_string($device->type_wall),
                           mysql_real_escape_string($device->type_wall_cavity_insulation),
                           mysql_real_escape_string($device->type_wall_sheet_insulation),
                           mysql_real_escape_string($device->fraction*100),
                           mysql_real_escape_string($device->id_device));
         if (!($mysqli->query($query))) 
         {
            error_log("Failed to update RO_TLD_Wall where id=".$device->id_device." .".$mysqli->error);
            return null;
         }
      
      }
      
      static function load_all_in_room($id_room)
      {
         $mysqli = connecti();

         $new_devices = array();
         $query = sprintf("SELECT * FROM TLD_Walls WHERE id_room_TLD_Walls='%d'",
                     mysql_real_escape_string($id_room));
         if(!($result = $mysqli->query($query)))
         { 
            error_log("Failed to select Wall. ".$mysqli->error);
            return null;
         }
         while($a_row = $result->fetch_assoc())
         {
            array_push($new_devices, RO_TLD_Wall::copy_to_device($a_row));
         }
                                    
         return $new_devices;
      }
      
      static function load($id_device)
      {
         $query = sprintf("SELECT * FROM TLD_Walls WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if (!($result = $mysqli->query($query)))
         {
            error_log ("Failed to select Wall with id_device ".$id_device.". ".$mysqli->error);
            return null;
         }
         if($a_row = $result->fetch_assoc())
         {
            return RO_TLD_Wall::copy_to_devices($a_row);
         }
         else
         {
            error_log("There is no Wall with ID ".$id_device);
            return null;
         }
      }

      static function copy_to_device($a_row)
      {
         $new_device = new RO_TLD_Wall();
      
         $new_device->id_device = (int)$a_row['id_device'];
         $new_device->id_room = (int)$a_row['id_room_TLD_Walls'];
         $new_device->id_residence = (int)$a_row['id_residence_TLD_Walls'];
         $new_device->is_info_entered = (int) $a_row['is_info_entered'];

         $new_device->type_wall = (int)$a_row['type_wall'];
         $new_device->type_wall_cavity_insulation = (int)$a_row['type_wall_cavity_insulation'];
         $new_device->type_wall_sheet_insulation = (int)$a_row['type_wall_sheet_insulation'];
         $new_device->fraction = 0.01*$a_row['fraction_x100'];
                              
         $new_device->load_actions($a_row);

         $new_device->action_free_no_insulation_model = $new_device->ro_actions[0];
         $new_device->action_upgrade_cavity_insulate_cellulose = $new_device->ro_actions[1];
         $new_device->action_upgrade_cavity_insulate_spray_foam = $new_device->ro_actions[2];
         $new_device->action_upgrade_cavity_insulate_fiberglass_batt = $new_device->ro_actions[3];
         $new_device->action_upgrade_sheet_one_inch = $new_device->ro_actions[4];
         $new_device->action_upgrade_sheet_two_inch = $new_device->ro_actions[5];
         $new_device->action_upgrade_sheet_three_inch = $new_device->ro_actions[6];
         $new_device->action_upgrade_sheet_four_inch = $new_device->ro_actions[7];
         $new_device->action_upgrade_sheet_five_inch = $new_device->ro_actions[8];
         $new_device->action_upgrade_sheet_six_inch = $new_device->ro_actions[9];

         return $new_device;
      }
   }
?>
