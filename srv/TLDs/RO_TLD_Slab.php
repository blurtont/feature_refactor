<?php
   include_once "RO_Device.php";

   class RO_TLD_Slab extends RO_Device
   {
      const name_table = "TLD_Slabs";
      
      public $type_slab;
      public $fraction_complete;
      public $fraction_floor_area;

      public $action_free_insulation;
      public $action_upgrade_insulation_R2;
      public $action_upgrade_insulation_R7;
      public $action_upgrade_insulation_R13;
      public $action_upgrade_insulation_R19;

      static function create_default($id_room)
      {
         return RO_TLD_Slab::load(Helper_RTLDs::create_device_and_actions($id_room, "TLD_Slabs", 17));
      }

      static function update(RO_TLD_Slab $device)
      {
         $mysqli = connecti();
         RO_Action::update_actions($device->ro_actions);

         $query = sprintf("UPDATE TLD_Slabs
                           SET id_room_TLD_Slabs='%d',
                               id_residence_TLD_Slabs='%d',
                               is_info_entered='%d',
                               type_slab='%d',
                               fraction_complete_100x='%d',
                               fraction_floor_area_x100='%d'
                           WHERE id_device='%d'",
                           mysql_real_escape_string($device->id_room),
                           mysql_real_escape_string($device->id_residence),
                           mysql_real_escape_string($device->is_info_entered),
                           mysql_real_escape_string($device->type_slab),
                           mysql_real_escape_string($device->fraction_complete*100),
                           mysql_real_escape_string($device->fraction_floor_area*100),
                           mysql_real_escape_string($device->id_device));
         if (!$mysqli->query($query))
         {
            error_log("Failed to update RO_TLD_Slab with id=".$device.id_device." .".$mysqli->error);
            return null;
         }
      }

      static function load_all_in_room($id_room)
      {
         $mysqli = connecti();
         $new_devices = array();
         $query = sprintf("SELECT * FROM TLD_Slabs WHERE id_room_TLD_Slabs='%d'",
                     mysql_real_escape_string($id_room));
         if(!($result = $mysqli->query($query)))
         {
            error_log("Failed to select Slab. ".$mysqli->error);
            return null;
         }
         while($a_row = $result->fetch_assoc())
         {
            array_push($new_devices, RO_TLD_Slab::copy_to_devices);
         }
                                             
         return $new_devices;
      }

      static function load($id_device)
      {
         $query = sprintf("SELECT * FROM TLD_Slabs WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if (!($result = $mysqli->query($query)))
         {
            error_log ("Failed to select Slab with id_device ".$id_device.". ".$mysqli->error);
            return null;
         }
         if($a_row = $result->fetch_assoc())
         {
            return RO_TLD_Slab::copy_to_devices($a_row);
         }
         else
         {
            error_log("There is no Slab with ID ".$id_device);
            return null;
         }
      }

      static function copy_to_devices($a_row)
      {
         $new_device = new RO_TLD_Slab();
            
         $new_device->id_device = (int)$a_row['id_device'];
         $new_device->id_room = (int)$a_row['id_room_TLD_Slabs'];
         $new_device->id_residence = (int)$a_row['id_residence_TLD_Slabs'];
         $new_device->is_info_entered = (int) $a_row['is_info_entered'];

         $new_device->type_slab = (int)$a_row['type_house_seal'];
         $new_device->fraction_complete = 0.01*$a_row['fraction_complete_x100'];
         $new_device->fraction_floor_area = 0.01*$a_row['fraction_floor_area_x100'];
                                 
         $new_device->load_actions($a_row);

         $new_device->action_free_insulation = $new_device->ro_actions[0];
         $new_device->action_upgrade_insulation_R2 = $new_device->ro_actions[1];
         $new_device->action_upgrade_insulation_R7 = $new_device->ro_actions[2];
         $new_device->action_upgrade_insulation_R13 = $new_device->ro_actions[3];
         $new_device->action_upgrade_insulation_R19 = $new_device->ro_actions[4];
                                                                               
         return $new_device;
      }
   }
?>
