<?php
   include_once "RO_Device.php";
   
   class RO_TLD_Cooling_System extends RO_Device
   {
      const name_table = "TLD_Cooling_Systems";
      
      public $efficiency;
      public $fraction;
      public $type_ac;
      public $seer;

      static function create_default($id_room)
      {
         return RO_TLD_Cooling_System::load(Helper_RTLDs::create_device_and_actions($id_room, "TLD_Cooling_Systems", 1));
      }

      static function update(RO_TLD_Cooling_System $device)
      {
         $mysqli = connecti();
         /* update actions */
         RO_Action::update_actions($device->ro_actions);

         $query = sprintf("UPDATE TLD_Cooling_Systems
                           SET id_room_TLD_Cooling_Systems='%d',
                               id_residence_TLD_Cooling_Systems='%d',
                               is_info_entered='%d',
                               fraction_x100='%d',
                               type_ac='%d',
                               seer_x10='%d'
                           WHERE id_device='%d'",
                           mysql_real_escape_string($device->id_room),
                           mysql_real_escape_string($device->id_residence),
                           mysql_real_escape_string($device->is_info_entered),
                           mysql_real_escape_string($device->fraction*100),
                           mysql_real_escape_string($device->type_ac),
                           mysql_real_escape_string($device->seer*10),
                           mysql_real_escape_string($device->id_device));
         if (!($mysqli->query($query)))
         { 
            error_log ("Failed to update RO_TLD_Cooling_Systems with id=".$device->id_room." .".$mysqli->error);
            return null;
         }
      }
      static function load_all_in_room($id_room)
      {
         $mysqli = connecti();

         $new_devices = array();
         $query = sprintf("SELECT * FROM TLD_Cooling_Systems WHERE id_room_TLD_Cooling_Systems='%d'",
                     mysql_real_escape_string($id_room));
         if(!($result = $mysqli->query($query))) 
         {
            error_log("Failed to select . ".$mysqli->error);
            return null;
         }
         while($a_row = $result->fetch_assoc())
         {
            array_push($new_devices, RO_TLD_Cooling_System::copy_to_device($a_row));
         }                                                                           
         return $new_devices;
      }
                                                                                                                              
      static function load($id_device)
      {
         $mysqli = connecti();
         $query = sprintf("SELECT * FROM TLD_Cooling_Systems WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if (!($result = $mysqli->query($query)))
         {
            error_log ("Failed to select Cooling System with id_device ".$id_device.". ".$mysqli->error);
            return null;
         }
         
         if($a_row = $result->fetch_assoc())
         {
            return RO_TLD_Cooling_System::copy_to_device($a_row);
         }
         else
         {
            error_log("There is no Cooling System with ID ".$id_device);
            return null;
         }
      }
      
      static function copy_to_device($a_row)
      {
         $new_device = new RO_TLD_Cooling_System();
         
         $new_device->id_device = (int)$a_row['id_device'];
         $new_device->id_room = (int)$a_row['id_room_TLD_Cooling_Systems'];
         $new_device->id_residence = (int)$a_row['id_residence_TLD_Cooling_Systems'];
         $new_device->is_info_entered = (int) $a_row['is_info_entered'];
         $new_device->fraction = 0.01*$a_row['fraction_x100'];
         $new_device->type_ac = (int)$a_row['type_ac'];
         $new_device->seer = 0.1*$a_row['seer_x10'];

         $new_device->load_actions($a_row);
         return $new_device;
      }
   }
?>
