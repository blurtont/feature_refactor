<?php
   include_once "RO_Device.php";

   class RO_TLD_Water_Heater extends RO_Device
   {
      const name_table = "TLD_Water_Heaters";
      
      public $type_water_heater;
      public $type_water_heater_fuel;
      public $efficiency;
      public $fraction_house_served;
      public $temperature;

      public $action_free_temperature_average;
      public $action_free_temperature_highest;
      public $action_free_temperature_lower;
      public $action_upgrade_electric_standard;
      public $action_upgrade_electric_energy_star;
      public $action_upgrade_gas_standard;
      public $action_upgrade_gas_energy_star;
      public $action_upgrade_electric_heat_pump;
      public $action_upgrade_on_demand_electric;
      public $action_upgrade_on_demand_gas;
      public $action_upgrade_solar;

      static function create_default($id_room)
      {
         return RO_TLD_Water_Heater::load(Helper_RTLDs::create_device_and_actions($id_room, "TLD_Water_Heaters", 30));
      }

      static function update(RO_TLD_Water_Heater $device)
      {
         $mysqli = connecti();
         RO_Action::update_actions($device->ro_actions);

         $query = sprintf("UPDATE TLD_Water_Heaters
                           SET id_room_TLD_Water_Heaters='%d',
                               id_residence_TLD_Water_Heaters='%d',
                               is_info_entered='%d',
                               type_water_heater='%d',
                               type_water_heater_fuel='%d',
                               efficiency_x100='%d',
                               fraction_house_served_x100='%d',
                               temperature='%d'
                           WHERE id_device='%d'",
                           mysql_real_escape_string($device->id_room),
                           mysql_real_escape_string($device->id_residence),
                           mysql_real_escape_string($device->is_info_entered),
                           mysql_real_escape_string($device->type_water_heater),
                           mysql_real_escape_string($device->type_water_heater_fuel),
                           mysql_real_escape_string($device->efficiency*100),
                           mysql_real_escape_string($device->fraction_house_served*100),
                           mysql_real_escape_string($device->temperature),
                           mysql_real_escape_string($device->id_device));
         if (!($mysqli->query($query))) 
         { 
            error_log("Failed to update RO_TLD_Water_Heater with id=".$device->id_device." .".$mysqli->error);
            return null;
         }
      }

      static function load_all_in_room($id_room)
      {
         $mysqli = connecti();
         $new_devices = array();
         $query = sprintf("SELECT * FROM TLD_Water_Heaters WHERE id_room_TLD_Water_Heaters='%d'",
                     mysql_real_escape_string($id_room));
         if(!($result = $mysqli->query($query))) 
         { 
            error_log("Failed to select Water Heaters. ".$mysqli->error);
            return null;
         }
         while($a_row = $result->fetch_assoc())
         {
            array_push($new_devices, RO_TLD_Water_Heater::copy_to_device($a_row));
         }
                                            
         return $new_devices;
      }

      static function load($id_device)
      {
         $mysqli = connecti();
         $query = sprintf("SELECT * FROM TLD_Water_Heaters WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if (!($result = $mysqli->query($query)))
         {
            error_log ("Failed to select Water Heater with id_device ".$id_device.". ".$mysqli->error);
            return null;
         }
                                 
         if($a_row = $result->fetch_assoc())
         {
            return RO_TLD_Water_Heater::copy_to_device($a_row);
         }
         else
         {
            error_log("There is no Water Heater with ID ".$id_device);
            return null;
         }
      }

      static function copy_to_device($a_row)
      {
         $new_device = new RO_TLD_Water_Heater();
            
         $new_device->id_device = (int)$a_row['id_device'];
         $new_device->id_room = (int)$a_row['id_room_TLD_Water_Heaters'];
         $new_device->id_residence = (int)$a_row['id_residence_TLD_Water_Heaters'];
         $new_device->is_info_entered = (int) $a_row['is_info_entered'];

         $new_device->type_water_heater = (int)$a_row['type_water_heater'];
         $new_device->type_water_heater_fuel = (int)$a_row['type_water_heater_fuel'];
         $new_device->efficiency = 0.01*$a_row['efficiency_x100'];
         $new_device->fraction_house_served = 0.01*$a_row['fraction_house_served_x100'];
         $new_device->temperature = (int)$a_row['temperature'];

         $new_device->load_actions($a_row);

         $new_device->action_free_temperature_average = $new_device->ro_actions[0];
         $new_device->action_free_temperature_highest = $new_device->ro_actions[1];
         $new_device->action_free_temperature_lower = $new_device->ro_actions[2];
         $new_device->action_upgrade_electric_standard = $new_device->ro_actions[3];
         $new_device->action_upgrade_electric_energy_star = $new_device->ro_actions[4];
         $new_device->action_upgrade_gas_standard = $new_device->ro_actions[5];
         $new_device->action_upgrade_gas_energy_star = $new_device->ro_actions[6];
         $new_device->action_upgrade_electric_heat_pump = $new_device->ro_actions[7];
         $new_device->action_upgrade_on_demand_electric = $new_device->ro_actions[8];
         $new_device->action_upgrade_on_demand_gas = $new_device->ro_actions[9];
         $new_device->action_upgrade_solar = $new_device->ro_actions[10];

         return $new_device;
      }
   }
?>
