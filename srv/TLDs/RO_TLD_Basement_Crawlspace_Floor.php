<?php
   include_once "RO_Device.php";

   class RO_TLD_Basement_Crawlspace_Floor extends RO_Device
   {
      const name_table = "TLD_Basement_Crawlspace_Floors";
      
      public $type_basement_crawlspace_floor;
      public $height;
      public $R_floor;
      public $R_wall_exterior;
      public $R_wall_interior;
      public $fraction_floor_area;
      public $fraction_below_grade;
      public $fraction;

      public $action_free_no_insulation;
      public $action_upgrade_insulation_R11;
      public $action_upgrade_insulation_R13;
      public $action_upgrade_insulation_R15;
      public $action_upgrade_insulation_R19;
      public $action_upgrade_insulation_R21;
      public $action_upgrade_insulation_R25;
      public $action_upgrade_insulation_R30;
      public $action_upgrade_insulation_R38;

      static function create_default($id_room)
      {
         return RO_TLD_Basement_Crawlspace_Floor::load(Helper_RTLDs::create_device_and_actions($id_room, "TLD_Basement_Crawlspace_Floors", 2));
      }

      static function update(RO_TLD_Basement_Crawlspace_Floor $device)
      {
         $mysqli = connecti();
         /* update actions */
         RO_Action::update_actions($device->ro_actions);

         $query = sprintf("UPDATE TLD_Basement_Crawlspace_Floors 
                           SET id_room_TLD_Basement_Crawlspace_Floors='%d',
                               id_residence_TLD_Basement_Crawlspace_Floors='%d',
                               is_info_entered='%d',
                               type_basement_crawlspace_floor='%d',
                               height='%d',
                               R_floor='%d',
                               R_wall_exterior='%d',
                               R_wall_interior='%d',
                               fraction_floor_area_100x='%d'
                           WHERE id_device='%d'",
                           mysql_real_escape_string($device->id_room),
                           mysql_real_escape_string($device->id_residence),
                           mysql_real_escape_string($device->is_info_entered),
                           mysql_real_escape_string($device->type_basement_crawlspace_floor),
                           mysql_real_escape_string($device->height),
                           mysql_real_escape_string($device->R_floor),
                           mysql_real_escape_string($device->R_wall_exterior),
                           mysql_real_escape_string($device->R_wall_interior),
                           mysql_real_escape_string($device->fraction_floor_area*100),
                           mysql_real_escape_string($device->id_device));
         if (!($mysqli->query($query))) 
         { 
            error_log ("Failed to update RO_TLD_Basement_Crawlspace_Floor with id_device=".$device->id_device." .".$mysqli->error);
            return null;
         }
      
      }

      function load_all_in_room($id_room)
      {
         $mysqli = connecti();

         $new_devices = array();

         $query = sprintf("SELECT * FROM TLD_Basement_Crawlspace_Floors WHERE id_room_TLD_Basement_Crawlspace_Floors='%d'",
                     mysql_real_escape_string($id_room));
         if(!($result = $mysqli->query($query))) 
         { 
            error_log("Failed to select Basement/Crawlspace Floors. ".$mysqli->error);
            return null;
         }
         while ($a_row = $result->fetch_assoc())
         {
            array_push($new_devices, RO_TLD_Basement_Crawlspace_Floor::copy_to_device($a_row));
         }
         return $new_devices;
      }

      function load($id_device)
      {
         $query = sprintf("SELECT * FROM TLD_Basement_Crawlspace_Floors WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if(!($result = $mysqli->query($query))) 
         { 
            error_log("Failed to select Basement/Crawlspace Floor. ".$mysqli->error);
            return null;
         }
         if ($a_row = $result->fetch_assoc())
         {
            return RO_TLD_Basement_Crawlspace_Floor::copy_to_device($a_row);
         }
         else
         {
            error_log("There is no Basement/Crawlspace Floor ".$id_device);
            return null;
         }
      }
   
      function copy_to_device($a_row)
      {
         $new_device = new RO_TLD_Basement_Crawlspace_Floor();

         $new_device->id_device = (int) $a_row['id_device'];
         $new_device->id_room = (int) $a_row['id_room_TLD_Basement_Crawlspace_Floors'];
         $new_device->id_residence = (int) $a_row['id_residence_TLD_Basement_Crawlspace_Floors'];
         $new_device->is_info_entered = (int) $a_row['is_info_entered'];

         $new_device->type_basement_crawlspace_floor = (int) $a_row['type_basement_crawlspace_floor'];
         $new_device->height = (int) $a_row['height'];
         $new_device->R_floor = (int) $a_row['R_floor'];
         $new_device->R_wall_exterior = (int) $a_row['R_wall_exterior'];
         $new_device->R_wall_interior = (int) $a_row['R_wall_interior'];
         $new_device->fraction_floor_area = 0.01 * $a_row['fraction_floor_area_100x'];
         $new_device->fraction_below_grade = 0.01 * $a_row['fraction_below_grade_100x'];
         $new_device->fraction = 0.01 * $a_row['fraction_100x'];

         $new_device->load_actions($a_row);

         $new_device->action_free_no_insulation = $new_device->ro_actions[0];
         $new_device->action_upgrade_insulation_R11 = $new_device->ro_actions[1];
         $new_device->action_upgrade_insulation_R13 = $new_device->ro_actions[2];
         $new_device->action_upgrade_insulation_R15 = $new_device->ro_actions[3];
         $new_device->action_upgrade_insulation_R19 = $new_device->ro_actions[4];
         $new_device->action_upgrade_insulation_R21 = $new_device->ro_actions[5];
         $new_device->action_upgrade_insulation_R25 = $new_device->ro_actions[6];
         $new_device->action_upgrade_insulation_R30 = $new_device->ro_actions[7];
         $new_device->action_upgrade_insulation_R38 = $new_device->ro_actions[8];

         return $new_device;
      }
   }
?>
