<?php
   include_once "RO_Device.php";

   class RO_TLD_Ductwork extends RO_Device
   {
      const name_table = "TLD_Ductworks";
      
      public $is_unconditioned_space;
      public $is_seal_excellent;
      public $is_insulated;
      public $type_ductwork;

      public $action_upgrade_seal_excellent;
      public $action_upgrade_seal_poor;
      public $action_upgrade_insulate;
      public $action_upgrade_insulate_none;

      static function create_default($id_room)
      {
         return RO_TLD_Ductwork::load(Helper_RTLDs::create_device_and_actions($id_room, "TLD_Ductworks", 15));
      }

      static function update(RO_TLD_Ductwork $device)
      {
         $mysqli = connecti();

         RO_Action::update_actions($device->ro_actions);

         $query = sprintf("UPDATE TLD_Ductworks
                           SET id_room_TLD_Ductworks='%d',
                               id_residence_TLD_Ductworks='%d',
                               is_info_entered='%d',
                               is_unconditioned_space='%d',
                               is_seal_excellent='%d',
                               is_insulated='%d',
                               type_ductwork='%d'
                           WHERE id_device='%d'",
                           mysql_real_escape_string($device->id_room),
                           mysql_real_escape_string($device->id_residence),
                           mysql_real_escape_string($device->is_info_entered),
                           mysql_real_escape_string($device->is_unconditioned_space),
                           mysql_real_escape_string($device->is_seal_excellent),
                           mysql_real_escape_string($device->is_insulated),
                           mysql_real_escape_string($device->type_ductwork),
                           mysql_real_escape_string($device->id_device));
         if (!($mysqli->query($query)))
         { 
            error_log("Failed to update TLD_Ductworks with id=".$device->id_device." .".$mysqli->error);
            return null;
         }
      }

      static function load_all_in_room($id_room)
      {
         $mysqli = connecti();
         $new_devices = array();
         $query = sprintf("SELECT * FROM TLD_Ductworks WHERE id_room_TLD_Ductworks='%d'",
                     mysql_real_escape_string($id_room));
         if(!($result = $mysqli->query($query)))
         {
            error_log("Failed to select Ductwork. ".$mysqli->error);
            return null;
         }
         while($a_row = $result->fetch_assoc())
         {
            array_push($new_devices, RO_TLD_Ductwork::copy_to_devices($a_row));
         }
                                       
         return $new_devices;
      }

      static function load($id_device)
      {
         $mysqli = connecti();
         $query = sprintf("SELECT * FROM TLD_Ductworks WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if (!($result = $mysqli->query($query)))
         {
            error_log ("Failed to select Ductwork with id_device ".$id_device.". ".$mysqli->error);
            return null;
         }
                                 
         if($a_row = $result->fetch_assoc())
         {
            return RO_TLD_Ductwork::copy_to_devices($a_row);
         }
         else
         {
            error_log("There is no Ductwork with ID ".$id_device);
            return null;
         }
      }

      static function copy_to_devices($a_row)
      {
         $new_device = new RO_TLD_Ductwork();
            
         $new_device->id_device = (int)$a_row['id_device'];
         $new_device->id_room = (int)$a_row['id_room_TLD_Ductworks'];
         $new_device->id_residence = (int)$a_row['id_residence_TLD_Ductworks'];
         $new_device->is_info_entered = (int) $a_row['is_info_entered'];

         $new_device->is_unconditioned_space = (int)$a_row['is_unconditioned_space'];
         $new_device->is_seal_excellent = (int)$a_row['is_seal_excellent'];
         $new_device->is_insulated = (int)$a_row['is_insulated'];
         $new_device->type_ductwork = (int)$a_row['type_ductwork'];
                                    
         $new_device->load_actions($a_row);
         $new_device->action_upgrade_seal_excellent = $new_device->ro_actions[0];
         $new_device->action_upgrade_seal_poor = $new_device->ro_actions[1];
         $new_device->action_upgrade_insulate = $new_device->ro_actions[2];
         $new_device->action_upgrade_insulate_none = $new_device->ro_actions[3];

         return $new_device;
      }
   }
?>
