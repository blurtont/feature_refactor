<?php
   include_once "RO_Device.php";

   class RO_TLD_House_Seal extends RO_Device
   {
      const name_table = "TLD_House_Seals";
      
      public $type_house_seal;
      public $fraction;
      public $fraction_complete;

      public $action_upgrade_to_very_leaky;
      public $action_upgrade_to_leaky;
      public $action_upgrade_to_slightly_leaky;
      public $action_upgrade_to_air_tight;

      static function create_default($id_room)
      {
         return RO_TLD_House_Seal::load(Helper_RTLDs::create_device_and_actions($id_room, "TLD_House_Seals", 21));
      }

      static function update(RO_TLD_House_Seal $device)
      {
         $mysqli = connecti();
         RO_Action::update_actions($device->ro_actions);

         $query = sprintf("UPDATE TLD_House_Seals
                           SET id_room_TLD_House_Seals='%d',
                               id_residence_TLD_House_Seals='%d',
                               is_info_entered='%d',
                               type_house_seal='%d',
                               fraction_x100='%d',
                               fraction_complete_x100='%d'
                           WHERE id_device='%d'",
                           mysql_real_escape_string($device->id_room),
                           mysql_real_escape_string($device->id_residence),
                           mysql_real_escape_string($device->is_info_entered),
                           mysql_real_escape_string($device->type_house_seal),
                           mysql_real_escape_string($device->fraction*100),
                           mysql_real_escape_string($device->fraction_complete*100),
                           mysql_real_escape_string($device->id_device));
         if (!($mysqli->query($query))) 
         { 
            error_log ("Failed to update RO_TLD_House_Seal with id=".$device->id_device." .".$mysqli->error);
            return null;
         }
      }

      static function load_all_in_room($id_room)
      {
         $mysqli = connecti();
         $new_devices = array();
         $query = sprintf("SELECT * FROM TLD_House_Seals WHERE id_room_TLD_House_Seals='%d'",
                     mysql_real_escape_string($id_room));
         if(!($result = $mysqli->query($query))) 
         { 
            error_log("Failed to select House Seal. ".$mysqli->error);
            return null;
         }
         while($a_row = $result->fetch_assoc())
         {
            array_push($new_devices, RO_TLD_House_Seal::copy_to_devices($a_row));
         }
                                      
         return $new_devices;
      }

      static function load($id_device)
      {
         $mysqli = connecti();
         $query = sprintf("SELECT * FROM TLD_House_Seals WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if (!($result = $mysqli->query($query)))
         {
            error_log ("Failed to select House Seal with id_device ".$id_device.". ".$mysqli->error);
            return null;
         }
                                 
         if($a_row = $result->fetch_assoc())
         {
            return RO_TLD_House_Seal::copy_to_devices($a_row);
         }
         else
         {
            error_log("There is no House Seal with ID ".$id_device);
            return null;
         }
      }

      static function copy_to_devices($a_row)
      {
         $new_device = new RO_TLD_House_Seal();
            
         $new_device->id_device = (int)$a_row['id_device'];
         $new_device->id_room = (int)$a_row['id_room_TLD_House_Seals'];
         $new_device->id_residence = (int)$a_row['id_residence_TLD_House_Seals'];
         $new_device->is_info_entered = (int) $a_row['is_info_entered'];

         $new_device->type_house_seal = (int)$a_row['type_house_seal'];
         $new_device->fraction = 0.01*$a_row['fraction_x100'];
         $new_device->fraction_complete = 0.01*$a_row['fraction_complete_x100'];
                                 
         $new_device->load_actions($a_row);
         
         $new_device->action_upgrade_to_very_leaky = $new_device->ro_actions[0];
         $new_device->action_upgrade_to_leaky = $new_device->ro_actions[1];
         $new_device->action_upgrade_to_slightly_leaky = $new_device->ro_actions[2];
         $new_device->action_upgrade_to_air_tight = $new_device->ro_actions[3];
                                                                               
         return $new_device;
      }
   }
?>
