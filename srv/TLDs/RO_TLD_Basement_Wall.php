<?php
   include_once "RO_Device.php";

   class RO_TLD_Basement_Wall extends RO_Device
   {
      const name_table = "TLD_Basement_Walls";
      
      public $height;
      public $fraction_below_grade;
      public $fraction;
      public $R_wall_insulation_interior;
      public $R_wall_insulation_exterior;
      public $type_basement_wall;

      public $action_free_insulation_none;
      public $action_upgrade_insulation_R11;
      public $action_upgrade_insulation_R19;

      static function create_default($id_room)
      {
         return RO_TLD_Basement_Wall::load(Helper_RTLDs::create_device_and_actions($id_room, "TLD_Basement_Walls", 4));
      }

      static function update(RO_TLD_Basement_Wall $device)
      {
         $mysqli = connecti();

         /* update action */
         RO_Action::update_actions($device->ro_actions);

         $query = sprintf("UPDATE TLD_Basement_Walls
                           SET id_room_TLD_Basement_Walls='%d',
                               id_residence_TLD_Basement_Walls='%d',
                               is_info_entered='%d',
                               height='%d',
                               fraction_below_grade_100x='%d',
                               fraction_100x='%d',
                               R_wall_insulation_interior='%d',
                               R_wall_insulation_exterior='%d',
                               type_basement_wall='%d'
                           WHERE id_device='%d'",
                           mysql_real_escape_string($device->id_room),
                           mysql_real_escape_string($device->id_residence),
                           mysql_real_escape_string($device->is_info_entered),
                           mysql_real_escape_string($device->height),
                           mysql_real_escape_string($device->fraction_below_grade*100),
                           mysql_real_escape_string($device->fraction*100),
                           mysql_real_escape_string($device->R_wall_insulation_interior),
                           mysql_real_escape_string($device->R_wall_insulation_exterior),
                           mysql_real_escape_string($device->type_basement_wall),
                           mysql_real_escape_string($device->id_device));

         if (!($mysqli->query($query)))
         { 
            error_log("Failed to update RO_TLD_Basement_Wall with id=".$device->id_device." .".$mysqli->error);
            return null;
         }
      }

      static function load_all_in_room($id_room)
      {
         $mysqli = connecti();
         $new_devices = array();
         $query = sprintf("SELECT * FROM TLD_Basement_Walls WHERE id_room_TLD_Basement_Walls='%d'",
                     mysql_real_escape_string($id_room));
         if(!($result = $mysqli->query($query)))
         { 
            error_log("Failed to select Basement Walls. ".$mysqli->error);
            return null;
         }
         while($a_row = $result->fetch_assoc())
         {
            array_push($new_devices, RO_TLD_Basement_Wall::copy_to_device($a_row));
         }
         return $new_devices;
      }

      static function load($id_device)
      {
         $mysqli = connecti();
         $query = sprintf("SELECT * FROM TLD_Basement_Walls WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if(!($result = $mysqli->query($query))) 
         {
            error_log("Failed to select Basement Wall with id_device ".$id_device." .".$mysqli->error);
            return null;
         }
         if ($a_row = $result->fetch_assoc())
         {
            return RO_TLD_Basement_Wall::copy_to_device($a_row);
         }
         else
         {
            error_log ("There is no Basement Wall with ID ".$id_device);
            return null;
         }
      }

      static function copy_to_device($a_row)
      {
         $new_device = new RO_TLD_Basement_Wall();

         $new_device->id_device = (int)$a_row['id_device'];
         $new_device->id_room = (int)$a_row['id_room_TLD_Basement_Walls'];
         $new_device->id_residence = (int)$a_row['id_residence_TLD_Basement_Walls'];
         $new_device->is_info_entered = (int) $a_row['is_info_entered'];
         $new_device->is_owner_only = true;

         $new_device->height = (int)$a_row['height'];
         $new_device->fraction_below_grade = 0.01*$a_row['fraction_below_grade_100x'];
         $new_device->fraction = 0.01*$a_row['fraction_100x'];
         $new_device->R_wall_insulation_interior = (int)$a_row['R_wall_insulation_interior'];
         $new_device->R_wall_insulation_exterior = (int)$a_row['R_wall_insulation_exterior'];
         $new_device->type_basement_wall = (int)$a_row['type_basement_wall'];

         $new_device->load_actions($a_row);

         $new_device->action_free_insulation_none = $new_device->ro_actions[0];
         $new_device->action_upgrade_insulation_R11 = $new_device->ro_actions[1];
         $new_device->action_upgrade_insulation_R19 = $new_device->ro_actions[2];

         return $new_device;
      }
   }
?>
