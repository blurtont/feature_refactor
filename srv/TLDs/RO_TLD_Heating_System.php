<?php
   include_once "RO_Device.php";

   class RO_TLD_Heating_System extends RO_Device
   {
      const name_table = "TLD_Heating_Systems";
      
      public $type_heat_system;
      public $type_heat_fuel;
      public $efficiency;
      public $fraction_house_served;

      /* reorder */
      public $action_upgrade_boiler_60;
      public $action_upgrade_boiler_70;
      public $action_upgrade_boiler_78;
      public $action_upgrade_boiler_80;
      public $action_upgrade_boiler_85;
      public $action_upgrade_boiler_87;
      public $action_upgrade_boiler_91;
      public $action_upgrade_boiler_94;

      public $action_upgrade_furnace_60;
      public $action_upgrade_furnace_70;
      public $action_upgrade_furnace_78;
      public $action_upgrade_furnace_80;
      public $action_upgrade_furnace_85;
      public $action_upgrade_furnace_90;
      public $action_upgrade_furnace_92;
      public $action_upgrade_furnace_94;
      public $action_upgrade_furnace_96;

      public $action_upgrade_heating_heat_pump_air_HSPF_7;
      public $action_upgrade_heating_heat_pump_air_HSPF_8;
      public $action_upgrade_heating_heat_pump_air_HSPF_9;
      public $action_upgrade_heating_heat_pump_air_HSPF_9pt2;
      public $action_upgrade_heating_heat_pump_ground_HSPF_8;
      public $action_upgrade_heating_heat_pump_ground_HSPF_11;
      public $action_upgrade_heating_heat_pump_ground_HSPF_13;

      static function create_default($id_room)
      {
         return RO_TLD_Heating_System::load(Helper_RTLDs::create_device_and_actions($id_room, "TLD_Heating_Systems", 18));
      }

      static function update(RO_TLD_Heating_System $device)
      {
         $mysqli = connecti();
         RO_Action::update_actions($device->ro_actions);

         $query = sprintf("UPDATE TLD_Heating_Systems
                           SET id_room_TLD_Heating_Systems='%d',
                               id_residence_TLD_Heating_Systems='%d',
                               is_info_entered='%d',
                               type_heat_system='%d',
                               type_heat_fuel='%d',
                               efficiency_100x='%d',
                               fraction_house_served_100x='%d'
                           WHERE id_device='%d'",
                           mysql_real_escape_string($device->id_room),
                           mysql_real_escape_string($device->id_residence),
                           mysql_real_escape_string($device->is_info_entered),                           
                           mysql_real_escape_string($device->type_heat_system),
                           mysql_real_escape_string($device->type_heat_fuel),
                           mysql_real_escape_string($device->efficiency*100),
                           mysql_real_escape_string($device->fraction_house_served*100),
                           mysql_real_escape_string($device->id_device));
         if (!($mysqli->query($query)))
         {
            error_log ("Failed to update TLD_Heating_Systems with id=".$device->id_device." .".$mysqli->error);
            return null;
         }
      
      }
      static function load_all_in_room($id_room)
      {
         $mysqli = connecti();
         $new_devices = array();
         $query = sprintf("SELECT * FROM TLD_Heating_Systems WHERE id_room_TLD_Heating_Systems='%d'",
                     mysql_real_escape_string($id_room));
         if(!($result = $mysqli->query($query)))
         { 
            error_log("Failed to select Heating System. ".$mysqli->error);
            return null;
         }

         while($a_row = $result->fetch_assoc())
         {
            array_push($new_devices, RO_TLD_Heating_System::copy_to_device($a_row));
         }
                                            
         return $new_devices;
      }

      static function load($id_device)
      {
         $mysqli = connecti();
         $query = sprintf("SELECT * FROM TLD_Heating_Systems WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if (!($result = $mysqli->query($query)))
         {
            error_log ("Failed to select Heating System with id_device ".$id_device.". ".$mysqli->error);
            return null;
         }
                              
         if($a_row = $result->fetch_assoc())
         {
            return RO_TLD_Heating_System::copy_to_device($a_row);
         }
         else
         {
            error_log("There is no Heating System with ID ".$id_device);
            return null;
         }                                                            
      } 
      
      static function copy_to_device($a_row)
      {
         $new_device = new RO_TLD_Heating_System();
            
         $new_device->id_device = (int)$a_row['id_device'];
         $new_device->id_room = (int)$a_row['id_room_TLD_Heating_Systems'];
         $new_device->id_residence = (int)$a_row['id_residence_TLD_Heating_Systems'];
         $new_device->is_info_entered = (int) $a_row['is_info_entered'];

         $new_device->type_heat_system = (int)$a_row['type_heat_system'];
         $new_device->type_heat_fuel = (int)$a_row['type_heat_fuel'];
         $new_device->efficiency = 0.01*$a_row['efficiency_100x'];
         $new_device->fraction_house_served = 0.01*$a_row['fraction_house_served_100x'];
                                 
         $new_device->load_actions($a_row);

         $new_device->action_free_no_insulation_model = $new_device->ro_actions[0];
         $new_device->action_upgrade_boiler_60 = $new_device->ro_actions[1];
         $new_device->action_upgrade_boiler_70 = $new_device->ro_actions[2];
         $new_device->action_upgrade_boiler_78 = $new_device->ro_actions[3];
         $new_device->action_upgrade_boiler_80 = $new_device->ro_actions[4];
         $new_device->action_upgrade_boiler_85 = $new_device->ro_actions[5];
         $new_device->action_upgrade_boiler_87 = $new_device->ro_actions[6];
         $new_device->action_upgrade_boiler_91 = $new_device->ro_actions[7];
         $new_device->action_upgrade_boiler_94 = $new_device->ro_actions[8];
         $new_device->action_upgrade_furnace_60 = $new_device->ro_actions[9];
         $new_device->action_upgrade_furnace_70 = $new_device->ro_actions[10];
         $new_device->action_upgrade_furnace_78 = $new_device->ro_actions[11];
         $new_device->action_upgrade_furnace_80 = $new_device->ro_actions[12];
         $new_device->action_upgrade_furnace_85 = $new_device->ro_actions[13];
         $new_device->action_upgrade_furnace_90 = $new_device->ro_actions[14];
         $new_device->action_upgrade_furnace_92 = $new_device->ro_actions[15];
         $new_device->action_upgrade_furnace_94 = $new_device->ro_actions[16];
         $new_device->action_upgrade_furnace_96 = $new_device->ro_actions[17];
         $new_device->action_upgrade_heating_heat_pump_air_HSPF_7 = $new_device->ro_actions[18];
         $new_device->action_upgrade_heating_heat_pump_air_HSPF_8 = $new_device->ro_actions[19];
         $new_device->action_upgrade_heating_heat_pump_air_HSPF_9 = $new_device->ro_actions[20];
         $new_device->action_upgrade_heating_heat_pump_air_HSPF_9pt2 = $new_device->ro_actions[21];
         $new_device->action_upgrade_heating_heat_pump_ground_HSPF_8 = $new_device->ro_actions[22];
         $new_device->action_upgrade_heating_heat_pump_ground_HSPF_11 = $new_device->ro_actions[23];
         $new_device->action_upgrade_heating_heat_pump_ground_HSPF_13 = $new_device->ro_actions[24];

         return $new_device;
      }
   }
?>
