<?php
include_once 'RO_TLD_Basement_Crawlspace_Floor.php';
include_once 'RO_TLD_Basement_Wall.php';
include_once 'RO_TLD_Ceiling.php';
include_once 'RO_TLD_Cooling_System.php';
include_once 'RO_TLD_Ductwork.php';
include_once 'RO_TLD_Heating_System.php';
include_once 'RO_TLD_House_Seal.php';
include_once 'RO_TLD_Slab.php';
include_once 'RO_TLD_Thermostat.php';
include_once 'RO_TLD_Wall.php';
include_once 'RO_TLD_Water_Heater.php';

class RO_TLDs
{
   public $tld_basement_crawlspace_floors;
   public $tld_basement_walls;
   public $tld_ceilings;
   public $tld_cooling_systems;
   public $tld_ductworks;
   public $tld_heating_systems;
   public $tld_house_seals;
   public $tld_slabs;
   public $tld_thermostats;
   public $tld_walls;
   public $tld_water_heaters;
   
   static function load_tlds($id_room)
   {
      $new_tlds = new RO_TLDs();

      $new_tlds->tld_basement_crawlspace_floors = RO_TLD_Basement_Crawlspace_Floor::load_all_in_room($id_room);
      $new_tlds->tld_basement_walls = RO_TLD_Basement_Wall::load_all_in_room($id_room);
      $new_tlds->tld_ceilings = RO_TLD_Ceiling::load_all_in_room($id_room);
      $new_tlds->tld_cooling_systems = RO_TLD_Cooling_System::load_all_in_room($id_room);
      $new_tlds->tld_ductworks = RO_TLD_Ductwork::load_all_in_room($id_room);
      $new_tlds->tld_heating_systems = RO_TLD_Heating_System::load_all_in_room($id_room);
      $new_tlds->tld_house_seals = RO_TLD_House_Seal::load_all_in_room($id_room);
      $new_tlds->tld_slabs = RO_TLD_Slab::load_all_in_room($id_room);
      $new_tlds->tld_thermostats = RO_TLD_Thermostat::load_all_in_room($id_room);
      $new_tlds->tld_walls = RO_TLD_Wall::load_all_in_room($id_room);
      $new_tlds->tld_water_heaters = RO_TLD_Water_Heater::load_all_in_room($id_room);

      return $new_tlds;
   }

   public function get_devices()
   {
      $devices = array_merge($this->tld_basement_crawlspace_floors,
                       $this->tld_basement_walls,
                       $this->tld_ceilings,
                       $this->tld_cooling_systems,
                       $this->tld_ductworks,
                       $this->tld_heating_systems,
                       $this->tld_house_seals,
                       $this->tld_slabs,
                       $this->tld_thermostats,
                       $this->tld_walls,
                       $this->tld_water_heaters);
      return $devices;
   }

   public function duplicate($id_room_dup)
   {
      $devices = $this->get_devices();
      foreach($devices as $device)
      {
         $device->duplicate($id_room_dup);
      }
   }

   static function update($ro_tlds)
   {
      $devices = $ro_tlds->get_devices();
      foreach($devices as $device)
      {
	 if($device->invalidated)
	 {
             $device::update($device);
	 }
      }
   }

   public function remove()
   {
      $devices = $this->get_devices();
      foreach($devices as $device)
      {
         $device->remove();
      }
   }
}
?>
