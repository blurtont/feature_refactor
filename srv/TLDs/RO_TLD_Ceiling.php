<?php
   include_once "RO_Device.php";

   class RO_TLD_Ceiling extends RO_Device
   {
      const name_table = "TLD_Ceilings";
      
      public $type_ceiling_insulation;
      public $area;
      public $fraction;

      public $action_upgrade_insulate_0_inch;
      public $action_upgrade_insulate_03_inch;
      public $action_upgrade_insulate_06_inch;
      public $action_upgrade_insulate_09_inch;
      public $action_upgrade_insulate_12_inch;
      public $action_upgrade_insulate_16_inch;

      static function create_default($id_room)
      {
         return RO_TLD_Ceiling::load(Helper_RTLDs::create_device_and_actions($id_room, "TLD_Ceilings", 7));
      }

      static function update(RO_TLD_Ceiling $device)
      {
         $mysqli = connecti();
         /* update actions */
         RO_Action::update_actions($device->ro_actions);

         $query = sprintf("UPDATE TLD_Ceilings
                           SET id_room_TLD_Ceilings='%d',
                               id_residence_TLD_Ceilings='%d',
                               is_info_entered='%d',
                               type_ceiling='%d',
                               type_ceiling_insulation='%d',
                               area='%d',
                               fraction_100x='%d'
                           WHERE id_device='%d'",
                           mysql_real_escape_string($device->id_room),
                           mysql_real_escape_string($device->id_residence),
                           mysql_real_escape_string($device->is_info_entered),
                           mysql_real_escape_string($device->type_ceiling),
                           mysql_real_escape_string($device->type_ceiling_insulation),
                           mysql_real_escape_string($device->area),
                           mysql_real_escape_string($device->fraction*100),
                           mysql_real_escape_string($device->id_device));
         if (!($mysqli->query($query)))
         { 
            error_log("Failed to update RO_TLD_Ceiling with id=".$device->id_device." .".$mysqli->error);
            return null;
         }
      }

      static function load_all_in_room($id_room)
      {
         $mysqli = connecti();

         $new_devices = array();
         $query = sprintf("SELECT * FROM TLD_Ceilings WHERE id_room_TLD_Ceilings='%d'",
                     mysql_real_escape_string($id_room));
         if (!($result = $mysqli->query($query)))
         { 
            error_log("Failed to select Ceiling. ".$mysqli->error);
            return null;
         }
         while($a_row = $result->fetch_assoc())
         {
            array_push($new_devices, RO_TLD_Ceiling::copy_to_device($a_row));
         }
                                         
         return $new_devices;
      }

      static function load($id_device)
      {
         $mysqli = connecti();

         $query = sprintf("SELECT * FROM TLD_Ceilings WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if (!($result = $mysqli->query($query)))
         {
            error_log ("Failed to select Ceiling with id_device ".$id_device.". ".$mysqli->error);
            return null;
         }
                        
         if($a_row = $result->fetch_assoc())
         {
            return RO_TLD_Ceiling::copy_to_device($a_row);
         }
         else
         {
            error_log("There is no Ceiling ".$id_device);
            return null;
         }
      }

      static function copy_to_device($a_row)
      {
         $new_device = new RO_TLD_Ceiling();

         $new_device->id_device = (int)$a_row['id_device']; 
         $new_device->id_room = (int)$a_row['id_room_TLD_Ceilings'];
         $new_device->id_residence = (int)$a_row['id_residence_TLD_Ceilings'];
         $new_device->is_info_entered = (int)$a_row['is_info_entered'];
         $new_device->is_owner_only = true;

         $new_device->type_ceiling = (int)$a_row['type_ceiling'];
         $new_device->type_ceiling_insulation = (int)$a_row['type_ceiling_insulation'];
         $new_device->area = (int)$a_row['area'];
         $new_device->fraction = 0.01*$a_row['fraction_100x'];

         $new_device->load_actions($a_row);

         $new_device->action_upgrade_insulate_0_inch = $new_device->ro_actions[0];
         $new_device->action_upgrade_insulate_03_inch = $new_device->ro_actions[1];
         $new_device->action_upgrade_insulate_06_inch = $new_device->ro_actions[2];
         $new_device->action_upgrade_insulate_09_inch = $new_device->ro_actions[3];
         $new_device->action_upgrade_insulate_12_inch = $new_device->ro_actions[4];
         $new_device->action_upgrade_insulate_16_inch = $new_device->ro_actions[5];
         
         return $new_device;
      }
   }
?>
