<?php
   include_once "RO_Device.php";

   class RO_TLD_Thermostat extends RO_Device
   {
      const name_table = "TLD_Thermostats";
      
      public $fraction_residence_controlled;
      public $temp_winter_setpoint_current;
      public $temp_winter_night_setback_current;
      public $temp_winter_day_setback_current;
      public $temp_summer_setpoint_current;
      public $temp_summer_night_setforward_current;
      public $temp_summer_day_setforward_current;
      public $hours_per_day_nonoccupied_weekday_winter;
      public $hours_per_day_nonoccupied_weekend_winter;
      public $hours_per_day_nonoccupied_weekday_summer;
      public $hours_per_day_nonoccupied_weekend_summer;
      public $days_residence_vacant_summer;
      public $days_residence_vacant_winter;
      public $setback_temperature_vacant_residence_winter;
      public $ac_turned_off_vacant_residence_summer;
      public $owns_thermostat;

      public $action_free_winter_night_setback_best;
      public $action_free_summer_night_setforward_best;
      public $action_free_winter_night_setback_typical;
      public $action_free_summer_night_setforward_typical;
      public $action_free_winter_night_setback_worst;
      public $action_free_summer_night_setforwrd_worst;
      public $action_free_use_winter_day_setback_best;
      public $action_free_use_summer_day_setforward_best;
      public $action_free_use_winter_day_setback_typical;
      public $action_free_use_summer_day_setforward_typical;
      public $action_free_use_winter_day_setback_worst;
      public $action_free_use_summer_day_setforward_worst;
      public $action_free_use_winter_setpoint_best;
      public $action_free_use_summer_setpoint_best;
      public $action_free_use_winter_setpoint_typical;
      public $action_free_use_summer_setpoint_typical;
      public $action_free_use_winter_setpoint_worst;
      public $action_free_use_summer_setpoint_worst;
      public $action_free_use_vacant_winter_setback;
      public $action_free_use_vacant_summer_turn_off;

      static function create_default($id_room)
      {
         return RO_TLD_Thermostat::load(Helper_RTLDs::create_device_and_actions($id_room, "TLD_Thermostats", 27));
      }

      static function update(RO_TLD_Thermostat $device)
      {
         $mysqli = connecti();
         RO_Action::update_actions($device->ro_actions);

         $query = sprintf("UPDATE TLD_Thermostats
                           SET id_room_TLD_Thermostats='%d',
                               id_residence_TLD_Thermostats='%d',
                               is_info_entered='%d',
                               fraction_residence_controlled_x100='%d',
                               temp_winter_setpoint_current='%d',
                               temp_winter_night_setback_current='%d',
                               temp_winter_day_setback_current='%d',
                               temp_summer_setpoint_current='%d',
                               temp_summer_night_setforward_current='%d',
                               temp_summer_day_setforward_current='%d',
                               hours_per_day_nonoccupied_weekday_winter='%d',
                               hours_per_day_nonoccupied_weekend_winter='%d',
                               hours_per_day_nonoccupied_weekday_summer='%d',
                               hours_per_day_nonoccupied_weekend_summer='%d',
                               days_residence_vacant_summer='%d',
                               days_residence_vacant_winter='%d',
                               setback_temperature_vacant_residence_winter='%d',
                               ac_turned_off_vacant_residence_summer='%d',
                               owns_thermostat='%d'
                           WHERE id_device='%d'",
                           mysql_real_escape_string($device->id_room),
                           mysql_real_escape_string($device->id_residence),
                           mysql_real_escape_string($device->is_info_entered),
                           mysql_real_escape_string($device->fraction_residence_controlled*100),
                           mysql_real_escape_string($device->temp_winter_setpoint_current),
                           mysql_real_escape_string($device->temp_winter_night_setback_current),
                           mysql_real_escape_string($device->temp_winter_day_setback_current),
                           mysql_real_escape_string($device->temp_summer_setpoint_current),
                           mysql_real_escape_string($device->temp_summer_night_setforward_current),
                           mysql_real_escape_string($device->temp_summer_day_setforward_current),
                           mysql_real_escape_string($device->hours_per_day_nonoccupied_weekday_winter),
                           mysql_real_escape_string($device->hours_per_day_nonoccupied_weekend_winter),
                           mysql_real_escape_string($device->hours_per_day_nonoccupied_weekday_summer),
                           mysql_real_escape_string($device->hours_per_day_nonoccupied_weekend_summer),
                           mysql_real_escape_string($device->days_residence_vacant_summer),
                           mysql_real_escape_string($device->days_residence_vacant_winter),
                           mysql_real_escape_string($device->setback_temperature_vacant_residence_winter),
                           mysql_real_escape_string($device->ac_turned_off_vacant_residence_summer),
                           mysql_real_escape_string($device->owns_thermostat),
                           mysql_real_escape_string($device->id_device));
         if (!($mysqli->query($query))) 
         {
            error_log("Failed to update RO_TLD_Thermostat with id=".$device->id_device." .".$mysqli->error);
            return null;
         }
      }

      static function load_all_in_room($id_room)
      {
         $mysqli = connecti();
         $new_devices = array();
         $query = sprintf("SELECT * FROM TLD_Thermostats WHERE id_room_TLD_Thermostats='%d'",
                     mysql_real_escape_string($id_room));
         if(!($result = $mysqli->query($query)))
         {
            error_log("Failed to select Thermostat. ".$mysqli->error);
            return null;
         }
         while($a_row = $result->fetch_assoc())
         {
            array_push($new_devices, RO_TLD_Thermostat::copy_to_devices($a_row));
         }
                                            
         return $new_devices;
      }

      static function load($id_device)
      {
         $mysqli = connecti();
         $query = sprintf("SELECT * FROM TLD_Thermostats WHERE id_device='%d'",
                     mysql_real_escape_string($id_device));
         if (!($result = $mysqli->query($query)))
         {
            error_log ("Failed to select Thermostat with id_device ".$id_device.". ".$mysqli->error);
            return null;
         }

         if($a_row = $result->fetch_assoc())
         {
            return RO_TLD_Thermostat::copy_to_devices($a_row);
         }
         else
         {
            error_log("There is no Thermostat with ID ".$id_device);
            return null;
         }
      }

      static function copy_to_devices($a_row)
      {
         $new_device = new RO_TLD_Thermostat();
         
         $new_device->id_device = (int)$a_row['id_device'];
         $new_device->id_room = (int)$a_row['id_room_TLD_Thermostats'];
         $new_device->id_residence = (int)$a_row['id_residence_TLD_Thermostats'];
         $new_device->is_info_entered = (int) $a_row['is_info_entered'];

         $new_device->fraction_residence_controlled = 0.01*$a_row['fraction_residence_controlled_x100'];
         $new_device->temp_winter_setpoint_current = (int)$a_row['temp_winter_setpoint_current'];
         $new_device->temp_winter_night_setback_current = (int)$a_row['temp_winter_night_setback_current'];
         $new_device->temp_winter_day_setback_current = (int)$a_row['temp_winter_day_setback_current'];
         $new_device->temp_summer_setpoint_current = (int)$a_row['temp_summer_setpoint_current'];
         $new_device->temp_summer_night_setforward_current = (int)$a_row['temp_summer_night_setforward_current'];
         $new_device->temp_summer_day_setforward_current = (int)$a_row['temp_summer_day_setforward_current'];
         $new_device->hours_per_day_nonoccupied_weekday_winter = (int)$a_row['hours_per_day_nonoccupied_weekday_winter'];
         $new_device->hours_per_day_nonoccupied_weekend_winter = (int)$a_row['hours_per_day_nonoccupied_weekend_winter'];
         $new_device->hours_per_day_nonoccupied_weekday_summer = (int)$a_row['hours_per_day_nonoccupied_weekday_summer'];
         $new_device->hours_per_day_nonoccupied_weekend_summer = (int)$a_row['hours_per_day_nonoccupied_weekend_summer'];
         $new_device->days_residence_vacant_summer = (int)$a_row['days_residence_vacant_summer'];
         $new_device->days_residence_vacant_winter = (int)$a_row['days_residence_vacant_winter'];
         $new_device->setback_temperature_vacant_residence_winter = (int)$a_row['setback_temperature_vacant_residence_winter'];
         $new_device->ac_turned_off_vacant_residence_summer = (int)$a_row['ac_turned_off_vacant_residence_summer'];
         $new_device->owns_thermostat = (int)$a_row['owns_thermostat'];
     
         $new_device->load_actions($a_row);

         $new_device->action_free_winter_night_setback_best = $new_device->ro_actions[0];
         $new_device->action_free_summer_night_setforward_best = $new_device->ro_actions[1];
         $new_device->action_free_winter_night_setback_typical = $new_device->ro_actions[2];
         $new_device->action_free_summer_night_setforward_typical = $new_device->ro_actions[3];
         $new_device->action_free_winter_night_setback_worst = $new_device->ro_actions[4];
         $new_device->action_free_summer_night_setforwrd_worst = $new_device->ro_actions[5];
         $new_device->action_free_use_winter_day_setback_best = $new_device->ro_actions[6];
         $new_device->action_free_use_summer_day_setforward_best = $new_device->ro_actions[7];
         $new_device->action_free_use_winter_day_setback_typical = $new_device->ro_actions[8];
         $new_device->action_free_use_summer_day_setforward_typical = $new_device->ro_actions[9];
         $new_device->action_free_use_winter_day_setback_worst = $new_device->ro_actions[10];
         $new_device->action_free_use_summer_day_setforward_worst = $new_device->ro_actions[11];
         $new_device->action_free_use_winter_setpoint_best = $new_device->ro_actions[12];
         $new_device->action_free_use_summer_setpoint_best = $new_device->ro_actions[13];
         $new_device->action_free_use_winter_setpoint_typical = $new_device->ro_actions[14];
         $new_device->action_free_use_summer_setpoint_typical = $new_device->ro_actions[15];
         $new_device->action_free_use_winter_setpoint_worst = $new_device->ro_actions[16];
         $new_device->action_free_use_summer_setpoint_worst = $new_device->ro_actions[17];
         $new_device->action_free_use_vacant_winter_setback = $new_device->ro_actions[18];
         $new_device->action_free_use_vacant_summer_turn_off = $new_device->ro_actions[19];

         return $new_device;
      }
   }
?>
