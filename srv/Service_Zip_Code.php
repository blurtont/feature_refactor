<?php
include_once 'globals.php';
include_once 'RO_Location.php';

class Service_Zip_Code
{
   static function is_valid_zip_code($zip_code)
   {
      $mysqli = connecti_weather();

      $query = sprintf("SELECT * FROM Zip_Codes WHERE zip_code='%d'", 
                  mysql_real_escape_string($zip_code));
      if (!($result = $mysqli->query($query))) 
      { 
         error_log("Service_Zip_Code::is_valid_zip_code failed to select zip_code. ".$mysqli->error);
         return null;
      }
      if ($a_row = $result->fetch_assoc())
      {
         $ro_location = new RO_Location();
         $ro_location->zip_code = (int)$a_row['zip_code'];
         $ro_location->city = $a_row['city'];
         $ro_location->state = $a_row['state_abbreviation'];
         $ro_location->latitude = (0.001)*$a_row['latitude'];
         $ro_location->longitude = (0.001)*$a_row['longitude'];
         return $ro_location;
      }
      else
      {
         return null;
      }
   }
}

?>
