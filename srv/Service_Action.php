<?php
   include_once "globals.php";
   include_once "RO_Action.php";
   include_once "Type_Interaction.php";
   
   class Service_Action
   {
      static function update_action($ro_action)
      {
         RO_Action::update($ro_action);
      }

      static function action_add_to_todo_list(RO_Action $ro_action, $id_user, $id_device, $id_room, $id_type_room)
      {
         $interaction = new Interaction();
         $interaction->id_user = $id_user;
         $interaction->id_type_interaction = Type_Interaction::DEVICE_ACTION_ADD_TO_TODO_LIST;
         $interaction->id_device = $id_device;
         $interaction->id_type_device = $ro_action->id_type_device_Actions;
         $interaction->id_room = $id_room;
         $interaction->id_type_room = $id_type_room;
         $interaction->id_action = $ro_action->id_action;
         $interaction->id_type_action = $ro_action->id_type_action_Actions;
         $interaction->insert();

         RO_Action::update($ro_action);
      }

      static function action_view(RO_Action $ro_action, $id_user, $id_device, $id_room, $id_type_room)
      {
         $interaction = new Interaction();
         $interaction->id_user = $id_user;
         $interaction->id_type_interaction = Type_Interaction::DEVICE_ACTION_VIEW;
         $interaction->id_device = $id_device;
         $interaction->id_type_device = $ro_action->id_type_device_Actions;
         $interaction->id_room = $id_room;
         $interaction->id_type_room = $id_type_room;
         $interaction->id_action = $ro_action->id_action;
         $interaction->id_type_action = $ro_action->id_type_action_Actions;
         $interaction->insert();

         RO_Action::update($ro_action);
      }
}
?>
