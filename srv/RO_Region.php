<?php
   include_once('globals.php');

   class RO_Region
   {
      public $heating_energy_electric_annual_typical_kWh_per_sf;
      public $heating_energy_gas_annual_typical_BTU_per_sf;
      public $heating_energy_fuel_oil_annual_typical_BTU_per_sf;
      public $heating_energy_kerosene_annual_typical_BTU_per_sf;
      public $heating_energy_propane_annual_typical_BTU_per_sf;
      public $cooling_energy_annual_typical_kWh_per_sf;
      public $water_heating_energy_electric_annual_typical_BTU_per_household;
      public $water_heating_energy_gas_annual_typical_BTU_per_household;
      public $water_heating_energy_fuel_oil_typical_BTU_per_household;
      public $water_heating_energy_propane_annual_typical_BTU_per_household;
      public $lighting_appliances_energy_annual_typical_kWh_per_sf;
      public $number_persons_per_household_gas_water_heating_typical;
      public $number_persons_per_household_electric_water_heating_typical;
      public $square_footage_typical;
      
      public $cost_per_kWh;
      public $cost_per_ccf;

      static function load($id_region)
      {
         $mysqli = connecti();
         $query = sprintf("SELECT * FROM Regions WHERE id_region='%d'",
                  mysql_real_escape_string($id_region));
         if (!($result = $mysqli->query($query)))
         {
            error_log("RO_Region::load() failed to select region. ".$mysqli->error);
            return null;
         }
         
         return RO_Region::copy_to_region($result);
      }

      static function copy_to_region($result)
      {
         $ro_region = new RO_Region();
         
         if ($a_row = $result->fetch_assoc())
         {
            $ro_region->heating_energy_electric_annual_typical_kWh_per_sf = $a_row['heating_energy_electric_annual_typical_kWh_per_sf'];
            $ro_region->heating_energy_gas_annual_typical_BTU_per_sf = $a_row['heating_energy_gas_annual_typical_BTU_per_sf'];
            $ro_region->heating_energy_fuel_oil_annual_typical_BTU_per_sf = $a_row['heating_energy_fuel_oil_annual_typical_BTU_per_sf'];
            $ro_region->heating_energy_kerosene_annual_typical_BTU_per_sf = $a_row['heating_energy_kerosene_annual_typical_BTU_per_sf'];
            $ro_region->heating_energy_propane_annual_typical_BTU_per_sf = $a_row['heating_energy_propane_annual_typical_BTU_per_sf'];
            $ro_region->cooling_energy_annual_typical_kWh_per_sf = $a_row['cooling_energy_annual_typical_kWh_per_sf'];
            $ro_region->water_heating_energy_electric_annual_typical_BTU_per_household = $a_row['water_heating_energy_electric_annual_typical_BTU_per_household'];
            $ro_region->water_heating_energy_gas_annual_typical_BTU_per_household = $a_row['water_heating_energy_gas_annual_typical_BTU_per_household'];
            $ro_region->water_heating_energy_fuel_oil_typical_BTU_per_household = $a_row['water_heating_energy_fuel_oil_typical_BTU_per_household'];
            $ro_region->water_heating_energy_propane_annual_typical_BTU_per_household = $a_row['water_heating_energy_propane_annual_typical_BTU_per_household'];
            $ro_region->lighting_appliances_energy_annual_typical_kWh_per_sf = $a_row['lighting_appliances_energy_annual_typical_kWh_per_sf'];
            $ro_region->number_persons_per_household_gas_water_heating_typical = $a_row['number_persons_per_household_gas_water_heating_typical'];
            $ro_region->number_persons_per_household_electric_water_heating_typical = $a_row['number_persons_per_household_electric_water_heating_typical'];
            $ro_region->square_footage_typical = $a_row['square_footage'];
         }
         return $ro_region;
      }
   }
?>
