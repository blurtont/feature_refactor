<?php
include_once 'globals.php';
include_once 'Type_Interaction.php';

class Interaction
{
   //id_interaction (set by DB)
   public $id_user;
   //time (set by DB)
   public $id_type_interaction;
   public $id_device;
   public $id_type_device;
   public $id_room;
   public $id_type_room;
   public $id_action;
   public $id_type_action;
   public $id_type_page_journal;

   public function insert()
   {
      $mysqli = connecti();
      
      if ($this->id_user === NULL ||
          $this->id_type_interaction === NULL)
      {
         error_log("id_user || id_type_ineraction == NULL, can't insert interaction.");
         return;
      }
      elseif ($this->id_user == 0 && 
              $this->id_type_interaction == Type_Interaction::ACCOUNT_LOGIN)
      {
         $this->id_type_interaction = Type_Interaction::LOGIN_FAILED; 
      }
      
      $query = sprintf("INSERT INTO Interactions (id_user_Interactions,
                                                  ip,
                                                  id_type_interaction_Interactions,
                                                  id_device_Interactions,
                                                  id_type_device_Interactions,
                                                  id_room_Interactions,
                                                  id_type_room_Interactions,
                                                  id_action_Interactions,
                                                  id_type_action_Interactions)
                        VALUES (%s, '%d', %s, %s, %s, %s, %s, %s, %s)",
                        $this->get_value_or_null($this->id_user),
                        mysql_real_escape_string(ip2long($_SERVER['REMOTE_ADDR'])),
                        $this->get_value_or_null($this->id_type_interaction),
                        $this->get_value_or_null($this->id_device),
                        $this->get_value_or_null($this->id_type_device),
                        $this->get_value_or_null($this->id_room),
                        $this->get_value_or_null($this->id_type_room),
                        $this->get_value_or_null($this->id_action),
                        $this->get_value_or_null($this->id_type_action));
      if (!($mysqli->query($query)))
      {
         error_log("ERROR: Failed to insert interaction. ".$query." ".$mysqli->error);
      }
   }

   protected function get_value_or_null($value)
   {
      if ($value === NULL)
      {
         return "NULL";
      }
      else
      {
         return "'".mysql_real_escape_string($value)."'";
      }
   }
}
