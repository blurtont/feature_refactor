<?php
   include_once 'globals.php';
   include_once 'RO_Room.php';

class Service_Room
{
   public function update(Array $ro_rooms)
   {
      RO_Room::update($ro_rooms);
   }

   public function update_shallow_multiple(Array $ro_rooms)
   {
      RO_Room::update_shallow_multiple($ro_rooms);
   }

   public function update_shallow($ro_room)
   {
      RO_Room::update_shallow($ro_room);
   }

   public function load($id_room)
   {
      return RO_Room::load($id_room);
   }
}

?>
