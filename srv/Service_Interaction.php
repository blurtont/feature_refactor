<?php
   include_once "Interaction.php"; //DEPRICATED
   include_once "RO_Interaction.php";
   include_once "globals.php";

   class Service_Interaction
   {
      function insert_of_type($id_user, $type_interaction)
      {
         $interaction = new Interaction();
         $interaction->id_user = $id_user;
         $interaction->id_type_interaction = $type_interaction;
         $interaction->insert();
      }

      function insert(RO_Interaction $ro_interaction)
      {
         $ro_interaction->insert();
      }
   }
?>
