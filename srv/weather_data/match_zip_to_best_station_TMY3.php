<?php include_once 'globals.php';

   $mysqli = weather_connecti(); /* globals.php */

   $query_get_zip_codes = sprintf("Select zip_code, latitude, longitude FROM Zip_Codes");

   if (!($result_zip_codes = $mysqli->query($query_get_zip_codes)))
   {
      error_log("Failed to run query: ".$query_get_zip_codes."\n".$mysqli->error);
      return null;
   }

   while($row_zip_code = $result_zip_codes->fetch_assoc())
   {
      $query_nearest_station = sprintf("SELECT id_usaf, ( 6371 * acos( cos( radians(%f) ) * cos( radians( latitude_x1000/1000 ) ) * cos( radians( longitude_x1000/1000) - radians(%f) ) + sin( radians(%f) ) * sin( radians( latitude_x1000/1000 ) ) ) ) AS distance FROM Weather_Stations_TMY3 ORDER BY distance LIMIT 0 , 1", $row_zip_code['latitude']/1000.0, $row_zip_code['longitude']/1000.0, $row_zip_code['latitude']/1000.0);

      if (!($result_nearest_station = $mysqli->query($query_nearest_station)))
      {
         error_log("Failed to run query: ".$query_nearest_station."\n".$mysqli->error);
         return null;
      }

      if ($row_nearest_station = $result_nearest_station->fetch_assoc())
      {
         $query_update_zip_code = sprintf("UPDATE Zip_Codes SET id_usaf_TMY3_Zip_Codes = '%d', distance_station_TMY3_km = '%d' WHERE zip_code = '%d'", $row_nearest_station['id_usaf'], round($row_nearest_station['distance']), $row_zip_code['zip_code']);  
         if (!$mysqli->query($query_update_zip_code))
         {
            error_log("Failed to run query: ".$query_update_zip_code."\n".$mysqli->error);
            return null;
         }
         $message = sprintf("Matched zip code %d with station %d at %dkm\n\n", $row_zip_code['zip_code'], $row_nearest_station['id_usaf'], $row_nearest_station['distance']);
         echo $message;
      }
      else
      {
         $message = sprintf("Failed to find station for zip code %d\n", $row_zip_code['zipcode']);
         echo $message;
      }      
   }
?>
