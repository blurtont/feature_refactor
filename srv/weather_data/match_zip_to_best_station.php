<?php include_once '../globals.php';

   $mysqli = connecti(); /* globals.php */

   $query_get_zip_codes = sprintf("Select zip_code, latitude, longitude FROM Zip_Codes");

   if (!($result_zip_codes = $mysql->query($query_get_zip_codes)))
   {
      error_log("Failed to run query: ".$query_get_zip_codes."\n".$mysqli->error);
      return null;
   }

   while($row_zip_code = $result_zip_codes->fetch_assoc())
   {
      $query_nearest_station = sprintf("SELECT id_usaf, id_wban, date_begin, date_end, ( 6371 * acos( cos( radians(%f) ) * cos( radians( latitude/1000 ) ) * cos( radians( longitude/1000) - radians(%f) ) + sin( radians(%f) ) * sin( radians( latitude/1000 ) ) ) ) AS distance FROM Weather_Stations HAVING date_begin < '2010-01-01' AND date_end > '2012-01-01' ORDER BY distance LIMIT 0 , 1", $row_zip_code['latitude']/1000.0, $row_zip_code['longitude']/1000.0, $row_zip_code['latitude']/1000.0);

      if (!($result_nearest_station = $mysqli->query($query_nearest_station)))
      {
         error_log("Failed to run query: ".$query_nearest_station."\n".$mysqli->error);
         return null;
      }

      if ($row_nearest_station = $result_nearest_station->fetch_assoc())
      {
         $query_update_zip_code = sprintf("UPDATE Zip_Codes SET id_usaf_Zip_Codes = '%d', id_wban_Zip_Codes = '%d', distance_station_km = '%d' WHERE zip_code = '%d'", $row_nearest_station['id_usaf'], $row_nearest_station['id_wban'], round($row_nearest_station['distance']), $row_zip_code['zip_code']);  
         if (!$mysqli->query($query_update_zip_code))
         {
            error_log("Failed to run query: ".$query_update_zip_code."\n".$mysqli->error);
            return null;
         }
         $message = sprintf("Matched zip code %d with station %d:%d at %dkm\n\n", $row_zip_code['zip_code'], $row_nearest_station['id_usaf'], $row_nearest_station['id_wban'], $row_nearest_station['distance']);
         echo $message;
      }
      else
      {
         $message = sprintf("Failed to find station for zip code %d\n", $row_zip_code['zipcode']);
         echo $message;
      }      
   }
?>
