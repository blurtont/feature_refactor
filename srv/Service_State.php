<?php
   include_once 'globals.php';
   include_once 'RO_HVAC_Baseline.php';
   include_once 'RO_Leaderboard.php';
   include_once 'RO_State_Global.php';
   include_once 'RO_User.php';
   include_once 'RO_Room.php';
   include_once 'RO_Utility_Bill.php';
   include_once 'RO_Residence.php';
   include_once 'RO_Calculation_Result.php';

class Service_State {

   /* load state */
   function load_state(RO_User $user)
   {
      $mysqli = connecti();

      /* load residence */
      $query = sprintf("SELECT id_residence_Habitants
                        FROM Habitants
                        WHERE id_user_Habitants = '%d'",
                       mysql_real_escape_string($user->userID));
      
      if (!($resulti = $mysqli->query($query)))
      {
         error_log("unable to complete load residence query:Service_State.php. " . $mysqli->error);
         return null;
      }
      
      if (!($row = $resulti->fetch_array()))
      {
         /* failed to lookup residence; create default */
         $id_residence = 0;
         $id_residence = RO_Residence::create_default_residence($user->userID);
         
         /* query for residence */
         if (!($resulti = $mysqli->query($query)))
         {
            error_log("Failed to lookup residence. " . $myqsli->error);
            return null;
         }
         if (!($row = $resulti->fetch_assoc()))
         {
            error_log("Failed to lookup newly added user residence. Service_State.php. " . $mysqli->error.$query_res);
            return null;
         }
      }
      else
      {
         $id_residence = $row[0];
      }

      $result = new RO_State_Global();
      $result->user = $user;
      $result->residence = RO_Residence::load($id_residence);
      $result->utility_bills = RO_Utility_Bill::load($result->residence->id_residence);

      $ro_hvac_baseline_first = RO_HVAC_Baseline::get_first($result->residence->id_residence);
      $ro_hvac_baseline_latest = RO_HVAC_Baseline::get_latest($result->residence->id_residence); 
      $latest_calculation_result = RO_Calculation_Result::select($result->residence->id_residence);

      $result->HVAC_baseline_first = $ro_hvac_baseline_first;
      $result->HVAC_baseline_latest = $ro_hvac_baseline_latest;
      $result->latest_calculation_result = $latest_calculation_result; 
      $result->ro_leaderboard = RO_Leaderboard::load_zip($result->residence->zip);
      
      return $result;
   }
}

