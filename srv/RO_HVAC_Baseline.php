<?php

   class RO_HVAC_Baseline
   {      
      public $heating_slope_gas_or_oil_BTU_per_day_per_degF;
      public $heating_slope_electric_kWh_per_day_per_degF;
      public $cooling_slope_kWh_per_day_per_degF;
      public $baseline_electric_kWh_per_day;
      public $baseline_gas_or_oil_BTU_per_day;
      public $Tbalc;
      public $Tbalh_gas_or_oil;
      public $Tbalh_electric;

      public static function insert( $ro_HVAC_baseline, $id_residence, $id_user )
      {
         $mysqli = connecti();
         $query = sprintf( "INSERT INTO HVAC_Baseline (id_residence_HVAC_Baseline, 
                            id_user_HVAC_Baseline, 
                            heating_slope_gas_or_oil_BTU_per_day_per_degF_x10000,
                            heating_slope_electric_kWh_per_day_per_degF_x10000,
                            cooling_slope_kWh_per_day_per_degF_x10000,
                            baseline_electric_kWh_per_day_x10000,
                            baseline_gas_or_oil_BTU_per_day_x10000,
                            Tbalc_x10000,
                            Tbalh_gas_or_oil_x10000,
                            Tbalh_electric_x10000)
                            VALUES ('%d', '%d', '%d', '%d', '%d', '%d', '%d', '%d', '%d', '%d')",
                           mysql_real_escape_string( $id_residence ),
                           mysql_real_escape_string( $id_user ),
                           mysql_real_escape_string( $ro_HVAC_baseline->heating_slope_gas_or_oil_BTU_per_day_per_degF * 10000 ),
                           mysql_real_escape_string( $ro_HVAC_baseline->heating_slope_electric_kWh_per_day_per_degF * 10000 ),
                           mysql_real_escape_string( $ro_HVAC_baseline->cooling_slope_kWh_per_day_per_degF * 10000 ),
                           mysql_real_escape_string( $ro_HVAC_baseline->baseline_electric_kWh_per_day * 10000 ),
                           mysql_real_escape_string( $ro_HVAC_baseline->baseline_gas_or_oil_BTU_per_day * 10000 ),
                           mysql_real_escape_string( $ro_HVAC_baseline->Tbalc * 10000 ),
                           mysql_real_escape_string( $ro_HVAC_baseline->Tbalh_gas_or_oil * 10000 ),
                           mysql_real_escape_string( $ro_HVAC_baseline->Tbalh_electric * 10000 ) );
         if ( !( $mysqli->query( $query ) ) )
         {
            error_log("Failed to insert HVAC_Baseline. ".$mysqli->error);
         }
      }

      public static function get_latest( $id_residence )
      {
         $mysqli = connecti();
         $query = sprintf( "SELECT * FROM HVAC_Baseline 
                            WHERE id_residence_HVAC_Baseline='%d' ORDER BY time DESC LIMIT 1",
                           $id_residence );
         if ( !( $result = $mysqli->query( $query ) ) )
         {
            error_log( "Failed to get latest HVAC_Baseline. " . $mysqli->error );
            return null;
         }

         if ( $a_row = $result->fetch_assoc() )
         {   
            $ro_HVAC_baseline = new RO_HVAC_Baseline();
            $ro_HVAC_baseline->copy_result( $a_row ); 
            return $ro_HVAC_baseline;
         }

         /* no worries, we just don't have any HVAC_Baselines */
         return null;
      }

      public static function get_first( $id_residence )
      {
         $mysqli = connecti();
         $query = sprintf( "SELECT * FROM HVAC_Baseline 
                            WHERE id_residence_HVAC_Baseline='%d' 
                            ORDER BY time LIMIT 1",
                           $id_residence );
         if ( !( $result = $mysqli->query( $query ) ) )
         {
            error_log( "Failed to get first HVAC_Baseline. " . $mysqli->error );
            return null;
         }

         if ( $a_row = $result->fetch_assoc() )
         {   
            $ro_HVAC_baseline = new RO_HVAC_Baseline();
            return $ro_HVAC_baseline->copy_result( $a_row );
         }
         return null;
      }

      public function copy_result( $a_row )
      {
         $this->heating_slope_gas_or_oil_BTU_per_day_per_degF = $a_row["heating_slope_gas_or_oil_BTU_per_day_per_degF_x10000"] * 0.0001;
         $this->heating_slope_electric_kWh_per_day_per_degF = $a_row["heating_slope_electric_kWh_per_day_per_degF_x10000"] * 0.0001;
         $this->cooling_slope_kWh_per_day_per_degF = $a_row["cooling_slope_kWh_per_day_per_degF_x10000"] * 0.0001;
         $this->baseline_electric_kWh_per_day = $a_row["baseline_electric_kWh_per_day_x10000"] * 0.0001;
         $this->baseline_gas_or_oil_BTU_per_day = $a_row["baseline_gas_or_oil_BTU_per_day_x10000"] * 0.0001;
         $this->Tbalc = $a_row["Tbalc_x10000"] * 0.0001;
         $this->Tbalh_gas_or_oil = $a_row["Tbalh_gas_or_oil_x10000"] * 0.0001;
         $this->Tbalh_electric = $a_row["Tbalh_electric_x10000"] * 0.0001;
         return $this;
      }
   }
?>
