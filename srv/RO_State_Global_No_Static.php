<?php

include_once 'RO_Residence.php';

class RO_State_Global_No_Static
{
   public $user; /* User.php */
   public $residence; /* RO_Residence.php */
   public $utility_bills;

   public function __construct()
   {
      $this->residence = new RO_Residence;
   }
}
