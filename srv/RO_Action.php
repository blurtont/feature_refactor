<?php
   include_once 'Type_Interaction.php';
   include_once 'Interaction.php';
   include_once 'globals.php';

   class RO_Action 
   {
      public $id_action;
      public $id_residence_Actions;
      public $id_type_action_Actions;
      public $id_type_device_Actions;

      public $added_to_todo_list;
      public $completed;
      public $completed_from_todo;
      public $is_viewed;

      public $time_added_todo;
      public $time_completed;
      public $time_first_viewed;

      /* new unix time stuff */
      public $unix_time_added_todo;
      public $unix_time_completed;
      public $unix_time_first_viewed;

      public $savings_completed_from_todo_kWh_per_year;
      public $savings_completed_from_todo_cost_per_year;
      public $savings_completed_from_todo_cost_per_year_cooling;
      public $savings_completed_from_todo_cost_per_year_heating;

      static function create($type_device, $type_action)
      {
         $mysqli = connecti();
         $result = new RO_Action();
         $query = sprintf("INSERT INTO Actions (id_type_device_Actions,
                                                id_type_action_Actions)
                           VALUES ('%d', '%d')",
                           mysql_real_escape_string($type_device),
                           mysql_real_escape_string($type_action));
         if (!($mysqli->query($query))) 
         { 
            error_log('unable to create ro_action--
            id_residence='.$id_residence.', 
            type_device='.$type_device.', 
            type_action='.$type_action.$mysqli->error);
            return null;
         }

         $result->id_action = $mysqli->insert_id;
         return $result;
      }

      /* store RO_Action object, insert and on duplicate update */
      static function update(RO_Action $ro_action)
      {
         $mysqli = connecti( );
         $query = sprintf( "UPDATE Actions 
                            SET id_residence_Actions='%d',
                                id_type_action_Actions='%d',
                                id_type_device_Actions='%d',
                                added_to_todo_list='%d',
                                completed='%d',
                                completed_from_todo='%d',
                                unix_time_added_todo='%d',
                                unix_time_completed='%d',
                                savings_completed_from_todo_kWh_per_year_1000x='%d',
                                savings_completed_from_todo_cost_per_year_100x='%d',
                                savings_completed_from_todo_cost_per_year_cooling_100x='%d',
                                savings_completed_from_todo_cost_per_year_heating_100x='%d',
                                is_viewed='%d',
                                unix_time_first_viewed='%d'
                            WHERE id_action='%d'",
                           mysql_real_escape_string( $ro_action->id_residence_Actions ),
                           mysql_real_escape_string( $ro_action->id_type_action_Actions ),
                           mysql_real_escape_string( $ro_action->id_type_device_Actions ),
                           mysql_real_escape_string( $ro_action->added_to_todo_list ),
                           mysql_real_escape_string( $ro_action->completed ),
                           mysql_real_escape_string( $ro_action->completed_from_todo ),
                           mysql_real_escape_string( $ro_action->unix_time_added_todo ),
                           mysql_real_escape_string( $ro_action->unix_time_completed ),
                           mysql_real_escape_string( $ro_action->savings_completed_from_todo_kWh_per_year*1000 ),
                           mysql_real_escape_string( $ro_action->savings_completed_from_todo_cost_per_year*100 ),
                           mysql_real_escape_string( $ro_action->savings_completed_from_todo_cost_per_year_cooling*100 ),
                           mysql_real_escape_string( $ro_action->savings_completed_from_todo_cost_per_year_heating*100 ),
                           mysql_real_escape_string( $ro_action->is_viewed ),
                           mysql_real_escape_string( $ro_action->unix_time_first_viewed ),
                           mysql_real_escape_string( $ro_action->id_action ) );

         if ( !( $mysqli->query( $query ) ) )
         {
            error_log( "Failed to update action item with id_action = " . $ro_action->id_action . " " . $mysqli->error );
            return null;
         }
      
      }


      static function update_actions( $ro_actions )
      {
         foreach ( $ro_actions as $ro_action )
         {
            if ($ro_action != NULL)
            {
               RO_Action::update($ro_action);
            }
         }
      }

      static function load( $id_action )
      {
         $mysqli = connecti( );
         $ro_action = new RO_Action( );
         $query = sprintf( "SELECT id_action,
                                   id_residence_Actions,
                                   id_type_action_Actions,
                                   id_type_device_Actions,
                                   added_to_todo_list,
                                   completed,
                                   completed_from_todo,
                                   unix_time_added_todo,
                                   unix_time_completed,
                                   savings_completed_from_todo_kWh_per_year_1000x,
                                   savings_completed_from_todo_cost_per_year_100x,
                                   savings_completed_from_todo_cost_per_year_heating_100x,
                                   savings_completed_from_todo_cost_per_year_cooling_100x,
                                   unix_time_first_viewed,
                                   is_viewed
                            FROM Actions WHERE id_action='%d'",
                            mysql_real_escape_string( $id_action ) );
         if ( !( $result = $mysqli->query( $query ) ) )
         { 
            error_log( "failed to load action " . $id_action . $mysqli->error );
            return null;
         }

         if ( $a_row = $result->fetch_assoc( ) )
         {
            $ro_action->id_action = (int) $a_row[ 'id_action' ];
            $ro_action->id_residence_Actions = (int) $a_row[ 'id_residence_Actions' ];
            $ro_action->id_type_action_Actions = (int) $a_row[ 'id_type_action_Actions' ];
            $ro_action->id_type_device_Actions = (int) $a_row[ 'id_type_device_Actions' ];
            $ro_action->added_to_todo_list = (int) $a_row[ 'added_to_todo_list' ];
            $ro_action->completed = (int) $a_row[ 'completed' ];
            $ro_action->completed_from_todo = (int) $a_row[ 'completed_from_todo' ];
            $ro_action->unix_time_added_todo = $a_row[ 'unix_time_added_todo' ];
            $ro_action->unix_time_completed = $a_row[ 'unix_time_completed' ];
            $ro_action->savings_completed_from_todo_kWh_per_year = 0.001 * $a_row[ 'savings_completed_from_todo_kWh_per_year_1000x' ];
            $ro_action->savings_completed_from_todo_cost_per_year = 0.01 * $a_row[ 'savings_completed_from_todo_cost_per_year_100x' ];
            $ro_action->savings_completed_from_todo_cost_per_year_heating = 0.01 * $a_row[ 'savings_completed_from_todo_cost_per_year_heating_100x' ];
            $ro_action->savings_completed_from_todo_cost_per_year_cooling = 0.01 * $a_row[ 'savings_completed_from_todo_cost_per_year_cooling_100x' ];
            $ro_action->unix_time_first_viewed = $a_row[ 'unix_time_first_viewed' ];
            $ro_action->is_viewed = (int) $a_row[ 'is_viewed' ];
         }
         else
         {
            error_log("ERROR RO_Action.php: failed to load action with id=".$id_action.". ");
            return null;
         }

         return $ro_action;
      }
      
      static function remove($action)
      {   
         $mysqli = connecti();
         $query = sprintf("DELETE FROM Actions WHERE id_action='%d'",
                          mysql_real_escape_string($action->id_action));
         if (!($mysqli->query($query))) 
         {
            error_log("failed to delete Action with id_action=".$action->id_action);
            return null;
         }
      }        

      /* todo: need id_residence_dup */
      static function dup($action)
      {
         $mysqli = connecti();
         if ($action->id_action == 0)
         {
            error_log("WARNING: RO_Action, dup, id_action == 0. not dupping.");
            return NULL;
         }
         /* create new empty action */
         $query = "INSERT INTO Actions () VALUES ()";
         if (!($mysqli->query($query))) 
         {
            error_log("Failed to create new dup action.".$mysqli->error);
            return null;
         }
         $dup_action_id = $mysqli->insert_id;
         $action->id_action = $dup_action_id;
         //note: doing this hackily in RO_Room::duplicate_room($id_room, $id_residence_dup)

         /* update dup action */
         RO_Action::update($action);
         return $dup_action_id;
      }
   }
?>
