<?php
   include_once("globals.php");

   function send_contact_us($name_first, $name_last, $email, $send_to_email, $message)
   {
      $subject = $name_first." ".$name_last." - Contact Us Message";

      /* scrub send to email in case someone moron tries to spoof the post variables... */
      if ($send_to_email != "info@dropoly.com" && 
          $send_to_email != "investors@dropoly.com" &&
          $send_to_email != "partnerships@dropoly.com" &&
          $send_to_email != "jobs@dropoly.com")
      {
         $sent_to_email = "info@dropoly.com";
      }
           
      send_contact_us_email($email, $name_first." ".$name_last, $send_to_email, $message, $subject);
      error_log("sent contact us");
   }
?>
