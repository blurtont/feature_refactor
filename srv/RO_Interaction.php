<?php

   class RO_Interaction
   {
      public $id_user;
      public $type_interaction;
      
      public $id_device;
      public $type_device;
      
      public $id_room;
      public $type_room;
      
      public $id_action;
      public $type_action;

      public function insert()
      {
         $mysqli = connecti();
         $this->_clean();
         $query = sprintf("INSERT INTO Interactions (id_user_Interactions, 
                                                     id_type_interaction_Interactions, 
                                                     id_device_Interactions,
                                                     id_type_device_Interactions,
                                                     id_room_Interactions,
                                                     id_type_room_Interactions,
                                                     id_action_Interactions,
                                                     id_type_action_Interactions)
                           VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
                           $this->get_value_or_null($this->id_user),
                           $this->get_value_or_null($this->type_interaction),
                           $this->get_value_or_null($this->id_device),
                           $this->get_value_or_null($this->type_device),
                           $this->get_value_or_null($this->id_room),
                           $this->get_value_or_null($this->type_room),
                           $this->get_value_or_null($this->id_action),
                           $this->get_value_or_null($this->type_action));
         if (!($mysqli->query($query)))
         {
            error_log ("ERROR: RO_Interaction->insert() Failed to insert interaction.".$mysqli->error);
            return null;
         }
      }

      private function _clean()
      {
         foreach ($this as $key => $value)
         {
            if ($value == 0)
            {
               $this->$key = NULL;
            }  
         }
      }

      protected function get_value_or_null($value)
      {
         if ($value === NULL)
         {
            return "NULL";
         }
         else
         {
            return "'".mysql_real_escape_string($value)."'";
         }
      }
   }
?>
