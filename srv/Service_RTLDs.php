<?php
include_once 'INCLUDE_RO_RLDs.php';
include_once 'INCLUDE_RO_TLDs.php';
include_once 'Type_Device.php';
include_once 'globals.php';
include_once 'Interaction.php';
include_once 'Type_Interaction.php';

class Service_RTLDs
{
   public function remove($ro_device)
   {
      $ro_device->remove();
   }

   public function update_rtld($type_device, $ro_device, $type_room, $id_user)
   {
      $interaction = new Interaction();
      $interaction->id_user = $id_user;
      $interaction->id_type_interaction = Type_Interaction::DEVICE_INFO_EDIT;
      $interaction->id_device = $ro_device->id_device;
      $interaction->id_type_device = $type_device;
      $interaction->id_room = $ro_device->id_room; 
      $interaction->id_type_room = $type_room;
      $interaction->insert();

      /* TODO make this not a static function */
      $ro_device::update($ro_device);
   }
   
   public function create($id_residence, $id_room, $type_device)
   {
      switch ($type_device)
      {
         case Type_Device::BASEMENT_CRAWLSPACE_FLOOR:
            $result = RO_TLD_Basement_Crawlspace_Floor::create_default($id_room);
            break;
         case Type_Device::BASEMENT_FLOOR:
            $result = RO_TLD_Basement_Floor::create_default($id_room);
            break;
         case Type_Device::BASEMENT_WALL:
            $result = RO_TLD_Basement_Wall::create_default($id_room);
            break;
         case Type_Device::CEILING:
            $result = RO_TLD_Ceiling::create_default($id_room);
            break;
         case Type_Device::CLOTHES_DRYER:
            $result = RO_RLD_Clothes_Dryer::create_default($id_room);
            break;
         case Type_Device::CLOTHES_WASHER:
            $result = RO_RLD_Clothes_Washer::create_default($id_room);
            break;
         case Type_Device::COFFEE_MAKER:
            $result = RO_RLD_Coffee_Maker::create_default($id_room);
            break;
         case Type_Device::COOKTOP:
            $result = RO_RLD_Cooktop::create_default($id_room);
            break;
         case Type_Device::COOLING_SYSTEM:
            $result = RO_TLD_Cooling_System::create_default($id_room);
            break;
         case Type_Device::DISH_WASHER:
            $result = RO_RLD_Dish_Washer::create_default($id_room);
            break;
         case Type_Device::DOOR:
            $result = RO_RLD_Door::create_default($id_room);
            break;
         case Type_Device::DUCTWORK:
            $result = RO_TLD_Ductwork::create_default($id_room);
            break;
         case Type_Device::FAUCET:
            $result = RO_RLD_Faucet::create_default($id_room);  
            break;
         case Type_Device::HEATING_SYSTEM:
            $result = RO_TLD_Heating_System::create_default($id_room);
            break;
         case Type_Device::HOUSE_SEAL:
            $result = RO_TLD_House_Seal::create_default($id_room);
            break;
         case Type_Device::LIGHT:
            $result = RO_RLD_Light::create_default($id_room);
            break;
         case Type_Device::LOCAL_COOLING:
            $result = RO_RLD_Local_Cooling_System::create_default($id_room);
            break;
         case Type_Device::LOCAL_HEATING:
            $result = RO_RLD_Local_Heating_System::create_default($id_room);
            break;
         case Type_Device::OVEN:
            $result = RO_RLD_Oven::create_default($id_room);
            break;
         case Type_Device::PLUG_LOAD:
            $result = RO_RLD_Plug_Load::create_default($id_room);
            break;
         case Type_Device::REFRIGERATOR:
            $result = RO_RLD_Refrigerator::create_default($id_room);
            break;
         case Type_Device::SHOWER:
            $result = RO_RLD_Shower::create_default($id_room);
            break;
         case Type_Device::SLAB_FLOOR:
            $result = RO_TLD_Slab::create_default($id_room);
            break;
         case Type_Device::THERMOSTAT:
            $result = RO_TLD_Thermostat::create_default($id_room);
            break;
         case Type_Device::WALL:
            $result = RO_TLD_Wall::create_default($id_room);
            break;
         case Type_Device::WATER_HEATER:
            $result = RO_TLD_Water_Heater::create_default($id_room);   
            break;
         case Type_Device::WINDOW_SYSTEM:
            $result = RO_RLD_Window_System::create_default($id_room);
            break;
      } 

      $result->id_residence = $id_residence;
      $service_rtlds = new Service_RTLDs();
      $service_rtlds->update_rtld($type_device, $result, 0, 0);
      return $result;
   }

}
