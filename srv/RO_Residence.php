<?php
include_once 'globals.php';
include_once 'RO_Room.php';
include_once 'RO_Savings_Monthly.php';
include_once 'RO_Utility_Bill.php';
include_once 'RO_Region.php';

class RO_Residence
{
   public $id_residence;
   public $city;
   public $id_state;
   public $zip;
   public $ro_region;
   public $address_street;
   public $color_main;
   public $color_accent;
   public $type_backdrop;
   public $num_residents;
   public $is_rent;
   public $type_basement;
   public $type_appliance_use;
   public $ceiling_height_feet;
   public $num_floors;
   public $square_footage;
   public $is_gas_or_oil_use;
   public $type_residence;

   /* DEPRICATED */
   public $is_fuel_type_electric;
   public $is_fuel_type_fuel_oil;
   public $is_fuel_type_natural_gas;
   public $is_fuel_type_propane;
   /* DEPRICATED */

   public $date_meter_electric;
   public $date_meter_natural_gas;
   public $date_meter_heating_oil;
   public $date_meter_propane;

   public $ro_rooms;
   public $type_rooms_available;
   public $ro_savings_monthlies;
   
   /* create an empty new residence */
   static function create_default_residence($id_user)
   {
      $mysqli = connecti();

      /* create default residence */
      $query = "INSERT INTO Residences () VALUES ()";
      if (!($mysqli->query($query)))
      {
         error_log ("Failed to insert new default residence. ".$mysqli->error);
         return null;
      }
      $id_residence = $mysqli->insert_id;

      /* set user residence id */
      $query = sprintf("INSERT INTO Habitants (id_residence_Habitants, id_user_Habitants, own) VALUES ('%d', '%d', 0) 
                  ON DUPLICATE KEY UPDATE id_residence_Habitants='%d'",
                  mysql_real_escape_string($id_residence),
                  mysql_real_escape_string($id_user),
                  mysql_real_escape_string($id_residence));
      if (!($mysqli->query($query))) 
      {
         error_log ("Failed to create residence ID in Habitants. ".$mysqli->error);
         return null;
      }

      /* create rooms */
      /* $included_room_type = array(1, 2, 7, 10, 14);
      foreach($included_room_type as $type_room)
      {
         RO_Room::create_default($id_residence, $type_room);
      }
      */
      unset($type_room); 
      
      return $id_residence;
   }

   /* Load residence $id_residence, create and return RO_Residence object */
   static function load($id_residence)
   {
      $mysqli = connecti();

      /* create new RO_Residence object */
      $ro_residence = new RO_Residence();
      
      /* load Residence properties */
      $query = sprintf("SELECT city,
                               id_state_Zip_Codes,
                               zip_code_Residences,
                               address_street,
                               color_main,
                               color_accent,
                               type_backdrop,
                               num_residents,
                               is_rent,
                               square_footage,
                               type_basement,
                               type_appliance_use,
                               ceiling_height_feet_x10,
                               num_floors,
                               is_gas_or_oil_use,
                               type_residence,
                               is_fuel_type_electric,
                               is_fuel_type_fuel_oil,
                               is_fuel_type_natural_gas,
                               is_fuel_type_propane,
                               date_meter_electric,
                               date_meter_natural_gas,
                               date_meter_fuel_oil,
                               date_meter_propane
                        FROM Residences, Zip_Codes WHERE id_residence='%d' AND zip_code = zip_code_Residences",
                  mysql_real_escape_string($id_residence));
      if (!($result = $mysqli->query($query))) 
      {
         error_log ("Failed to select residence. ".$mysqli->error);
         return null;
      }
      if (!($a_row = $result->fetch_assoc()))
      {
         error_log ("Failed to select residence, no entry for ".$id_residence);
         return null;
      }
      $ro_residence->id_residence = $id_residence;
      $ro_residence->city = $a_row['city'];
      $ro_residence->id_state = $a_row['id_state_Zip_Codes'];
      $ro_residence->zip = (int)$a_row['zip_code_Residences'];
      $ro_residence->address_street = $a_row['address_street'];
      $ro_residence->color_main = (int) $a_row['color_main'];
      $ro_residence->color_accent = (int) $a_row['color_accent'];
      $ro_residence->type_backdrop = (int) $a_row['type_backdrop'];
      $ro_residence->num_residents = (int) $a_row['num_residents'];
      $ro_residence->is_rent = (int) $a_row['is_rent'];
      $ro_residence->square_footage = (int) $a_row['square_footage'];
      $ro_residence->type_basement = (int) $a_row['type_basement'];
      $ro_residence->type_appliance_use = (float) $a_row['type_appliance_use'];
      $ro_residence->ceiling_height_feet = 0.1*$a_row['ceiling_height_feet_x10'];
      $ro_residence->num_floors = (int) $a_row['num_floors'];
      $ro_residence->is_gas_or_oil_use = (int) $a_row['is_gas_or_oil_use'];
      $ro_residence->type_residence = (int) $a_row['type_residence'];

      $ro_residence->is_fuel_type_electric = (int) $a_row['is_fuel_type_electric'];
      $ro_residence->is_fuel_type_fuel_oil = (int) $a_row['is_fuel_type_fuel_oil'];
      $ro_residence->is_fuel_type_natural_gas = (int) $a_row['is_fuel_type_natural_gas'];
      $ro_residence->is_fuel_type_propane = (int) $a_row['is_fuel_type_propane'];

      $ro_residence->date_meter_electric = (int) $a_row['date_meter_electric'];
      $ro_residence->date_meter_natural_gas = (int) $a_row['date_meter_natural_gas'];
      $ro_residence->date_meter_heating_oil = (int) $a_row['date_meter_fuel_oil'];
      $ro_residence->date_meter_propane = (int) $a_row['date_meter_propane'];

      $ro_residence->type_rooms_available = array(1, 2, 9, 4, 5, 7, 10, 11, 12, 14);
      $ro_residence->ro_rooms = RO_Room::load_rooms($id_residence);

      /* get ro_region */
      $query = sprintf("SELECT * FROM States WHERE id_state='%d'",
                  mysql_real_escape_string($ro_residence->id_state));
      if (!($result = $mysqli->query($query))) 
      {
         error_log ("RO_Residence::load failed to select state". $mysqli->error);
         return null;
      }
      if (($a_row = $result->fetch_assoc()) == null) error_log ("RO_Residence::load, Failed to select state with id=".$ro_residence->id_state);
      $ro_residence->ro_region = RO_Region::load($a_row['id_region_States']);
      $ro_residence->ro_region->cost_per_kWh = $a_row['cost_per_kWh'];
      $ro_residence->ro_region->cost_per_ccf = $a_row['cost_per_ccf'];

      /* return RO_Residence */
      return $ro_residence;
   }

   public function remove()
   {
      $mysqli = connecti();
      $query = sprintf("DELETE FROM Residences WHERE id_residence='%d'",
                        mysql_real_escape_string($this->id_residence));
      if (!($mysqli->query($query)))
      {
         error_log ("failed to delete residence");
         return null;
      }

      foreach($this->ro_rooms as $ro_room)
      {
         RO_Room::remove($ro_room);
      }
   }

   static function update(RO_Residence $residence)
   {
      RO_Residence::update_shallow($residence);
      RO_Room::update($residence->ro_rooms);
      if(is_array($residence->ro_savings_monthlies))
      {
         RO_Savings_Monthly::update($residence->ro_savings_monthlies);
      }
   }

   static function update_shallow(RO_Residence $residence)
   {
      $mysqli = connecti();
      $query = sprintf("UPDATE Residences SET zip_code_Residences='%d',
                                              address_street='%s',
                                              type_residence='%d',
                                              color_main='%d',
                                              color_accent='%d',
                                              type_backdrop='%d',
                                              num_residents='%d',
                                              is_rent='%d',
                                              type_basement='%d',
                                              type_appliance_use='%f',
                                              ceiling_height_feet_x10='%d',
                                              num_floors='%d',
                                              square_footage='%d',
                                              is_gas_or_oil_use='%d',
                                              is_fuel_type_electric='%d',
                                              is_fuel_type_fuel_oil='%d',
                                              is_fuel_type_natural_gas='%d',
                                              is_fuel_type_propane='%d',
                                              date_meter_electric='%d',
                                              date_meter_natural_gas='%d',
                                              date_meter_fuel_oil='%d',
                                              date_meter_propane='%d'
                        WHERE id_residence='%d'",
                        mysql_real_escape_string($residence->zip),
                        mysql_real_escape_string($residence->address_street),
                        mysql_real_escape_string($residence->type_residence),
                        mysql_real_escape_string($residence->color_main),
                        mysql_real_escape_string($residence->color_accent),
                        mysql_real_escape_string($residence->type_backdrop),
                        mysql_real_escape_string($residence->num_residents),
                        mysql_real_escape_string($residence->is_rent),
                        mysql_real_escape_string($residence->type_basement),
                        mysql_real_escape_string($residence->type_appliance_use),
                        mysql_real_escape_string($residence->ceiling_height_feet*10),
                        mysql_real_escape_string($residence->num_floors),
                        mysql_real_escape_string($residence->square_footage),
                        mysql_real_escape_string($residence->is_gas_or_oil_use),
                        mysql_real_escape_string($residence->is_fuel_type_electric),
                        mysql_real_escape_string($residence->is_fuel_type_fuel_oil),
                        mysql_real_escape_string($residence->is_fuel_type_natural_gas),
                        mysql_real_escape_string($residence->is_fuel_type_propane),
                        mysql_real_escape_string($residence->date_meter_electric),
                        mysql_real_escape_string($residence->date_meter_natural_gas),
                        mysql_real_escape_string($residence->date_meter_heating_oil),
                        mysql_real_escape_string($residence->date_meter_propane),
                        mysql_real_escape_string($residence->id_residence));
      if (!($mysqli->query($query)))
      {
         error_log ("failed to update residence with id_residence ".$residence->id_residence.$mysqli->error.$query);
         return null;
      }
   }

   /* duplicates the residence with $id_residence
      returns new residence */
   static function duplicate($id_user, $id_residence)
   {
      $mysqli = connecti();
      /* create Habitants entry */
      $query = sprintf("SELECT * FROM Habitants WHERE id_residence_Habitants='%d'
                        ORDER BY id_user_Habitants ASC",
                        mysql_real_escape_string($id_residence));
      if (!($result = $mysqli->query($query))) 
      {
         error_log("failed to select old entry in Habitants for duplicate.".$mysqli->error);
         return null;
      }
      if($a_row = $result->fetch_assoc())
      {
         /* create a new "empty" residence */
         $query = "INSERT INTO Residences () VALUES ()";
         if (!($mysqli->query($query)))
         {
            error_log ("Failed to insert new default residence. ".$mysqli->error);
            return null;
         }
         $id_residence_dup = mysql_insert_id();
         $query = sprintf("INSERT INTO Habitants (id_residence_Habitants, 
                                                  id_user_Habitants, 
                                                  own, 
                                                  is_questionnaire_residence_complete, 
                                                  is_questionnaire_user_complete) 
                           VALUES ('%d', '%d', '%d', '%d', '%d')",
                           mysql_real_escape_string($id_residence_dup),
                           mysql_real_escape_string($id_user),
                           mysql_real_escape_string((int)$a_row['own']),
                           mysql_real_escape_string((int)$a_row['is_questionnaire_residence_complete']),
                           mysql_real_escape_string((int)$a_row['is_questionnaire_user_complete']));
         if (!($mysqli->query($query)))
         {
            error_log("failed to update new Habitants entry on duplicate");
            return null;
         }

         /* load residence to copy */
         $ro_residence = RO_Residence::load($id_residence);

         /* set residence id to duplicate residence */
         $ro_residence->id_residence = $id_residence_dup;
         RO_Residence::update_shallow($ro_residence);
         RO_Room::duplicate($ro_residence->ro_rooms, $ro_residence->id_residence);
         return $id_residence_dup;
      }
      else
      {
         error_log("failed to create duplicate Habitants entry");
      }
      return $ro_residence;
   }
}
