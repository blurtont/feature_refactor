<?php
include_once 'globals.php';
include_once 'RO_Calculation_Result.php';

   class Service_Calculation_Result
   {
      public static function insert($ro_calculation_result)
      {
         connect();
         RO_Calculation_Result::insert($ro_calculation_result);
      }

      public static function select($id_residence)
      {
         connect();
         return RO_Calculation_Result::select($id_residence);
      }

      public static function select_worst($zip_code)
      {
         connect();
         return RO_Calculation_Result::select_worst($zip_code);
      }

      public static function select_best($zip_code)
      {
         connect();
         return RO_Calculation_Result::select_best($zip_code);
      }

      public static function select_average($zip_code)
      {
         connect();
         return RO_Calculation_Result::select_average($zip_code);
      }
   }
?>
