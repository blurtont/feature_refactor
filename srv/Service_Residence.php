<?php
   include_once "globals.php";
   include_once "Interaction.php";
   include_once "RO_Residence.php";
   include_once "Service_Interaction.php";
   include_once "Type_Interaction.php";

class Service_Residence
{
   public function add_room($type_room, $id_residence)
   {
      $ro_room = RO_Room::create_default($id_residence, $type_room);
      return $ro_room;
   }

   public function load($id_user)
   {
      /* get id_residence */
      $mysqli = connecti();
      $query = sprintf("SELECT id_residence_Habitants, own FROM Habitants WHERE id_user_Habitants='%d'",
                  mysql_real_escape_string($id_user));
       if (!($result = $mysqli->query($query)))
       {
          error_log ("Failed to find residence for id_user=".$id_user.". ".$mysqli->error);
          return null;
       }
      if ($a_row = $result->fetch_assoc())
      {
         $residence = RO_Residence::load($a_row['id_residence_Habitants']);
         return $residence;
      }
      return 0;
   }

   public function duplicate(RO_Residence $ro_residence, $id_user_dup)
   {
      return RO_Residence::duplicate($id_user_dup, $ro_residence->id_residence);
   }

   public function remove(RO_Residence $ro_residence)
   {
      $ro_residence->remove();
   }

   public function remove_room($ro_room)
   {
      RO_Room::remove($ro_room);
      return $ro_room->id_room;
   }

   public function update(RO_Residence $ro_residence)
   {
      RO_Residence::update($ro_residence);
   }

   public function update_shallow(RO_Residence $ro_residence)
   {
      RO_Residence::update_shallow($ro_residence);
   }

   public function view_room($id_user, $id_room, $id_type_room)
   {
      $interaction = new Interaction();
      $interaction->id_user = $id_user;
      $interaction->id_type_interaction = Type_Interaction::ROOM_VIEW;
      $interaction->id_room = $id_room;
      $interaction->id_type_room = $id_type_room;
      $interaction->insert();
   }
}
?>
