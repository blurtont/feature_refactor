<?php
include_once 'globals.php';
include_once 'RO_Utility_Bill.php';
include_once 'Type_Interaction.php';

class Service_Utility
{
   public function update($id_user, $utility_bills, $id_residence)
   {
      RO_Utility_Bill::update_new($utility_bills, $id_residence);
      if (sizeof($utility_bills)>0)
      {
         return RO_Utility_Bill::load($id_residence);
      }
      else
      {
         return null;
      }
   }

   public function create($id_user, $id_residence, $type_utility, $energy, $meter_date) //DEPRICATED
   {
      $interaction = new Interaction();
      $interaction->id_user = $id_user;
      $interaction->id_type_interaction = Type_Interaction::UTILITY_BILL_CREATE;
      $interaction->insert();

      RO_Utility_Bill::create($id_residence, $type_utility, $energy, $meter_date);
      return RO_Utility_Bill::load($id_residence);
   }

   public function create_default($ro_residence, $id_user) //DEPRICATED
   {
      $interaction = new Interaction();
      $interaction->id_user = $id_user;
      $interaction->id_type_interaction = Type_Interaction::UTILITY_SKIP_ENTRY;
      $interaction->insert();

      RO_Utility_Bill::create_default($ro_residence);
      return RO_Utility_Bill::load($ro_residence->id_residence);
   }

   public function load($id_residence)
   {
      return RO_Utility_Bill::load($id_residence);
   }

   public function duplicate($ro_utility_bills, $id_residence_dup)
   {
      foreach($ro_utility_bills as $ro_utility_bill)
      {
         $ro_utility_bill->duplicate($id_residence_dup);
      }
   }

   public function remove($ro_utility_bills, $id_residence)
   {
      RO_Utility_Bill::remove($ro_utility_bills);
      return RO_Utility_Bill::load($id_residence);
   }

   public function remove_all_of_types($id_residence, $type_fuels)
   {
      RO_Utility_Bill::remove_all_of_types($id_residence, $type_fuels);
      return RO_Utility_Bill::load($id_residence);
   }
}
?>
