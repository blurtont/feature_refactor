<?php
   include_once 'RO_User.php';
   include_once 'globals.php';
   include_once 'emails.php';

   function verify_email_change($email, $email_new, $key)
   {
      $email = urlencode_email($email);
      $email_new = urlencode_email($email_new);
      $mysqli = connecti();
      $outputMessage = "We're sorry, there was an error in the request.";
      if (strlen($key) != 50)
      {
         error_log ('incorrect email reset key length');
         return $outputMessage;   
      }
      
      /* check if email_new is available */
      $query = sprintf("SELECT userID FROM Users WHERE email='%s'",
                  mysql_real_escape_string($email_new));
      if (!($result = $mysqli->query($query)))
      {
         error_log ('verify_email_change failed to check if email_new is available. '.$mysqli->error);
         return null;
      }
      if ($row = $result->fetch_assoc())
      {
         error_log ('new email address already exists');
         return $outputMessage;
      }

      $query = sprintf("SELECT userid, firstName FROM Users WHERE email='%s' AND key_email_reset='%s'",
                  mysql_real_escape_string($email),
                  mysql_real_escape_string($key));

      if (!($result = $mysqli->query($query))) 
      {
         error_log("failed to run query. ".$mysqli->error);
         return null;
      }

      if ($row = $result->fetch_assoc())
      {
         /* the email/key_email_reset matched, update email */
         $query = sprintf("UPDATE Users SET email='%s', key_email_reset=null WHERE userID='%d'",
                     mysql_real_escape_string($email_new),
                     mysql_real_escape_string($row['userid']));
         
         if (!($mysqli->query($query)))
         {
            error_log('verify_email_change, failed to update Users table.'.$mysqli->error);
            return null;
         }

         $outputMessage = "Your account has succesfully been updated. Old email: ".$email.". Updated email: ".$email_new.".";
         $pid = pcntl_fork();
         if ($pid == -1) 
         {   
            error_log("Verify_Email_Change: fork failed");
            return $outputMessage;
         }   
         else if ($pid)
         {   
            /* parent flow control continues */
            return $outputMessage;
         }   
         else /* child */
         {   
            /* send confirmation emails */
            $body = get_email_success_new_email($row['firstName'], $email_new);
            sendDropolyEmail($email, $row['firstName'], $body, "Dropoly - Your Account Has Been Modified");
            sendDropolyEmail($email_new, $row['firstName'], $body, "Dropoly.com - Your Account Has Been Modified");
         }
      }
   }
?>
