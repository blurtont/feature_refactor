<?php
include_once 'globals.php';

   class RO_Calculation_Result
   {  
      public $id_user;
      public $id_residence;
      public $time_stamp;
      public $unix_time_stamp;

      public $cost_total_per_sf;
      public $cost_heating_per_sf;
      public $cost_cooling_per_sf;
      public $cost_lighting_appliances_per_person;
      public $cost_water_heating_per_person;
      
      public $percentage_cost_savings_claimed_total;
      public $percentage_cost_savings_claimed_heating;
      public $percentage_cost_savings_claimed_cooling;
      public $percentage_cost_savings_claimed_lighting_appliances;
      public $percentage_cost_savings_claimed_water_heating;
      
      public $percentage_cost_savings_measured_total; //measured based on new utility data
      public $percentage_cost_savings_measured_heating;
      public $percentage_cost_savings_measured_cooling;
      public $percentage_cost_savings_measured_lighting_appliances;
      public $percentage_cost_savings_measured_water_heating;

      public static function insert(RO_Calculation_Result $value)
      {
         $mysqli = connecti();
         $query = sprintf( "INSERT INTO Calculation_Results 
                                   (id_user_Calculation_Results,
                                    id_residence_Calculation_Results,
                                    unix_time_stamp,
                                    cost_total_per_sf_x10000,
                                    cost_heating_per_sf_x10000,
                                    cost_cooling_per_sf_x10000,
                                    cost_lighting_appliances_per_person_x10000,
                                    cost_water_heating_per_person_x10000,
                                    percentage_cost_savings_claimed_total_x10000,
                                    percentage_cost_savings_claimed_heating_x10000,
                                    percentage_cost_savings_claimed_cooling_x10000,
                                    percentage_cost_savings_claimed_lighting_appliances_x10000,
                                    percentage_cost_savings_claimed_water_heating_x10000,
                                    percentage_cost_savings_measured_total_x10000,
                                    percentage_cost_savings_measured_heating_x10000,
                                    percentage_cost_savings_measured_cooling_x10000,
                                    percentage_cost_savings_measured_lighting_appliances_x10000,
                                    percentage_cost_savings_measured_water_heating_x10000)
                            VALUES ('%d', '%d', UNIX_TIMESTAMP(), '%d', '%d', '%d', 
                                    '%d', '%d', '%d', '%d', '%d', 
                                    '%d', '%d', '%d', '%d', '%d',
                                    '%d', '%d')
                            ON DUPLICATE KEY UPDATE id_user_Calculation_Results=id_user_Calculation_Results",
                           mysql_real_escape_string( $value->id_user ),
                           mysql_real_escape_string( $value->id_residence ),
                           mysql_real_escape_string( $value->cost_total_per_sf * 10000 ),
                           mysql_real_escape_string( $value->cost_heating_per_sf * 10000 ),
                           mysql_real_escape_string( $value->cost_cooling_per_sf * 10000 ),
                           mysql_real_escape_string( $value->cost_lighting_appliances_per_person * 10000 ),
                           mysql_real_escape_string( $value->cost_water_heating_per_person * 10000 ),
                           mysql_real_escape_string( $value->percentage_cost_savings_claimed_total * 10000 ),
                           mysql_real_escape_string( $value->percentage_cost_savings_claimed_heating * 10000 ),
                           mysql_real_escape_string( $value->percentage_cost_savings_claimed_cooling * 10000 ),
                           mysql_real_escape_string( $value->percentage_cost_savings_claimed_lighting_appliances * 10000 ),
                           mysql_real_escape_string( $value->percentage_cost_savings_claimed_water_heating * 10000 ),
                           mysql_real_escape_string( $value->percentage_cost_savings_measured_total * 10000 ),
                           mysql_real_escape_string( $value->percentage_cost_savings_measured_heating * 10000 ),
                           mysql_real_escape_string( $value->percentage_cost_savings_measured_cooling * 10000 ),
                           mysql_real_escape_string( $value->percentage_cost_savings_measured_lighting_appliances * 10000 ),
                           mysql_real_escape_string( $value->percentage_cost_savings_measured_water_heating * 10000 ) );

         if (!$mysqli->query($query))
         {
            error_log("failed to insert calculation result. " . $query . $mysqli->error);
            return false;
         }
         return true;
      }

      public static function select($id_residence)
      {
         error_log('RO_Calculation_Result::select('.$id_residence.') called');
         $mysqli = connecti();
         $query = sprintf("SELECT * FROM Calculation_Results
                           WHERE id_residence_Calculation_Results='%d'
                           ORDER BY unix_time_stamp DESC LIMIT 1",
                           mysql_real_escape_string($id_residence));
         
         if (($result = $mysqli->query($query)) && ($a_row = $result->fetch_assoc()))
         {
            $new = new RO_Calculation_Result();
            RO_Calculation_Result::copy_to_new($new, $a_row);
            return $new;
         }
         else
         {
            error_log('failed to select calculation result for id_residence ' . $id_residence . $mysqli->error . $query);
         }
         return null;
      }

      /* TODO: actually select by zip code, comparing all zips for now */
      public static function select_worst($zip_code)
      {
         $query = sprintf("SELECT * FROM Calculation_Results
                           ORDER BY cost_total_per_sf_x10000 DESC
                           LIMIT 1");
         $result = mysql_query($query) or die("RO_Calculation_Result: failed to select_worst.".mysql_error());
         $new = new RO_Calculation_Result();
         if($a_row = mysql_fetch_assoc($result))
         {
            RO_Calculation_Result::copy_to_new($new, $a_row);
         }
         return $new;
      }

      /* TODO: actually select by zip code, comparing all zips for now */
      public static function select_best($zip_code)
      {
         $query = sprintf("SELECT * FROM Calculation_Results
                           ORDER BY cost_total_per_sf_x10000 LIMIT 1");
         $result = mysql_query($query) or die("RO_Calculation_Result: failed to select_best.".mysql_error());
         $new = new RO_Calculation_Result();
         if($a_row = mysql_fetch_assoc($result))
         {
            RO_Calculation_Result::copy_to_new($new, $a_row);
         }
         return $new;
      }

      /* TODO: actually select by zip code, comparing all zips for now */
      public static function select_average($zip_code)
      {
         $query = "SELECT COUNT(DISTINCT id_residence_Calculation_Results) FROM Calculation_Results";
         $result = mysql_query($query) or die ("RO_Calculation_Result: failed to select_average, count distinct. ".mysql_error());
         $total_residences = 1;
         if ($a_row = mysql_fetch_array($result))
         {
            $total_residences = $a_row[0];
         }

         $limit = floor($total_residences/2);

         $query = sprintf("SELECT * FROM Calculation_Results
                           ORDER BY cost_total_per_sf_x10000 
                           LIMIT %d, 1", 
                           mysql_real_escape_string($limit));

         $result = mysql_query($query) or die("RO_Calculation_Result: failed to select_average.".mysql_error());
         $new = new RO_Calculation_Result();
         if($a_row = mysql_fetch_assoc($result))
         {
            RO_Calculation_Result::copy_to_new($new, $a_row);
         }
         return $new;
      }

      public static function copy_to_new( &$new, $a_row )
      {
         $new->id_user = (int) $a_row['id_user_Calculation_Results'];
         $new->id_residence = (int) $a_row['id_residence_Calculation_Results'];
         $new->unix_time_stamp = (int) $a_row['unix_time_stamp'];
         
         $new->cost_total_per_sf = (0.0001) * $a_row['cost_total_per_sf_x10000'];
         $new->cost_heating_per_sf = (0.0001) * $a_row['cost_heating_per_sf_x10000'];
         $new->cost_cooling_per_sf = (0.0001) * $a_row['cost_cooling_per_sf_x10000'];
         $new->cost_lighting_appliances_per_person = (0.0001) * $a_row['cost_lighting_appliances_per_person_x10000'];
         $new->cost_water_heating_per_person = (0.0001) * $a_row['cost_water_heating_per_person_x10000'];

         $new->percentage_cost_savings_claimed_total = (0.0001) * $a_row['percentage_cost_savings_claimed_total_x10000'];
         $new->percentage_cost_savings_claimed_heating = (0.0001) * $a_row['percentage_cost_savings_claimed_heating_x10000'];
         $new->percentage_cost_savings_claimed_cooling = (0.0001) * $a_row['percentage_cost_savings_claimed_cooling_x10000'];
         $new->percentage_cost_savings_claimed_lighting_appliances = (0.0001) * $a_row['percentage_cost_savings_claimed_lighting_appliances_x10000'];
         $new->percentage_cost_savings_claimed_water_heating = (0.0001) * $a_row['percentage_cost_savings_claimed_water_heating_x10000'];

         $new->percentage_cost_savings_measured_total = (0.0001) * $a_row['percentage_cost_savings_measured_total_x10000'];
         $new->percentage_cost_savings_measured_heating = (0.0001) * $a_row['percentage_cost_savings_measured_heating_x10000'];
         $new->percentage_cost_savings_measured_cooling = (0.0001) *$a_row['percentage_cost_savings_measured_cooling_x10000'];
         $new->percentage_cost_savings_measured_lighting_appliances = (0.0001) * $a_row['percentage_cost_savings_measured_lighting_appliances_x10000'];
         $new->percentage_cost_savings_measured_water_heating = (0.0001) * $a_row['percentage_cost_savings_measured_water_heating_x10000']; 
      }
   }
?>
