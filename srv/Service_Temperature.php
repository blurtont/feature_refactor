<?php
include_once 'RO_Temperature_Datapoint.php';
include_once 'RO_Request_Temperature.php';
include_once 'globals.php';

class Service_Temperature
{
   public function get_temperatures( RO_Request_Temperature $request )
   {
      $mysqli = connecti_weather2( ); /* globals.php */

      /* get station ids from zip code */

      $query_get_station_ids = sprintf( "SELECT * FROM Zip_Codes
                                         WHERE zip_code = '%d'",
                                        $request->zip );

      if ( !( $result = $mysqli->query( $query_get_station_ids ) ) )
      {
         error_log( "Failed to run query: " . $query_get_station_ids . "\n" . $mysqli->error( ) );
         return null;
      }

      if ( $row = $result->fetch_assoc( ) )
      {
         $id_usaf = $row[ 'id_usaf_Zip_Codes' ];
         $id_wban = $row[ 'id_wban_Zip_Codes' ];
      }
      else
      {
         error_log( "Failed to lookup zip code " . $request->zip . "\n" );
      }

      /* get temperature data*/
      $query_temperatures = sprintf( "SELECT unix_time, temp_air_celsius
                                      FROM Weather_Hourly
                                      WHERE id_usaf = '%d'
                                      AND id_wban = '%d'
                                      AND unix_time >= '%d' AND unix_time <= '%d'",
                                     mysql_real_escape_string( $id_usaf ),
                                     mysql_real_escape_string( $id_wban ),
                                     mysql_real_escape_string( $request->unix_time_start ),
                                     mysql_real_escape_string( $request->unix_time_end ) );

      if ( !( $result = $mysqli->query( $query_temperatures ) ) )
      {
         error_log( "Failed to run query: " . $query_temperatures . "\n" . $mysqli->error );
         return null;
      }

      $request->results = array( );
      while ( $row = $result->fetch_array( MYSQLI_NUM ) )
      {
         $datapoint = new RO_Temperature_Datapoint( );
         $datapoint->unix_time = $row[ 0 ];
         $datapoint->temp_air_celsius = $row[ 1 ];
         array_push( $request->results, $datapoint );
      }

      return $request;
   }

}
      
?>
