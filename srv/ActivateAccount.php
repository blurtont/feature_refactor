<?php
   include_once 'globals.php';
   include_once 'emails.php';
   include_once 'Interaction.php';
   include_once 'Type_Interaction.php';

   function activateAccount($emailAddress, $key)
   {
      connect();

      $outputMessage = "";

      $query = sprintf("SELECT email, firstName, lastName, emailVerified, accountActivated, userID FROM Users WHERE email='%s' AND activateAccountKey='%s'",
                  mysql_real_escape_string($emailAddress),
                  mysql_real_escape_string($key));
      $result = mysql_query($query) or die("failed to run query. ".mysql_error());

      if ($row = mysql_fetch_assoc($result))
      {
         /* the email/key pair has been matchedd */
         if ($row['accountActivated'])
         {
            $outputMessage .= "The email address '".$emailAddress."' is already activated. ";
         }
         else
         {
            /* set the emailActivated flag to true */
            $query = sprintf("UPDATE Users SET accountActivated=1 WHERE email='%s' AND activateAccountKey='%s'",
                        mysql_real_escape_string($emailAddress),
                        mysql_real_escape_string($key));
            mysql_query($query) or die('failed to run query. '.mysql_error());
            $outputMessage .= $emailAddress." has been activated. ";

            /* send activation email to user */

            $body = getEmailAccountActive($row['firstName']);
            sendDropolyEmail("info@dropoly.com", $emailAddress, $row['firstName'], $body, "Dropoly.com - Your Account Has Been Activated!");
            /* do we want to send email to admins? prob not. */
            $interaction = new Interaction();
            $interaction->id_user = $row['userID'];
            $interaction->id_type_interaction = Type_Interaction::ACCOUNT_VALIDATE_EMAIL;
            $interaction->insert();         
         }
         
         if ($row['emailVerified'])
         {
            $outputMessage .= $row['firstName']." has verified his/her email address. ".$row['firstName'].' '.$row['lastName']."'s account is now fully activated and verified. ";
         }
         else
         {
            $outputMessage .= $row['firstName'].' '.$row['lastName']."  has yet to verify his/her email address. ";
         }

      }
      else
      {
         error_log("failed to activate user, could not find email/key pair: ".$query);
         $outputMessage .= "Failed to lookup email/key pair. ".$query;
      }
      
      /* TODO: this should be a nice lookin dropoly html page */
      echo $outputMessage;
   }
?>
