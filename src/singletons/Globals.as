package singletons
{
	import com.google.analytics.AnalyticsTracker;
	
	import main.EnergyModel;
	
	public class Globals
	{
		public static var gui_top:Dropoly;
		public static var tracker:AnalyticsTracker;
	}
}