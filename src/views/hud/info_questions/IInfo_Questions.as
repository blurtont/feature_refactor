package views.hud.info_questions
{
	import mx.core.IVisualElement;

	public interface IInfo_Questions extends IVisualElement
	{
		function handler_save_info():void;
	}
}