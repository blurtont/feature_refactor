<?xml version="1.0" encoding="utf-8"?>
<ns:Info_Questions_Device xmlns:fx="http://ns.adobe.com/mxml/2009"
						  xmlns:s="library://ns.adobe.com/flex/spark"
						  xmlns:info_questions="info_questions.*"
						  xmlns:components="assets.components.*" 
						  xmlns:mx="library://ns.adobe.com/flex/mx"
						  xmlns:ns="views.hud.info_questions.*"
						  stateChangeComplete="update();" 
						  show="update();">
	
	<fx:Style source="assets/css/Info_Questions.css"/>
	
	<fx:Script>
		<![CDATA[
			
			import mx.controls.Alert;
			
			import ros.RO_Room_Components.RO_Device;
			import ros.RO_Room_Components.TLDs.RO_TLD_Cooling_System;
			
			import singletons.Lang;
			
			import spark.components.Group;
			
			import types.Type_AC_Central;
			
			private const lookup_SEER_values_standard:Vector.<Number> = 
				Vector.<Number>([6, 7, 8, 8.5, 9, 10, 12, 13]);
			
			private const lookup_SEER_values_energy_star:Vector.<Number> =  
				Vector.<Number>([6, 7, 8, 8.5, 9, 12, 13.5, 14.5]);
			
			private const lookup_display_select_age:Array = 
				[	"Before 1980", "1980-1985", "1985-1990", "1990-1995", 
					"1995-2000", "2000-2008", "2008-2011", "After 2011"];			
			
			private var _seer:Number;
			
			override public function set ro_device(rtld:RO_Device):void
			{
				super.ro_device = rtld;
				var ro_device:RO_TLD_Cooling_System = RO_TLD_Cooling_System(_ro_device);
				
				radio_group_ac_type.selectedValue = ro_device.type_ac;
				seer = ro_device.seer;
				
				numericstepper_fraction_house_served.value = 100 * ro_device.fraction;
				
				update();
			}
			
			override public function handler_save_info():void
			{
				var ro_device:RO_TLD_Cooling_System = RO_TLD_Cooling_System(_ro_device);
				
				ro_device.type_ac = int(radio_group_ac_type.selectedValue);
				ro_device.seer = _seer;
				ro_device.fraction = 0.01 * numericstepper_fraction_house_served.value;
				
				super.handler_save_info();
			}
			
			protected function update():void
			{
				group_select_efficiency.removeAllElements();
				group_select_efficiency.addElement(group_toggle_know_efficiency);
				
				var group_age:Group, group_SEER:Group;
				
				var type_ac:int = (radio_group_ac_type.selectedValue != null) ? int(radio_group_ac_type.selectedValue) : RO_TLD_Cooling_System(_ro_device).type_ac; 
				
				switch(type_ac)
				{
					case(Type_AC_Central.GROUND_SOURCE_HEAT_PUMP):
						group_age = group_select_age_gshp;
						break;
					case(Type_AC_Central.AIR_CONDITIONER):
					case(Type_AC_Central.AIR_SOURCE_HEAT_PUMP):
					default:
						group_age = group_select_age;
						break;
				}
				
				group_SEER = group_select_SEER;
				
				if(toggle_know_efficiency.selected)
				{
					group_select_efficiency.addElement(group_SEER);
				}
				else
				{
					group_select_efficiency.addElement(group_age);
					group_select_efficiency.addElement(group_select_energy_star);
				}
			}
			
			internal function set seer(value_new:Number):void
			{
				var lookup_SEER:Vector.<Number> = currentState == 'energy_star' ? 
					lookup_SEER_values_energy_star : lookup_SEER_values_standard;
				
				var check_is_in_range:Function = 
					function(value:Number, index:int, array:Vector.<Number>):Boolean{
						if(value <= value_new && (index+1 == array.length || array[index+1] > value_new))
						{
							input_age.selectedValue = index;
							return true;
						}
						return false;
					};
				
				if(!lookup_SEER.some(check_is_in_range))
					input_age.selectedValue = input_age.minimum;
				
				radio_group_age_gshp.selectedValue =
					_seer = 
					input_SEER.value = 
					value_new;
			}
			
			protected function handler_change_stepper(event:Event):void
			{
				var lookup_SEER:Vector.<Number> = currentState == 'energy_star' ? 
					lookup_SEER_values_energy_star : lookup_SEER_values_standard;
				
				if(input_age.selectedValue < lookup_SEER.length) 
					seer = lookup_SEER[input_age.selectedValue];
			}
			
		]]>
	</fx:Script>
	
	<fx:Declarations>
		<s:RadioButtonGroup id="radio_group_ac_type" change="update()"/>
		<s:RadioButtonGroup id="radio_group_age_gshp" 
							change="seer = Number(radio_group_age_gshp.selectedValue);"/>
		
		<s:HGroup id="group_toggle_know_efficiency" gap="20" verticalAlign="middle">
			<s:Label text="I know the efficiency of my cooling system"
					 styleName="prompt_info_questions" width="100%"/>
			<s:ToggleButton id="toggle_know_efficiency" buttonMode="true" change="update()"
							skinClass="assets.skins.hud.Skin_ToggleButton_Check"/>
		</s:HGroup>
		
		<s:VGroup id="group_select_SEER" horizontalAlign="center" width="100%" gap="5">
			<s:Line stroke="{stroke_info_questions}" width="100%"/>
			<s:HGroup width="100%" verticalAlign="middle" gap="5">
				<s:Label text="What is the SEER value of your cooling system?"
						 styleName="prompt_info_questions" width="100%"/>
				<s:NumericStepper id="input_SEER" minimum="6" maximum="25"
								  change="seer = input_SEER.value;"
								  styleName="editable_info_questions" width="75"/>
				<s:Spacer width="{label_percent.width}"/>
			</s:HGroup>
		</s:VGroup>
		
		<s:VGroup id="group_select_age" horizontalAlign="center" width="100%" gap="5">
			<s:Line stroke="{stroke_info_questions}" width="100%"/>
			<s:HGroup width="100%" verticalAlign="middle" gap="5">
				<s:Label text="When was your cooling system manufactured?"
						 styleName="prompt_info_questions" width="100%"/>
				<components:AssociativeStepper id="input_age" width="120" displayTextSource="{lookup_display_select_age}" change="handler_change_stepper(event)" />
				<s:Spacer width="{label_percent.width}"/>
			</s:HGroup>
		</s:VGroup>
		
		<s:VGroup id="group_select_age_gshp" horizontalAlign="center" width="100%" gap="5">
			<s:Line stroke="{stroke_info_questions}" width="100%"/>
			<s:Label text="When was your ground-source heat pump manufactured?"
					 styleName="prompt_info_questions" width="100%"/>
			<s:HGroup gap="20">
				<s:RadioButton label="Before 2000" width="130" 
							   value="12" group="{radio_group_age_gshp}"
							   skinClass="assets.skins.generic.radio_buttons.Skin_RadioButton_Pill_Green" buttonMode="true"/>
				<s:RadioButton label="After 2000" width="130" 
							   value.standard="15" value.energy_star="22" group="{radio_group_age_gshp}"
							   skinClass="assets.skins.generic.radio_buttons.Skin_RadioButton_Pill_Green" buttonMode="true"/>
			</s:HGroup>
		</s:VGroup>
		
		<s:VGroup id="group_select_energy_star" width="100%" gap="5">
			<s:Line stroke="{stroke_info_questions}" width="100%"/>
			<s:HGroup verticalAlign="middle" width="100%" gap="20">
				<s:Label text="Was your unit Energy Star rated at purchase?"
						 styleName="prompt_info_questions" width="100%"/>
				<s:RadioButton label="Yes" buttonMode="true" width="120" click="currentState='energy_star';"
							   selected.energy_star="true" selected.standard="false"
							   skinClass="assets.skins.generic.radio_buttons.Skin_RadioButton_Pill_Green"/>
				<s:RadioButton label="No" buttonMode="true" width="120" click="currentState='standard';"
							   selected.energy_star="false" selected.standard="true"
							   skinClass="assets.skins.generic.radio_buttons.Skin_RadioButton_Pill_Green"/>
				<s:Spacer width="{label_percent.width - 20 + 5}"/>
			</s:HGroup>
		</s:VGroup>
		
	</fx:Declarations>
	
	<ns:states>
		<s:State name="standard"/>
		<s:State name="energy_star"/>
	</ns:states>
	
	<s:VGroup gap="5"  width="100%" horizontalAlign="center">
		<s:Label text="{Lang.xml_external.rooms.devices.whole_house_cooling.info_questions.type_ac.copy_prompt}"
				 styleName="prompt_info_questions" width="100%"/>
		<s:RadioButton label="Air Conditioner" width="220"
					   group="{radio_group_ac_type}" value="{Type_AC_Central.AIR_CONDITIONER}"
					   skinClass="assets.skins.generic.radio_buttons.Skin_RadioButton_Pill_Green" buttonMode="true"/>
		<s:RadioButton label="Air Source Heat Pump" width="220"
					   group="{radio_group_ac_type}" value="{Type_AC_Central.AIR_SOURCE_HEAT_PUMP}"
					   skinClass="assets.skins.generic.radio_buttons.Skin_RadioButton_Pill_Green" buttonMode="true"/>
		<s:RadioButton label="Ground Source Heat Pump" width="220"
					   group="{radio_group_ac_type}" value="{Type_AC_Central.GROUND_SOURCE_HEAT_PUMP}"
					   skinClass="assets.skins.generic.radio_buttons.Skin_RadioButton_Pill_Green" buttonMode="true"/>
	</s:VGroup>
	
	<s:Line width="100%" stroke="{stroke_info_questions}"/>
	
	<s:VGroup id="group_select_efficiency" gap="5" width="100%" horizontalAlign="center"/>
	
	<s:Line width="100%" stroke="{stroke_info_questions}"/>
	
	<s:HGroup gap="5" width="100%" verticalAlign="middle"> 
		<s:Label text="{Lang.xml_external.rooms.devices.whole_house_heating.info_questions.fraction.copy_prompt}"
				 styleName="prompt_info_questions" width="100%"/>
		<s:NumericStepper id="numericstepper_fraction_house_served" 
						  minimum="0" maximum="100" stepSize="10"
						  styleName="editable_info_questions" width="75"/>
		<s:Label id="label_percent" text="%" styleName="prompt_info_questions" paddingLeft="0"/>
	</s:HGroup>
	
</ns:Info_Questions_Device>
