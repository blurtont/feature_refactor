package views.hud.hud_alerts
{
	import mx.core.IVisualElement;

	public interface IHUD_Alert extends IVisualElement
	{
		function get label_button_back():String;
		function function_back():void;
		function get label_button_continue():String;
		function function_continue():void;
	}
}