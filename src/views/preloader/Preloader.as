package views.preloader
{
	import com.greensock.TweenMax;
	
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Scene;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.events.TimerEvent;
	import flash.net.URLRequest;
	import flash.utils.Timer;
	
	import mx.events.FlexEvent;
	import mx.flash.UIMovieClip;
	import mx.preloaders.IPreloaderDisplay;

	public class Preloader extends Sprite implements IPreloaderDisplay
	{		
		
		private var dpb_image_control:flash.display.Loader;
		private var main_movie:MovieClip;
		private var progress_bar:MovieClip;
		private var progress_frame:int;
		
		public function Preloader()
		{
			super();
		}
		
		public function set preloader(preloader:Sprite):void
		{
			preloader.addEventListener(ProgressEvent.PROGRESS, handle_progress);
			preloader.addEventListener(Event.COMPLETE, handle_complete);
			preloader.addEventListener(FlexEvent.INIT_PROGRESS, handle_init_progress);
			preloader.addEventListener(FlexEvent.INIT_COMPLETE, handle_init_complete);
		}
		
		public function initialize():void
		{
			dpb_image_control = new flash.display.Loader();
			dpb_image_control.contentLoaderInfo.addEventListener(Event.COMPLETE, loader_complete_handler);
			dpb_image_control.load(new URLRequest("assets/swfs/preloader/loader.swf"));
		}
		
		private function loader_complete_handler(event:Event):void
		{
			main_movie = event.target.content as MovieClip;
			addChild(main_movie);
			progress_bar = MovieClip(main_movie.getChildAt(1));
			progress_bar.addEventListener(Event.EXIT_FRAME, frame_entered);
		}
		
		private function handle_progress(event:ProgressEvent):void {
			if(progress_bar != null) 
			{
				progress_frame = progress_bar.totalFrames * (event.bytesLoaded / event.bytesTotal);
	
				if(progress_frame == progress_bar.totalFrames) {
					TweenMax.to(progress_bar, 0.5, {frame:progress_bar.totalFrames});
				}
				else
				{
					progress_bar.play();
				}
			}
		}
		
		private function handle_complete(event:Event):void {}
		
		private function handle_init_progress(event:Event):void {}
		
		private function handle_init_complete(event:Event):void {
			dispatch_complete();
		}
		
		private function frame_entered(event:Event):void
		{
			if(progress_bar.currentFrame >= progress_frame)
			{
				progress_bar.stop();
			}
		}
		
		private function dispatch_complete():void
		{
			dispatchEvent(new Event(Event.COMPLETE));
		}
		
		public function get backgroundColor():uint {
			return 0;
		}
		
		public function set backgroundColor(value:uint):void {
		}
		
		public function get backgroundAlpha():Number {
			return 0;
		}
		
		public function set backgroundAlpha(value:Number):void {
		}
		
		public function get backgroundImage():Object {
			return this;
		}
		
		public function set backgroundImage(value:Object):void {
		}
		
		public function get backgroundSize():String {
			return "";
		}
		
		public function set backgroundSize(value:String):void {
		}
		
		public function get stageWidth():Number {
			return 200;
		}
		
		public function set stageWidth(value:Number):void {
		}
		
		public function get stageHeight():Number {
			return 200;
		}
		
		public function set stageHeight(value:Number):void {
		}
	}
}