package views.tutorial
{
	public interface IOverlay_Tutorial
	{
		function get is_display_tutorial():Boolean;
		function display_tutorial():void
	}
}