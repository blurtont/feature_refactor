package proxy_library
{
	import globals.Globals_Library;
	
	import mx.managers.CursorManager;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import ros.RO_Room_Components.RO_Device;
	
	import services.Service_Device;
	
	import singletons.Globals;
	import singletons.RO_State_Global;

	public class Proxy_Service_Device
	{
		public static var new_ro_device:RO_Device;
		
		public static function update_rtld(ro_device:RO_Device, type_room:int):void
		{
			Service_Device.update_rtld(ro_device, type_room, handler_responder_update_rtld, handler_fault);
		}
		
		public static function remove(ro_device:RO_Device):void
		{
			CursorManager.setBusyCursor();
			Service_Device.remove(ro_device, handler_responder_remove, handler_fault);
		}
		
		public static function create(ro_device:RO_Device, id_room:int):void
		{
			CursorManager.setBusyCursor();
			var handler_responder:Function = function(e:ResultEvent):void{
				CursorManager.removeBusyCursor();
				ro_device.copy_props(RO_Device(e.result));
				ro_device.invalidated = true;
				Globals_Library.energy_model.init_residence(RO_State_Global.residence);
				Globals.gui_top.view_hud.hud_progress.update_keys_counts_decrement_only(RO_State_Global.residence);
//				trace('created device successfully');
			};
			Service_Device.create(ro_device, id_room, handler_responder, handler_fault);
		}
		
		protected static function handler_responder_update_rtld(event:ResultEvent):void
		{
//			trace("updated device successfully.");
		}
		
		protected static function handler_responder_remove(e:ResultEvent):void
		{
//			trace('removed device successfully');
			CursorManager.removeBusyCursor();
			
			Globals_Library.energy_model.init_residence(RO_State_Global.residence);
 			Globals.gui_top.view_hud.hud_progress.update_keys_counts(RO_State_Global.residence);
		}
		
		protected static function handler_fault(event:FaultEvent):void
		{
			CursorManager.removeBusyCursor();
			Proxy_Service_Device.new_ro_device = null;
			trace("ERROR: failed to update_device.");
			trace(event.message);
		}
	}
}