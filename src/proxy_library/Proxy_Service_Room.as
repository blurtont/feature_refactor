package proxy_library
{
	import mx.rpc.CallResponder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	import ros.RO_Room;
	
	import services.Service_Room;

	public class Proxy_Service_Room
	{
		public static function update(ro_room:RO_Room):void
		{
			Service_Room.update(ro_room);
			/*Globals.tracker.trackEvent(Type_Event_Tracking_Categories.HOME_NAV,
			Type_Event_Tracking_Actions.ADD,
			Type_Room.get_default_room_name(type_room),
			RO_State_Global.user.userID);*/
		}
		
		public static function update_shallow(ro_room:RO_Room):void
		{
			Service_Room.update_shallow(ro_room);
			/*Globals.tracker.trackEvent(Type_Event_Tracking_Categories.HOME_NAV,
			Type_Event_Tracking_Actions.ADD,
			Type_Room.get_default_room_name(type_room),
			RO_State_Global.user.userID);*/
		}
		
		public static function update_shallow_multiple(ro_rooms:Array):void
		{
			Service_Room.update_shallow_multiple(ro_rooms);
		}
	}
}