package proxy_library
{
	import events.types.Type_Event_Tracking_Actions;
	import events.types.Type_Event_Tracking_Categories;
	import events.types.Type_Event_Tracking_Labels;
	
	import services.Service_Zip_Code;
	
	import singletons.Globals;
	import singletons.RO_State_Global;

	public class Proxy_Service_Zip_Code
	{
		public static function is_valid_zip_code(zip_code:int, call_later:Function):void
		{
			Service_Zip_Code.is_valid_zip_code(zip_code, call_later)
			Globals.tracker.trackEvent(Type_Event_Tracking_Categories.QUESTIONNAIRE,
			Type_Event_Tracking_Actions.VERIFY,
			Type_Event_Tracking_Labels.ZIP,
			RO_State_Global.user.userID);
		}
	}
}