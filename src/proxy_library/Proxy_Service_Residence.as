package proxy_library
{
	import flash.events.Event;
	
	import globals.Globals_Library;
	
	import main.energyModels.UtilityCostsModel;
	
	import mx.managers.CursorManager;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import ros.RO_Residence;
	import ros.RO_Room;
	
	import services.Service_Residence;
	
	import singletons.Globals;
	import singletons.RO_State_Global;

	public class Proxy_Service_Residence
	{
		public static function add_room(type_room:int, handler_responder_add_room:Function):void
		{
			CursorManager.setBusyCursor();
			Service_Residence.callback_result = handler_responder_add_room;
			Service_Residence.callback_fault = function(e:FaultEvent):void{
				CursorManager.removeBusyCursor();
				trace("ERROR: failed to add room");
				trace(e.message);
			};
			Service_Residence.add_room(type_room);
		}
		
		public static function load_residence():void
		{
			Service_Residence.callback_result = handler_responder_load_residence;
			Service_Residence.callback_fault = function(event:FaultEvent):void{
				trace("ERROR: failed to load_residence.");
				trace(event.message);
			};
			Service_Residence.load_residence();
		}
		
		public static function remove_room(ro_room:RO_Room):void
		{
			Service_Residence.callback_result = handler_responder_remove_room;
			Service_Residence.callback_fault = function(e:FaultEvent):void{
				trace("ERROR: failed to remove room");
				trace(e.message);
			};
			Service_Residence.remove_room(ro_room);
		}
		
		public static function update(ro_residence:RO_Residence):void
		{
			Service_Residence.callback_result = handler_responder_update;
			Service_Residence.callback_fault = handler_fault_update;
			Service_Residence.update(ro_residence);
		}
		
		public static function update_shallow(ro_residence:RO_Residence):void
		{
			Service_Residence.callback_result = handler_responder_update;
			Service_Residence.callback_fault = handler_fault_update;
			Service_Residence.update_shallow(ro_residence);
		}
		
		public static function view_room(ro_room:RO_Room):void
		{
			Service_Residence.callback_result = function(e:ResultEvent):void{
//				trace("room view recorded successfully");
			};
			Service_Residence.callback_fault = function(e:FaultEvent):void{
				trace("ERROR: failed to record room view");
				trace(e.message);
			};
				
			Service_Residence.view_room(ro_room);
		}
		
		protected static function handler_responder_load_residence(event:ResultEvent):void
		{
			Service_Residence.callback_result = null;
			Service_Residence.callback_fault = null;
			//TODO: this will eventually do something different, 
			//i.e. load one of multiple residences, then do something...
			//or something... 
			var ro_residence:RO_Residence = event.result as RO_Residence;
			RO_State_Global.residence = ro_residence;
			ro_residence.set_room_names_in_actions();
			
			/* HAX pull utility data out into static references */
			UtilityCostsModel.static_cost_per_ccf = ro_residence.ro_region.cost_per_ccf;
			UtilityCostsModel.static_cost_per_kWh = ro_residence.ro_region.cost_per_kWh;
		}
		
		/*protected static function handler_responder_add_room(e:ResultEvent):void
		{
			Service_Residence.callback_result = null;
			Service_Residence.callback_fault = null;
			CursorManager.removeBusyCursor();
			
			var ro_room:RO_Room = e.result as RO_Room;
			if(RO_State_Global.residence != null)
			{	
				RO_State_Global.residence.ro_rooms.push(ro_room);
				Globals_Library.energy_model.model_residence.create_room(ro_room);
				Globals.gui_top.view_home_navigator.update_icons();
				Globals.gui_top.view_hud.hud_progress.update_keys_counts(RO_State_Global.residence);
			}
		}*/
		
		protected static function handler_responder_remove_room(e:ResultEvent):void
		{
			Service_Residence.callback_result = null;
			Service_Residence.callback_fault = null;
			CursorManager.removeBusyCursor();

			var id_room:int = e.result as int;
			if( RO_State_Global.residence != null )
			{	
				var ro_rooms:Array = RO_State_Global.residence.ro_rooms;
				for each ( var ro_room:RO_Room in ro_rooms )
				{
					if ( ro_room.id_room == id_room )
					{
						ro_rooms.splice( ro_rooms.indexOf( ro_room ), 1 );
						Globals_Library.energy_model.model_residence.remove_room( ro_room );
						Globals.gui_top.dispatchEvent( new Event( Dropoly.EVENT_UPDATE ) );
						break;
					}
				}
				Globals.gui_top.view_hud.hud_progress.update_keys_counts(RO_State_Global.residence);
			}
		}
		
		protected static function handler_responder_update(e:ResultEvent):void
		{
			CursorManager.removeBusyCursor();
//			trace("residence updated successfully");
		}
		
		protected static function handler_fault_update(e:FaultEvent):void
		{
			CursorManager.removeBusyCursor();
			trace("ERROR: faled to update residence");
			trace(e.message);
		}
		
		
	}
}
