package proxy_library
{
	import flash.events.Event;
	import flash.net.SharedObject;
	
	import mx.managers.CursorManager;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import consts.Const;
	
	import events.StringMessageEvent;
	import events.types.Type_Event_Tracking_Actions;
	import events.types.Type_Event_Tracking_Categories;
	import events.types.Type_Event_Tracking_Labels;
	
	import ros.RO_User;
	
	import services.Service_State;
	import services.Service_User;
	
	import singletons.Globals;
	import singletons.Lang;
	
	import types.Type_Service_Error_User;
	
	import views.login.LoginCardComponent;

	public class Proxy_Service_User
	{
		public static var call_later_update_email:Function;
		public static var login_card:LoginCardComponent;
		
		public static function login_or_create_with_facebook(ro_user:RO_User, facebook_dump:String):void
		{
			Service_User.login_or_create_with_facebook(ro_user, facebook_dump, loginResponder_resultHandler, ro_faultHandler);
		}
		
		public static function create(ro_user:RO_User):void
		{
			Service_User.create(ro_user, createResponder_resultHandler, ro_faultHandler);
		}
		
		public static function email_send_welcome(ro_user:RO_User):void
		{
			Service_User.email_send_welcome(ro_user, handler_responder_email_send_welcome, ro_faultHandler);
		}
		
		public static function email_send_verify(ro_user:RO_User):void
		{
			Service_User.email_send_verify(ro_user, handler_responder_email_send_verify, ro_faultHandler);
		}
		
		public static function login(ro_user:RO_User):void
		{
			Service_User.login(ro_user, loginResponder_resultHandler, ro_faultHandler);
			Globals.tracker.trackEvent(Type_Event_Tracking_Categories.DROPOLY,
				Type_Event_Tracking_Actions.LOAD,
				Type_Event_Tracking_Labels.RETURN_USER);
		}
		
		public static function login_no_pass(ro_user:RO_User):void
		{
			Service_User.login_no_pass(ro_user, loginResponder_resultHandler, handler_fault);
//			trace('Service_User.login_no_pass(ro_user)');
			CursorManager.setBusyCursor();
			login_card.dispatchEvent(new Event('event_attempt_login', true));
			Globals.tracker.trackEvent(Type_Event_Tracking_Categories.DROPOLY,
				Type_Event_Tracking_Actions.LOAD,
				Type_Event_Tracking_Labels.RETURN_USER);
		}
		
		public static function password_reset(ro_user:RO_User, reset_code:String):void
		{
			Service_User.password_reset(ro_user, reset_code, resetPasswordResponder_resultHander, handler_fault);
		}
		
		public static function send_password_reset_email(ro_user:RO_User):void
		{
			Service_User.send_password_reset_email(ro_user, sendResetPassEmailResponder_resultHander, handler_fault);
		}
		
		public static function update(ro_user:RO_User):void
		{
			Service_User.update(ro_user, handler_responder_update_user, handler_fault);
		}
		
		public static function update_new_email(ro_user:RO_User, email_address_new:String, call_later:Function):void
		{
			Service_User.update_new_email(ro_user, email_address_new, call_later, handler_responder_update_new_email, handler_fault);
		}
		
		public static function update_password(ro_user:RO_User, password_old:String, password_new:String, handler_responder_update_password:Function):void
		{			
			Service_User.update_password(ro_user, password_old, password_new, handler_responder_update_password, handler_fault);
		}
		
		public static function remove_account(ro_user:RO_User, handler_responder_account_delete:Function):void
		{
			Service_User.remove_account(ro_user, handler_responder_account_delete, handler_fault);
		}
		
		protected static function loginResponder_resultHandler(event:ResultEvent):void
		{
			var userMember:RO_User = event.result as RO_User;
			if (userMember.errorType == Type_Service_Error_User.NONE) /* login successful */
			{
				login_card.returnPasswordTextInput.text = "";
				Service_State.ro_user = userMember;
				login_card.dispatchEvent(new StringMessageEvent("birtTalkEvent", true, false, "itWorkedMessage"));
				
				var shared_object:SharedObject = SharedObject.getLocal(Const.SERVER_NAME);
				if (login_card.check_box_stay_logged_in.selected)
				{
					shared_object.data.id_user = userMember.userID;
				}
				else
				{
					shared_object.data.id_user = null;
				}
				
				login_card.dispatchEvent(new Event('event_login_successful', true));
			}
			else
			{
				trace('ERROR: Proxy_Service_User: failed to login. ',userMember.errorType, userMember.errorMessage);
				CursorManager.removeBusyCursor();
				switch(userMember.errorType)
				{
					case Type_Service_Error_User.LOGIN_ACCOUNT_NOT_ACTIVATED:
					{
						login_card.problemTextText = Type_Service_Error_User.GET_TEXT(userMember.errorType);
						//						resendVerifyEmailButton.visible = false;
						login_card.currentState = "returnLoginIssue";
						login_card.dispatchEvent(new StringMessageEvent("birtTalkEvent", true, false, "errorMessage"));
						//						login_card.dispatchEvent(new Event("setStateCreateLogin"));
						break;
					}
					case Type_Service_Error_User.LOGIN_EMAIL_ACCOUNT_NOT_ACTIVATED:
					{
						login_card.problemTextText = Type_Service_Error_User.GET_TEXT(userMember.errorType);
						//						resendVerifyEmailButton.visible = true;
						login_card.currentState = "returnLoginIssue";
						login_card.dispatchEvent(new StringMessageEvent("birtTalkEvent", true, false, "errorMessage"));
						//						login_card.dispatchEvent(new Event("setStateCreateLogin"));
						break;
					}
					case Type_Service_Error_User.LOGIN_EMAIL_NOT_VERIFIED:
					{
						login_card.problemTextText = Type_Service_Error_User.GET_TEXT(userMember.errorType);
						//						resendVerifyEmailButton.visible = true;
						login_card.currentState = "returnLoginIssue";
						login_card.dispatchEvent(new StringMessageEvent("birtTalkEvent", true, false, "errorMessage"));
						//						login_card.dispatchEvent(new Event("setStateCreateLogin"));
						break;
					}
					case Type_Service_Error_User.INVALID_LOGIN:
					{
						//						login_card.errorExplainText.text = Type_Service_Error_User.GET_TEXT(userMember.errorType);
						login_card.dispatchEvent(new StringMessageEvent("birtTalkEvent", true, false, "errorMessage"));
						//						login_card.errorExplainText.visible = true;
					}
					default:
					{
						/* won't ever get here */
						login_card.currentState = 'login';
						break;
					}
				}
				login_card.dispatchEvent(new Event('event_login_failed', true, false));
			}
		}
		
		protected static function handler_responder_update_password(e:ResultEvent):void
		{
//			trace("password change attempt");
		}
		
		protected static function handler_responder_email_send_welcome(e:ResultEvent):void
		{
//			trace("welcome email sent");
		}
		
		protected static function handler_responder_email_send_verify(e:ResultEvent):void
		{
//			trace("verify email sent");
		}
		
		protected static function handler_responder_update_new_email(e:ResultEvent):void
		{
//			trace("user email successfully");
			if(Proxy_Service_User.call_later_update_email != null)
			{
				Proxy_Service_User.call_later_update_email.call(null, Boolean(e.result));
			}
		}
		
		protected static function handler_responder_update_user(e:ResultEvent):void
		{
//			trace("user updated successfully");
		}
		
		protected static function handler_fault(e:FaultEvent):void
		{
			trace("ERROR: Proxy_Service_User: failed to update_user");
			CursorManager.removeBusyCursor();
			trace(e.message);
		}
		
		protected static function createResponder_resultHandler(event:ResultEvent):void
		{
			var userNew:RO_User = event.result as RO_User;
			if (userNew.errorType == Type_Service_Error_User.NONE) /* no errors */
			{
//				trace('created new user successfully');
				Globals.tracker.trackEvent(Type_Event_Tracking_Categories.DROPOLY,
					Type_Event_Tracking_Actions.LOAD,
					Type_Event_Tracking_Labels.CREATE,
					userNew.userID);
				/* dispatchEvent(new Event("loginSuccessful", true)); */
				login_card.user = userNew;
				login_card.currentState = "confirmCreateLogin";
				login_card.dispatchEvent(new StringMessageEvent("birtTalkEvent", true, false, "itWorkedMessage"));
				
				/* send welcome and verification emails */
				Proxy_Service_User.email_send_verify(userNew);
				Proxy_Service_User.email_send_welcome(userNew);
			}
			else
			{
				/* there was some error */
				trace('error: failed to create user. ', userNew.errorMessage);
				login_card.currentState = "problemCreateLogin";
				login_card.problemTextText = Type_Service_Error_User.GET_TEXT(userNew.errorType) + Lang.xml_external.login.panel_login.registration_issue.copy_error_repeat_problems;
				login_card.dispatchEvent(new StringMessageEvent("birtTalkEvent", true, false, "errorMessage"));
			}
			CursorManager.removeBusyCursor();
		}
		
		protected static function resendVerifyEmailResponder_resultHander(event:ResultEvent):void
		{
			/* throw event to tell bird to say "sent!" */
			/* throw event to go back to return login */
			login_card.dispatchEvent(new Event("setStateReturnLogin"));
			login_card.dispatchEvent(new StringMessageEvent("birtTalkEvent", true, false, "itWorkedMessage"));
			login_card.currentState = "returnLogin";
			CursorManager.removeBusyCursor();
		}
		
		protected static function sendResetPassEmailResponder_resultHander(event:ResultEvent):void
		{
			login_card.dispatchEvent(new StringMessageEvent("birtTalkEvent", true, false, "noTalk"));
			CursorManager.removeBusyCursor();
			login_card.currentState = "resetPassword";
//			trace('sent reset code.');
			Globals.tracker.trackEvent(Type_Event_Tracking_Categories.DROPOLY,
				Type_Event_Tracking_Actions.PASS_RESET,
				Type_Event_Tracking_Labels.SEND,
				RO_User(event.result).userID);
		}
		
		protected static function resetPasswordResponder_resultHander(event:ResultEvent):void
		{
			if(RO_User(event.result).errorMessage=='none')
			{
				Proxy_Service_User.login_no_pass(RO_User(event.result));
				login_card.dispatchEvent(new StringMessageEvent("birtTalkEvent", true, false, "itWorkedMessage"));
//				trace("password reset!");
				CursorManager.removeBusyCursor();
				Globals.tracker.trackEvent(Type_Event_Tracking_Categories.DROPOLY,
					Type_Event_Tracking_Actions.PASS_RESET,
					Type_Event_Tracking_Labels.SUCCESS,
					RO_User(event.result).userID);
			}
			else
			{
				login_card.currentState = 'login';
				trace("ERROR: Proxy_Service_User: password reset failed");
				CursorManager.removeBusyCursor();
				Globals.tracker.trackEvent(Type_Event_Tracking_Categories.DROPOLY,
					Type_Event_Tracking_Actions.PASS_RESET,
					Type_Event_Tracking_Labels.FAIL,
					RO_User(event.result).userID);
			}
		}
		
		protected static function ro_faultHandler(event:FaultEvent):void
		{
			trace("ERROR: Proxy_Service_User: "+event.fault.message);
			CursorManager.removeBusyCursor();
			login_card.currentState = "problemCreateLogin";
			login_card.problemTextText = "We're sorry, a server error occured. " + event.fault.message;
			Globals.tracker.trackEvent(Type_Event_Tracking_Categories.DROPOLY,
				Type_Event_Tracking_Actions.LOGIN_FAIL);
		}
	}
}