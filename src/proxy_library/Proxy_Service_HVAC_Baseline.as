package proxy_library
{
	import globals.Globals_Library;
	
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import ros.RO_HVAC_Baseline;
	
	import services.Service_HVAC_Baseline;
	
	import singletons.Globals;
	import singletons.RO_State_Global;

	public class Proxy_Service_HVAC_Baseline
	{
		public static function insert(value:RO_HVAC_Baseline, id_residence:int, id_user:int):void
		{
			Service_HVAC_Baseline.insert(value, id_residence, id_user, handler_responder_insert, handler_fault);
		}
		
//		public static function get_latest(id_residence:int):void
//		{
//			Service_HVAC_Baseline.get_latest(id_residence, handler_responder_get_latest, handler_fault);
//		}
//		
//		public static function get_first(id_residence:int):void
//		{
//			Service_HVAC_Baseline.get_first(id_residence, handler_responder_get_first, handler_fault);
//		}
		
		protected static function handler_responder_insert(e:ResultEvent):void
		{
//			trace("Service_HVAC_Baseline.handler_responder_insert: it worked!");
		}
		
		protected static function handler_fault(e:FaultEvent):void
		{
			trace("ERROR: Service_HVAC_Baseline failed:"+e.message);
		}	
		
//		protected static function handler_responder_get_latest(e:ResultEvent):void
//		{
//			RO_State_Global.HVAC_baseline_latest = RO_HVAC_Baseline(e.result);
//			var temp:RO_HVAC_Baseline = RO_HVAC_Baseline(e.result);
//			/* attempt recalculate */
//			Globals_Library.energy_model.model_grade_energy.has_first_HVAC_baseline = true;
//			Globals_Library.energy_model.calculate(true);
//			Globals.gui_top.update_potential_savings();
//		}
//		
//		protected static function handler_responder_get_first(e:ResultEvent):void
//		{
//			RO_State_Global.HVAC_baseline_first = RO_HVAC_Baseline(e.result);
//			var temp:RO_HVAC_Baseline = RO_HVAC_Baseline(e.result);
//			if (Globals_Library.energy_model != null)
//			{
//				Globals_Library.energy_model.model_grade_energy.has_first_HVAC_baseline = true;
//				/* attempt recalculate */
//				Globals_Library.energy_model.calculate(true);
//				Globals.gui_top.update_potential_savings();
//			}
//		}
	}
}
