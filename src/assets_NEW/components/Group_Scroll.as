package assets_NEW.components
{
	import spark.components.Group;
	import spark.core.NavigationUnit;
	
	public class Group_Scroll extends Group
	{
		private var _step_size:int = 5;
		
		public function get stepSize():int
		{
			return _step_size;
		}
		
		public function set stepSize(value:int):void
		{
			_step_size = value;
		}
		
		override public function getVerticalScrollPositionDelta(navigationUnit:uint):Number
		{
			var vertical_scroll_pos_delta:Number = super.getVerticalScrollPositionDelta(navigationUnit);

			if(vertical_scroll_pos_delta == 0)
			{
				return 0;
			}
			else if(vertical_scroll_pos_delta < 0)
			{
				return -1*_step_size;
			}
			else if(vertical_scroll_pos_delta > 0)
			{
				return _step_size;
			}
			
			return vertical_scroll_pos_delta; 
		}
		
		public function Group_Scroll()
		{
			super();
		}
	}
}