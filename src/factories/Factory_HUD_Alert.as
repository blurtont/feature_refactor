package factories
{
	import main.scientificModels.ScientificFunctions;
	
	import ros.RO_Action;
	import ros.RO_Room_Components.RO_Device;
	
	import views.hud.hud_alerts.HUD_Alert;
	import views.hud.hud_alerts.HUD_Alert_Results_Device;
	import views.hud.hud_alerts.HUD_Alert_Tutorial_Page_Community;
	import views.hud.hud_alerts.HUD_Alert_Tutorial_Page_Results;
	import views.hud.hud_alerts.HUD_Alert_Tutorial_Page_Todo;
	import views.hud.hud_alerts.HUD_Alert_Tutorial_Page_Utility;
	import views.hud.hud_alerts.HUD_Alert_Utility_Entry_Complete;
	
	public class Factory_HUD_Alert
	{
		public static function create( type_alert:Number, args:Array ):HUD_Alert
		{
			var result:HUD_Alert = new HUD_Alert();
			switch(type_alert)
			{
				case HUD_Alert.HUD_ALERT_RESULTS_DEVICE:
					result = new HUD_Alert_Results_Device();
					
					var ro_actions:Array = [];
					if (args[0] == null)
					{
						trace("WARNING: Factory_HUD_Alert: args[0] should be an RO_Device and is null");
					}
					else if (!args[0] is RO_Device)
					{
						trace("WARNING: Factory_HUD_Alert: arg[0] should be a RO_Device and is not");	
					}
					else
					{
						/* filter out actions with <= 0 savings, sort by payback ascending and pick the  */
						ro_actions = RO_Device(args[0]).ro_actions.filter( 
							function(item:RO_Action, index:int, array:Array):Boolean { return item.savings_cost > 0 }).sort( 
								ScientificFunctions.sort_ro_action_payback_ascending);
						HUD_Alert_Results_Device(result).ro_device = RO_Device(args[0]);
					}
					
//					var ro_actions:Array = args[ 0 ] == null ? [] : RO_Device( args[ 0 ] ).ro_actions.filter( 
//						function( a:RO_Action, i:int, arr:Array ):Boolean { return a.savings_cost > 0 } ).sort( 
//							ScientificFunctions.sort_ro_action_payback_ascending );
					
					HUD_Alert_Results_Device(result).ro_action = 
						(ro_actions.length > 0 ) ? ro_actions[0] : null;
					break;
				
				case HUD_Alert.HUD_ALERT_TUTORIAL_PAGE_COMMUNITY:
					result = new HUD_Alert_Tutorial_Page_Community();
					break;
				case HUD_Alert.HUD_ALERT_TUTORIAL_PAGE_RESULTS:
					result = new HUD_Alert_Tutorial_Page_Results();
					break;
				case HUD_Alert.HUD_ALERT_TUTORIAL_PAGE_TODO:
					result = new HUD_Alert_Tutorial_Page_Todo();
					break;
				case HUD_Alert.HUD_ALERT_TUTORIAL_PAGE_UTILITY:
					result = new HUD_Alert_Tutorial_Page_Utility();
					break;
				
				case HUD_Alert.HUD_ALERT_UTILITY_COMPLETE_ALL:
					result = new HUD_Alert_Utility_Entry_Complete() as HUD_Alert;
					break;
				
				default:
					break;
			}
			return result;
		}
	}
}
