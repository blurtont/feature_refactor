package factories
{
	import views.hud.info_questions.*;
	
	import ros.RO_Object;
	import ros.RO_Room;
	import ros.RO_Room_Components.RO_Device;
	
	import spark.components.Group;
	
	public class Factory_Info_Questions
	{
		public static function create(ro_object:RO_Object):IInfo_Questions
		{
			var result:IInfo_Questions;
			if(ro_object is RO_Device)
			{
				result = Factory_Info_Questions_Device.create(RO_Device(ro_object));
			}
			else if(ro_object is RO_Room)
			{
				result = new Info_Questions_Room();
				Info_Questions_Room(result).ro_room = RO_Room(ro_object);
			}
			return result;
		}
	}
}