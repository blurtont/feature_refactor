package factories
{
	import assets.graphics.rooms.Graphic_Background_Kitchen;
	import assets.graphics.rooms.Graphic_Background_Room_Attic;
	import assets.graphics.rooms.Graphic_Background_Room_Bathroom_Full;
	import assets.graphics.rooms.Graphic_Background_Room_Bedroom_Master;
	import assets.graphics.rooms.Graphic_Background_Room_Utility;
	
	import backdrops.gui_rooms.*;
	
	import main.energyModels.weatherModels.GroundTemperatureModel;
	
	import mx.core.IVisualElement;
	import mx.core.UIComponent;
	
	import ros.RO_Room;
	
	import spark.components.Group;
	import spark.primitives.Graphic;
	
	import types.Type_Room;
	
	public class Factory_Room_Backgrounds
	{
		public static function create(ro_room:RO_Room):IVisualElement
		{
//			return new GUI_Room_Attic(); //Graphic_Backdrop_Attic();
			var graphic:*, result:Group;
			switch(ro_room.type_room)
			{
				case Type_Room.ATTIC:
					result = new Group();
					graphic = new Graphic_Background_Room_Attic();
					result.addElement(graphic);
					graphic.x = 25;
					graphic.y = 140;
					return result;
					break;
				case Type_Room.BASEMENT:
					return new GUI_Room_Basement();
					break;
				case Type_Room.BEDROOM_1:
				case Type_Room.BEDROOM_2:
					return new GUI_Room_Bedroom_2();
					break;
				case Type_Room.DINING_ROOM:
					return new GUI_Room_Dining();
					break;
				case Type_Room.BATHROOM_DOWNSTAIRS:
//				case Type_Room.BATHROOM_UPSTAIRS:
					return new GUI_Room_Bathroom_Big();
					break;
				case Type_Room.KITCHEN:
					return new Graphic_Background_Kitchen();
					break;
				case Type_Room.LIVING_ROOM:
					return new GUI_Room_TV_Room();
					break;
				case Type_Room.BATHROOM_MASTER:
					result = new Group();
					graphic = new Graphic_Background_Room_Bathroom_Full();
					result.addElement(graphic);
					graphic.x = 60;
					graphic.y = 25;
					return result;
					break;
				case Type_Room.BEDROOM_MASTER:
					return new Graphic_Background_Room_Bedroom_Master();
					break;
				case Type_Room.OFFICE:
					return new GUI_Room_Home_Office();
					break;
				case Type_Room.TV_ROOM:
					return new GUI_Room_TV_Room();
					break;
				case Type_Room.UTILITY_ROOM:
					return new Graphic_Background_Room_Utility();
					break;
				default:
					/* ROOM TYPE DOESN'T EXIST: ERROR */
					trace("ERROR: This room type does not exist");
					break;
			}
			return null;
		}
	}
}