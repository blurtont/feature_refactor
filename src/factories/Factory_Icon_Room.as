package factories
{
	import mx.core.IVisualElement;
	
	import spark.primitives.Graphic;
	
	import ros.RO_Room;
	
	import types.Type_Room;
	
	import views.navigator_rooms.Icon_Room;
	import views.navigator_rooms.assets.icons.Graphic_Icon_Room_Attic;
	import views.navigator_rooms.assets.icons.Graphic_Icon_Room_Basement;
	import views.navigator_rooms.assets.icons.Graphic_Icon_Room_Bathroom_Tub;
	import views.navigator_rooms.assets.icons.Graphic_Icon_Room_Bedroom_Pillow;
	import views.navigator_rooms.assets.icons.Graphic_Icon_Room_Dining;
	import views.navigator_rooms.assets.icons.Graphic_Icon_Room_Kitchen;
	import views.navigator_rooms.assets.icons.Graphic_Icon_Room_Office;
	import views.navigator_rooms.assets.icons.Graphic_Icon_Room_TV;
	import views.navigator_rooms.assets.icons.Graphic_Icon_Room_Utility;

	public class Factory_Icon_Room
	{
		public static function create(ro_room:RO_Room, decoration_only:Boolean = false):Icon_Room
		{
			var result:Icon_Room = new Icon_Room();
			result.decoration_only = decoration_only;
			result.ro_room = ro_room;
			
			/* init icon */
			result.group_icon.removeAllElements();
			result.group_icon.addElement(Factory_Icon_Room._get_graphic_icon_room(ro_room));

			return result;
		}
		
		private static function _get_graphic_icon_room(ro_room:RO_Room):IVisualElement
		{
			var result:IVisualElement;
			switch(ro_room.type_room)
			{
				case Type_Room.ATTIC:
				{
					result = new Graphic_Icon_Room_Attic();
					break;
				}
				case Type_Room.BASEMENT:
				{
					result = new Graphic_Icon_Room_Basement();
					break;
				}
				case Type_Room.BATHROOM_DOWNSTAIRS:
				case Type_Room.BATHROOM_MASTER:
//				case Type_Room.BATHROOM_UPSTAIRS:
				{
					result = new Graphic_Icon_Room_Bathroom_Tub();
					break;
				}
				case Type_Room.BEDROOM_1:
				case Type_Room.BEDROOM_2:
				case Type_Room.BEDROOM_MASTER:
				{
					result = new Graphic_Icon_Room_Bedroom_Pillow();
					break;
				}
				case Type_Room.DINING_ROOM:
				{
					result = new Graphic_Icon_Room_Dining();
					break;
				}
				case Type_Room.KITCHEN:
				{
					result = new Graphic_Icon_Room_Kitchen();
					break;
				}
//				case Type_Room.LIVING_ROOM:
//				{
//					//derp
//					break;
//				}
				case Type_Room.OFFICE:
				{
					result = new Graphic_Icon_Room_Office();
					break;
				}
				case Type_Room.TV_ROOM:
				{
					result = new Graphic_Icon_Room_TV();
					break;
				}
				case Type_Room.UTILITY_ROOM:
				{
					result = new Graphic_Icon_Room_Utility();
					break;
				}
				default:
				{
					result = new Graphic_Icon_Room_Attic(); //box is default, because just because.
					break;
				}
			}
			return result;
		}
	}
}