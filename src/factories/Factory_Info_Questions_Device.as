package factories
{
	import views.hud.info_questions.*;
	
	import types.Type_Device;
	
	import ros.RO_Room_Components.RO_Device;
	
	public class Factory_Info_Questions_Device
	{
		public static function create(ro_device:RO_Device):Info_Questions_Device
		{
			var result:Info_Questions_Device;
			if(ro_device is RO_Device)
			{
				switch(Type_Device.type_device(ro_device))
				{
					case Type_Device.BASEMENT_CRAWLSPACE_FLOOR:
					{
						result = new info_questions_basement_crawl_space_floor();
						break;
					}
						
					case Type_Device.BASEMENT_FLOOR:
					{
						//info_questions = new info_questions_basement_wall
						result = null;
						break;
					}
						
					case Type_Device.BASEMENT_WALL:
					{
						result = new info_questions_basement_wall();
						break;
					}
						
					case Type_Device.CEILING:
					{
						result = new info_questions_ceiling();
						break;
					}
					case Type_Device.CLOTHES_DRYER:
					{
						result = new info_questions_clothes_dryer();
						break;
					}
						
					case Type_Device.CLOTHES_WASHER:
					{
						result = new info_questions_clothes_washer();
						break;
					}
						
					case Type_Device.COFFEE_MAKER:
					{
						result = new info_questions_coffee_maker();
						break;
					}
						
					case Type_Device.COOKTOP:
					{
						result = new info_questions_cooktop();
						break;
					}
						
					case Type_Device.COOLING_SYSTEM:
					{
						result = new info_questions_whole_house_cooling();
						break;
					}
						
					case Type_Device.DISH_WASHER:
					{
						result = new info_questions_dishwasher();
						break;
					}
						
					case Type_Device.DOOR:
					{
						result = new info_questions_door();
						break;
					}
						
					case Type_Device.DUCTWORK:
					{
						result = new info_questions_ductwork();
						break;
					}
						
					case Type_Device.FAUCET:
					{
						result = new info_questions_faucet();
						break;
					}
						
					case Type_Device.HEATING_SYSTEM:
					{
						result = new info_questions_whole_house_heating();
						break;
					}
						
					case Type_Device.HOUSE_SEAL:
					{
						result = new info_questions_house_seal();
						break;
					}
						
					case Type_Device.LIGHT:
					{
						result = new info_questions_lights();
						break;
					}
						
					case Type_Device.LOCAL_COOLING:
					{
						result = new info_questions_local_cooling();
						break;
					}
						
					case Type_Device.LOCAL_HEATING:
					{
						result = new info_questions_local_heating();
						break;
					}
						
					case Type_Device.OVEN:
					{
						result = null; //the oven is a lie.
						//					info_questions = new info_questions_();
						break;
					}
						
					case Type_Device.PLUG_LOAD:
					{
						result = new info_questions_plug_load_device();
						break;
					}
						
					case Type_Device.REFRIGERATOR:
					{
						result = new info_questions_refrigerator();
						break;
					}
						
					case Type_Device.SHOWER:
					{
						result = new info_questions_shower();
						break;
					}
						
					case Type_Device.SLAB_FLOOR:
					{
						result = new info_questions_slab();
						break;
					}
						
					case Type_Device.THERMOSTAT:
					{
						result = new info_questions_thermostat();
						break;
					}
						
					case Type_Device.WALL:
					{
						result = new info_questions_wall();
						break;
					}
						
					case Type_Device.WATER_HEATER:
					{
						result = new info_questions_whole_house_water_heating();
						break;
					}
						
					case Type_Device.WINDOW_SYSTEM:
					{
						result = new info_questions_window();
						break;
					}
						
					default:
					{
						trace("ERROR Factory_Info_Questions: unkown type_device");
						result = null;
						break;
					}
				}
				result.ro_device = ro_device;
			}
			return result;
		}
	}
}