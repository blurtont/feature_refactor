package factories
{
	import assets.graphics.icons.*;
	import assets.graphics.icons.devices.*;
	
	import mx.core.IVisualElement;
	
	import ros.RO_Room_Components.RO_Device;
	
	import types.Type_Device;
	
	import views.navigator_devices.Icon_Device;
	import views.navigator_rooms.assets.icons.*;
	
	public class Factory_Icon_Device
	{
		public static function create(ro_device:RO_Device):Icon_Device
		{
			var result:Icon_Device = new Icon_Device();
			result.ro_device = ro_device;
			
			/* init icon */
			result.group_icon.removeAllElements();
			result.group_icon.addElement(Factory_Icon_Device._get_graphic_icon_device(ro_device));

			return result;
		}
		
		private static function _get_graphic_icon_device(ro_device:RO_Device):IVisualElement
		{
			var result:IVisualElement;
			
			switch (Type_Device.type_device(ro_device))
			{
				case Type_Device.BASEMENT_CRAWLSPACE_FLOOR:
				{
					result = new Graphic_Icon_Room_Attic(); // TODO
					break;
				}
				case Type_Device.BASEMENT_FLOOR:
				{
					result = new Graphic_Icon_Room_Attic(); // TODO
					break;
				}
				case Type_Device.BASEMENT_WALL:
				{
					result = new Graphic_Device_Icon_Basement_Wall();
					break;
				}
				case Type_Device.CEILING:
				{
					result = new Graphic_Device_Icon_Insulation_Ceiling();
					break;
				}
				case Type_Device.CLOTHES_DRYER:
				{
					result = new Graphic_Device_Icon_Clothes_Dryer();
					break;
				}
				case Type_Device.CLOTHES_WASHER:
				{
					result = new Graphic_Device_Icon_Clothes_Washer();
					break;
				}
				case Type_Device.COFFEE_MAKER:
				{
					result = new Graphic_Device_Icon_Coffee_Maker();
					break;
				}
				case Type_Device.COOKTOP:
				{
					result = new Graphic_Device_Icon_Cooktop();
					break;
				}
				case Type_Device.COOLING_SYSTEM:
				{
					result = new Graphic_Device_Icon_Cooling_Residence();
					break;
				}
				case Type_Device.DISH_WASHER:
				{
					result = new Graphic_Device_Icon_Dishwasher();
					break;
				}
				case Type_Device.DOOR:
				{
					result = new Graphic_Device_Icon_Door();
					break;
				}
				case Type_Device.DUCTWORK:
				{
					result = new Graphic_Icon_Room_Attic(); // TODO
					break;
				}
				case Type_Device.FAUCET:
				{
					result = new Graphic_Device_Icon_Faucet();
					break;
				}
				case Type_Device.HEATING_SYSTEM:
				{
					result = new Graphic_Device_Icon_Furnace();
					break;
				}
				case Type_Device.HOUSE_SEAL:
				{
					result = new Graphic_Icon_Room_Attic(); // TODO
					break;
				}
				case Type_Device.LIGHT:
				{
					result = new Graphic_Device_Icon_Light();
					break;
				}
				case Type_Device.LOCAL_COOLING:
				{
					result = new Graphic_Icon_Room_Attic(); // TODO
					break;
				}
				case Type_Device.LOCAL_HEATING:
				{
					result = new Graphic_Icon_Room_Attic(); // TODO
					break;
				}
				case Type_Device.OVEN:
				{
					result = new Graphic_Icon_Room_Attic(); // TODO
					break;
				}
				case Type_Device.PLUG_LOAD:
				{
					result = new Graphic_Device_Icon_Plug_Load();
					break;
				}
				case Type_Device.REFRIGERATOR:
				{
					result = new Graphic_Device_Icon_Refrigerator();
					break;
				}
				case Type_Device.SHOWER:
				{
					result = new Graphic_Device_Icon_Shower();
					break;
				}
				case Type_Device.SLAB_FLOOR:
				{
					result = new Graphic_Icon_Room_Attic(); // TODO
					break;
				}
				case Type_Device.THERMOSTAT:
				{
					result = new Graphic_Device_Icon_Thermostat();
					break;
				}
				case Type_Device.WALL:
				{
					result = new Graphic_Icon_Room_Attic(); // TODO
					break;
				}
				case Type_Device.WATER_HEATER:
				{
					result = new Graphic_Device_Icon_Water_Heater();
					break;
				}
				case Type_Device.WINDOW_SYSTEM:
				{
					result = new Graphic_Device_Icon_Window();
					break;
				}
				default:
				{
					result = new Graphic_Icon_Room_Attic();
					break;
				}
			}
			return result;
		}

	}
}