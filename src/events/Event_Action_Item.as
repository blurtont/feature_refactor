package events
{
	import flash.events.Event;
	
	import ros.RO_Action;
	
	public class Event_Action_Item extends Event
	{
		public var ro_action:RO_Action;
		
		public function Event_Action_Item(type:String, ro_action:RO_Action)
		{
			this.ro_action = ro_action;
			super(type, true, false);
		}
	}
}