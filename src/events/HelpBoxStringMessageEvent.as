package events
{
	import flash.events.Event;

	public class HelpBoxStringMessageEvent extends Event
	{
		private var _titleText:String;
		private var _helpText:String;
		
		public function HelpBoxStringMessageEvent(type:String,
										   bubbles:Boolean,
										   cancelable:Boolean,
										   titleText:String,
										   helpText:String)
		{
			super(type, bubbles, cancelable);
			_titleText = titleText;
			_helpText = helpText;
		}
		
		public function get titleText():String
		{
			return _titleText;
		}

		public function get helpText():String
		{
			return _helpText;
		}

		override public function clone():Event
		{
			return new HelpBoxStringMessageEvent(type, bubbles, cancelable, _titleText, _helpText);
		}
	}
}