package events
{
	import flash.events.Event;

	public class StringMessageEvent extends Event
	{
		private var _message:String;
		
		public function StringMessageEvent(type:String,
										   bubbles:Boolean,
										   cancelable:Boolean,
										   message:String)
		{
			super(type, bubbles, cancelable);
			_message = message;
		}

		public function get message():String
		{
			return _message;
		}
		
		override public function clone():Event
		{
			return new StringMessageEvent(type, bubbles, cancelable, _message);
		}
	}
}