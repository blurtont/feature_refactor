package events
{
	import flash.events.Event;
	
	public class Event_Message extends Event
	{
		private var _message:*;
		
		public function Event_Message(type:String,
										   bubbles:Boolean,
										   cancelable:Boolean,
										   message:*)
		{
			super(type, bubbles, cancelable);
			_message = message;
		}
		
		public function get message():*
		{
			return _message;
		}
		
		override public function clone():Event
		{
			return new StringMessageEvent(type, bubbles, cancelable, _message);
		}
	}
}