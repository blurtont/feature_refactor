package events
{
	import flash.events.Event;

	public class NumberMessageEvent extends Event
	{
		private var _message:Number;
		
		public function NumberMessageEvent(type:String,
										   message:Number,
										   bubbles:Boolean=false,
										   cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			_message = message;
		}
		
		public function get message():Number
		{
			return _message;
		}
		
		override public function clone():Event
		{
			return new NumberMessageEvent(type, _message, bubbles, cancelable);
		}
	}
}