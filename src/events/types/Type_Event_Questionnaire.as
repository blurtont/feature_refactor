package events.types
{
	public final class Type_Event_Questionnaire
	{
		public static var helpButtonClick:String = "helpButtonClick";
		public static var personalUseChanged:String = "personalUseChanged";
		public static var numRoommatesChanged:String = "numRoommatesChanged";
		public static var numRoommatesChangedMouseUp:String = "numRoommatesChangedMouseUp";
		public static var rentOwnChanged:String = "rentOwnChanged";
		public static var squareFootageChanged:String = "squarefootageChanged";
		public static var doItYourselfChanged:String = "doItYourselfChanged";
		public static var basementTypeChanged:String = "basementTypeChanged";
		public static var applianceEnergyUseSelectedChanged:String = "applianceEnergyUseSelectedChanged";
		public static var ceilingHeightChanged:String = "ceilingHeightChanged";
//		public static var numberFloorsChanged:String = "numberFloorsChanged";
	}
}