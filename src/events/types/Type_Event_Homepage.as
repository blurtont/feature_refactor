package events.types
{
	public final class Type_Event_Homepage
	{
		public static var enter_residence:String = "type_event_homepage_enter_residence";
		public static var all_done:String = "type_event_all_done";
	}
}