package events.types
{
	public final class Type_Event_Tracking_Categories
	{
		public static const DROPOLY:String = "Dropoly";
		public static const HOME_NAV:String = "Home Navigation";
		public static const HUD:String = "HUD";
		public static const JOURNAL:String = "Journal";
		public static const JOURNAL_TODO:String = "Journal - TODO";
		public static const JOURNAL_UTIL:String = "Journal - UTIL";
		public static const QUESTIONNAIRE:String = "Questionnaire";
		public static const TUTORIAL:String = "Tutorial";
	}
}