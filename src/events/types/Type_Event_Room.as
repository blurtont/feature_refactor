package events.types
{
	public final class Type_Event_Room
	{
		public static var toggleViewUgrade:String = "viewUgradeToggle";
		public static var toggleViewFree:String = "viewFreeToggle";
		public static var toggleViewTopPriority:String = "viewTopPriorityToggle";
		public static var toggleViewAddInfo:String = "viewAddInfoToggle";		
	}
}