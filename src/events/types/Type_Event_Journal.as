package events.types
{
	public final class Type_Event_Journal
	{
		public static var open:String = "type_event_journal_open";
		public static var click_category_results:String = "type_event_journal_open_click_category_results";
		public static var click_todo_list_item:String = "type_event_journal_click_todo_list_item";
		public static var do_action:String = "type_event_journal_do_action";
		public static var action_completed:String = "type_event_journal_action_completed";
	}
}