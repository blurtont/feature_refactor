package events.types
{
	public final class Type_Event_Tracking_Labels
	{
		public static const CREATE:String = "new";
		public static const RETURN_USER:String = "return";
		public static const SEND:String = "send";
		public static const SUCCESS:String = "success";
		public static const FAIL:String = "fail";
		public static const FREE:String = "free";
		public static const DEVICE:String = "device";
		public static const ROOM:String = "room";
		public static const PAGE_RESULTS:String = "results page";
		public static const PAGE_TODO:String = "todo page";
		public static const PAGE_UTILITIES:String = "utilities page";
		public static const PAGE_SAVINGS:String = "savings page";
		public static const JOURNAL_TAB:String = "journal tab";
		public static const UPGRADE:String = "upgrade";
		public static const ZIP:String = "zip";
		public static const HEIGHT_CEILING:String = "ceiling height";
	}
}