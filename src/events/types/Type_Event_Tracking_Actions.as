package events.types
{
	public final class Type_Event_Tracking_Actions
	{
		public static const LOAD:String = "load";
		public static const LOGIN_FAIL:String = "login fail";
		public static const PASS_RESET:String = "pass reset";
		public static const STATE_CHANGE:String = "state change";
		public static const ADD:String = "add";
		public static const REMOVE:String = "remove";
		public static const EDIT:String = "edit";
		public static const VIEW:String = "view";
		public static const COMPLETE:String = "complete";
		public static const VERIFY:String = "verify";
	}
}